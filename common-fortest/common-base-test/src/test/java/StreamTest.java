import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author zhoua
 * @date 2024/6/29 15:30
 */
public class StreamTest {

    @Data
    @AllArgsConstructor
    public static class Student {
        private Long id;
        private String cnName;
        private String enName;
    }

    @Test
    public void testList2Map() {
        Student student1 = new Student(1L, "张三", "zhangsan");
        Student student2 = new Student(2L, "李四", "lisi");
        Student student3 = new Student(3L, "王五", null);
        Student student4 = new Student(4L, "", "");

        List<Student> studentList = Lists.newArrayList(student1, student2,
                student3, student4);
        // 因为student3的enName为空, 所以导致空指针异常, 这里需要将null排除
        Map<Long, String> idEnNameMap = studentList.stream().filter(s -> !Objects.isNull(s.getEnName()))
                .collect(Collectors.toMap(Student::getId, Student::getEnName));



        System.out.println(idEnNameMap);

    }
}
