import org.junit.Test;

/**
 * @author an.zhou
 * @date 2023/4/24 17:24
 */
public class ExtendsTest {

    public static class Parent {

        public Parent() {
            System.out.println("parent#this()");
        }

        {
            System.out.println("parent#instanceCode");
        }

        private String label = "parent";

        public void setLabel(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public static class Son extends Parent {

        public Son() {
            System.out.println("son#this()");
        }

    }

    @Test
    public void testPrint() {
        Son son = new Son();
        System.out.println(son.getLabel());

        System.out.println("V85453-图达通China江苏苏州市苏州市相城区高铁新城青龙港路286号长三角国际研发社区启动区9号楼DV85453".length());
    }
}
