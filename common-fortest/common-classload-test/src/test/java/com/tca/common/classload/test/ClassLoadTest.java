package com.tca.common.classload.test;

import com.tca.common.classload.ThirdClazzClassLoader;
import com.tca.common.classload.ThirdJarClassLoader;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * @author zhoua
 * @date 2023/12/4 21:41
 */
@Slf4j
public class ClassLoadTest {

    @Test
    public void testThirdJarClassLoader4Singleton() {
        // 单例模式
        try {
            ClassLoader classLoader = ThirdJarClassLoader.getClassLoader("E:\\java_install\\apache-maven-repository\\tca\\third-jar-ext\\1.0.0");
            Class<?> clazz = classLoader.loadClass("com.tca.third.jar.ext.Animal");

            Constructor<?> constructor = clazz.getDeclaredConstructor(null);
            Object animal = constructor.newInstance();
            log.info("animal对象生成 ---> {}", animal);

            Method method = clazz.getDeclaredMethod("test");
            method.invoke(animal);

            log.info("==================================");

            // 再来一个
            ClassLoader classLoader2 = ThirdJarClassLoader.getClassLoader("E:\\java_install\\apache-maven-repository\\tca\\third-jar-ext\\1.0.0");
            Class<?> clazz2 = classLoader2.loadClass("com.tca.third.jar.ext.Animal");
            log.info("classLoader == classLoader2 --> {}", classLoader == classLoader2);
            log.info("clazz == clazz2 --> {}", clazz == clazz2);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testThirdJarClassLoader4Proto() {
        // 非单例模式
        try {
            ClassLoader classLoader = ThirdJarClassLoader.getClassLoader("E:\\java_install\\apache-maven-repository\\tca\\third-jar-ext\\1.0.0", false);
            Class<?> clazz = classLoader.loadClass("com.tca.third.jar.ext.Animal");

            Constructor<?> constructor = clazz.getDeclaredConstructor(null);
            Object animal = constructor.newInstance();
            log.info("animal对象生成 ---> {}", animal);

            Method method = clazz.getDeclaredMethod("test");
            method.invoke(animal);

            log.info("==================================");

            // 再来一个
            ClassLoader classLoader2 = ThirdJarClassLoader.getClassLoader("E:\\java_install\\apache-maven-repository\\tca\\third-jar-ext\\1.0.0", false);
            Class<?> clazz2 = classLoader2.loadClass("com.tca.third.jar.ext.Animal");
            log.info("classLoader == classLoader2 --> {}", classLoader == classLoader2);
            log.info("clazz == clazz2 --> {}", clazz == clazz2);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testThirdClazzClassLoader4Singleton() {
        String directory = "E:\\idea_work_space\\third-jar\\target\\classes";
        String className = "com.tca.third.jar.Dog";

        try {
            ClassLoader classLoader = ThirdClazzClassLoader.getClassLoader(directory);
            Class<?> clazz = classLoader.loadClass(className);

            Constructor<?> con = clazz.getDeclaredConstructor(null);
            Object dog = con.newInstance();

            Method method = clazz.getDeclaredMethod("getVersion");
            Object result = method.invoke(dog);

            log.info("result = {}", result);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
