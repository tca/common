import cn.hutool.core.io.FileUtil;

import java.io.File;

/**
 * @author zhoua
 * @date 2022/11/25 19:55
 */
public class CopyFileTest {

    private static final String source = "D:\\idea_work_space\\eagle";

    private static final String dest = "C:\\Users\\zhouan\\Desktop";

    public static void main(String[] args) {
//        fileCopy(source, dest);
        txt2java("E:\\idea_work_space\\unicorn-application");
    }

    private static void fileCopy(String source, String dest) {
        File file = new File(source);
        if (file.isDirectory()) {
            String name = file.getName();
            File destFile = new File(dest, name);
            destFile.mkdir();

            File[] files = file.listFiles();
            for (File f : files) {
                fileCopy(f.getAbsolutePath(), destFile.getAbsolutePath());
            }
        } else {
            String nameBefore = file.getName();
            String fileName = nameBefore.endsWith("java")? nameBefore.substring(0, nameBefore.length() - 5) +  ".txt":
                    nameBefore;
            File destFile = new File(dest, fileName);
            FileUtil.copy(file, destFile, true);
        }
    }

    private static void txt2java(String source) {
        File file = new File(source);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                txt2java(f.getAbsolutePath());
            }
        } else {
            String nameBefore = file.getName();
            if (nameBefore.endsWith(".txt")) {
                String nameAfter = nameBefore.substring(0, nameBefore.length() - 4) +  ".java";
                file.renameTo(new File(file.getParentFile(), nameAfter));
            }
        }
    }
}
