package com.tca.common.web.test.service;

import com.tca.common.core.bean.Result;
import com.tca.common.web.test.entity.User;
import com.tca.common.web.test.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserMapper mockUserMapper;

    @InjectMocks
    private UserService userServiceUnderTest;

    @Test
    void testGetUser() {
        // Setup
        final User user = new User();
        user.setId(0L);
        user.setName("name");
        user.setAge(0);
        user.setEmail("email");
        user.setVersion(0);
        final Result<User> expectedResult = Result.success(user);

        // Configure UserMapper.selectById(...).
        final User user1 = new User();
        user1.setId(0L);
        user1.setName("name");
        user1.setAge(0);
        user1.setEmail("email");
        user1.setVersion(0);
        when(mockUserMapper.selectById(0L)).thenReturn(user1);

        // Run the test
        final Result<User> result = userServiceUnderTest.getUser(0L);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testAddUser() {
        // Setup
        // Run the test
        final Result result = userServiceUnderTest.addUser(0L, "name", 0);

        // Verify the results
        // Confirm UserMapper.insert(...).
        final User entity = new User();
        entity.setId(0L);
        entity.setName("name");
        entity.setAge(0);
        entity.setEmail("email");
        entity.setVersion(0);
        verify(mockUserMapper).insert(entity);
    }
}
