package com.tca.common.web.test.service;

import com.tca.common.core.bean.Result;
import com.tca.common.core.utils.IdUtils;
import com.tca.common.web.test.TestApplication;
import com.tca.common.web.test.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(classes = TestApplication.class)
class UserServiceSpringBootTest {

    @SpyBean
    private UserService spyUserService;

    @Test
    @Transactional // 用于数据回滚
    void testGetUser() {
        Long id = IdUtils.getId();
        // 这里是逻辑删除
        int i = spyUserService.deleteById(id);

        Result<User> userExists = spyUserService.getUser(id);
        Assertions.assertNull(userExists.getData());

        // Setup
        final User user = new User();
        user.setId(id);
        user.setName("name");
        user.setAge(0);
        user.setEmail("email");
        user.setVersion(0);

        Result result = spyUserService.addUser(user.getId(), user.getName(), user.getAge());

        Assertions.assertTrue(result.isSuccess());

        Result<User> userResult = spyUserService.getUser(user.getId());
        Assertions.assertTrue(userResult.isSuccess());
        Assertions.assertEquals(userResult.getData().getName(), user.getName());
    }


}
