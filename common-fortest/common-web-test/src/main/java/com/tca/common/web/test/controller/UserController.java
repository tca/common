package com.tca.common.web.test.controller;

import com.tca.common.core.bean.Result;
import com.tca.common.web.test.entity.User;
import com.tca.common.web.test.req.UserReq;
import com.tca.common.web.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhoua
 * @date 2024/10/31 22:41
 */
@RestController
@RequestMapping("/user")
public class UserController {


    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public Result<User> getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @PostMapping
    public Result addUser(@RequestBody UserReq userReq) {
        return userService.addUser(userReq.getId(), userReq.getName(), userReq.getAge());
    }

}
