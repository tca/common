package com.tca.common.web.test.controller;

import com.tca.common.core.bean.Result;
import com.tca.common.core.datapermission.DataPermissionContextHolder;
import com.tca.common.web.config.LogRecordListenerExecutorConfig;
import com.tca.common.web.filter.DataPermissionContextHolderFilter;
import com.tca.common.web.test.config.IgnoreConfig;
import com.tca.common.web.test.req.UserReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author zhoua
 * @Date 2021/12/10
 */
@RestController
@RequestMapping("/test/web")
@Slf4j
public class TestController {

    @Autowired
    private LogRecordListenerExecutorConfig logRecordListenerExecutorConfig;

    @Autowired
    private DataPermissionContextHolder dataPermissionContextHolder;

    @Autowired
    private DataPermissionContextHolderFilter dataPermissionContextHolderFilter;

//    @Value("${tca.ignoreUrls}")
//    private List<String> ignoreUrls;
    @Autowired
    private IgnoreConfig ignoreConfig;


    @GetMapping("/long2str")
    public Result<Long> long2Str() {
        long maxValue = Long.MAX_VALUE;
        log.info("long max = {}", maxValue);
        return Result.success(maxValue);
    }

    @GetMapping("/date2str")
    public Result<Date> date2Str() {
        return Result.success(new Date());
    }

    @GetMapping("/hello/{name}")
    public Result<String> hello(@PathVariable(value = "name") String name) {
        return Result.success("hello, " + name);
    }


    @PostMapping("/hello")
    public Result<String> hello(HttpServletRequest httpServletRequest,
                                @RequestBody UserReq user) {
        log.info("请求参数: {}", user);
        return Result.success("hello, " + user.getName());
    }

    @GetMapping("/autoConfiguration")
    public Result<String> testAutoConfiguration() {
        log.info(dataPermissionContextHolder.toString());
        log.info(dataPermissionContextHolderFilter.toString());
        log.info(logRecordListenerExecutorConfig.toString());
        return Result.success();
    }

    @GetMapping("/ignore")
    public Result<List<String>> testIgnoreUrls() {
        log.info("ignoreUrls: {}", ignoreConfig.getIgnoreUrls());
        return Result.success(ignoreConfig.getIgnoreUrls());
    }

}
