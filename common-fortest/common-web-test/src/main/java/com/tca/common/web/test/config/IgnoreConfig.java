package com.tca.common.web.test.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author an.zhou
 * @date 2023/5/30 13:09
 */
@Configuration
@ConfigurationProperties(prefix = "tca")
@Data
public class IgnoreConfig {

    private List<String> ignoreUrls;
}
