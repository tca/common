package com.tca.common.web.test.service;

import com.tca.common.core.bean.Result;
import com.tca.common.web.test.entity.User;
import com.tca.common.web.test.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zhoua
 * @date 2024/10/31 22:37
 */
@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public Result<User> getUser(Long id) {
        return Result.success(userMapper.selectById(id));
    }

    public Result addUser(Long id, String name, Integer age) {
        User user = new User();
        user.setAge(age);
        user.setName(name);
        user.setId(id);
        userMapper.insert(user);
        return Result.success();
    }

    public int deleteById(Long id) {
        return userMapper.deleteById(id);
    }
}
