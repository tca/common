package com.tca.common.web.test.converter;

import com.tca.common.web.test.req.UserReq;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zhoua
 * @date 2022/1/13 11:46
 */
@Slf4j
public class ImageConverter implements java.util.function.UnaryOperator<UserReq> {

    @Override
    public UserReq apply(UserReq userReq) {
        log.info("源: {}", userReq);
        userReq.setImg(null);
        return userReq;
    }
}
