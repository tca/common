package com.tca.common.web.test.config;

import com.tca.common.web.config.LogRecordListenerExecutorConfig;
import com.tca.common.web.handler.LogRecordListener;
import com.tca.common.web.handler.PrintLogRecordListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhoua
 * @date 2022/1/12 23:59
 */
@Configuration
public class LogConfiguration {

//    @Bean
//    public LogHandlerExecutorConfig logHandlerExecutorConfig() {
//        return new LogHandlerExecutorConfig();
//    }

    @Bean
    public LogRecordListener printLogRecordListener(LogRecordListenerExecutorConfig logRecordListenerExecutorConfig) {
        return new PrintLogRecordListener(logRecordListenerExecutorConfig);
    }
}
