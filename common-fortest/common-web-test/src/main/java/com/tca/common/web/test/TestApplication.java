package com.tca.common.web.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * @author zhoua
 * @date 2022/1/12 21:14
 */
@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) throws IOException {

        SpringApplication.run(TestApplication.class);
    }
}
