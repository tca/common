package com.tca.common.web.test.req;

import lombok.Data;

/**
 * @author zhoua
 * @date 2022/1/13 11:46
 */
@Data
public class UserReq {

    private String name;

    private Integer age;

    private String img;

    private Long id;
}
