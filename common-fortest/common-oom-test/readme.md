## jvm启动参数配置
// 堆空间大小配置 100M
-Xmx100M -Xms100M
// 垃圾回收器相关配置
-XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintTenuringDistribution -XX:+PrintGCApplicationStoppedTime -Xloggc:./logs/gc.log -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=10m
// OOM生成快照文件以及快照文件的路径
-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=./logs/dump.hprof 

## OOM解决思路
1.首先，需要在java启动脚本中添加相关参数：
```
 // 出现oom时产生快照dump文件
 -XX:+HeapDumpOnOutOfMemoryError
 // dump文件的路径
 -XX:HeapDumpPath=./logs/dump.hprof
```
2.出现oom时，拿到dump.hprof文件，用工具MAT打开；
3.打开文件时需要选择 Leak Suspects Report，即MAT帮助我们分析的可能会存在内存泄漏的地方；
4.如果上述没有分析出来，则需要按照如下步骤分析：
step1 内存占用过大的实例对象是什么？
```
使用MAT，点击直方图图标（Histogram），默认按照占用的内存倒序排序，比如是byte[]
```
step2 这个对象被谁引用？
```
使用MAT，点击dominator tree，会降序排序每个对象大小
```
step3 定位到具体的线程和代码？
```
使用MAT，点击thread_overview
```