package com.tca.common.oom.test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhoua
 * @date 2023/10/14 11:40
 */
public class OomStarter {

    private static final Integer MAX_COUNT = 1000;

    private static final List<byte[]> list = new ArrayList<>();

    public static void main(String[] args) {
        byte[] bytes;

        for (int i = 0; i < MAX_COUNT; i++) {
            System.out.println("第 " + i + " 次添加1M数组");
            bytes = new byte[1024 * 1024];
            list.add(bytes);
        }

    }
}
