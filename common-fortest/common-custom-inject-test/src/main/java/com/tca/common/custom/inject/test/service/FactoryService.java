package com.tca.common.custom.inject.test.service;

/**
 * @author zhoua
 * @date 2024/9/7 12:51
 */
public interface FactoryService {

    /**
     * getName
     * @return
     */
    public String getName();
}
