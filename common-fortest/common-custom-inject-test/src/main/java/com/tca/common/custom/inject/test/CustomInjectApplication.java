package com.tca.common.custom.inject.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhoua
 * @date 2024/9/7 12:50
 */
@SpringBootApplication
public class CustomInjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomInjectApplication.class, args);
    }
}
