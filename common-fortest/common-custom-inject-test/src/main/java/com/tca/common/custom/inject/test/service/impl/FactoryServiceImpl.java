package com.tca.common.custom.inject.test.service.impl;

import com.tca.common.custom.inject.test.service.FactoryService;
import org.springframework.stereotype.Service;

/**
 * @author zhoua
 * @date 2024/9/7 12:52
 */
@Service
public class FactoryServiceImpl implements FactoryService {

    @Override
    public String getName() {
        return "default";
    }
}
