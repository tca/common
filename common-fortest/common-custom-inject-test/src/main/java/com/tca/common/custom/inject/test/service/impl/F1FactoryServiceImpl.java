package com.tca.common.custom.inject.test.service.impl;

import com.tca.common.custom.inject.annotation.FactoryCode;
import org.springframework.stereotype.Service;

/**
 * @author zhoua
 * @date 2024/9/7 12:53
 */
@Service
@FactoryCode(value = "F1")
public class F1FactoryServiceImpl extends FactoryServiceImpl {
    @Override
    public String getName() {
        return "F1";
    }
}
