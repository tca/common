package com.tca.common.custom.inject.test.controller;

import com.tca.common.core.bean.Result;
import com.tca.common.custom.inject.test.service.FactoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhoua
 * @date 2024/9/7 12:54
 */
@RestController
@RequestMapping("/factory")
public class FactoryController {

    @Autowired
    private FactoryService factoryService;

    @GetMapping("/name")
    public Result<String> getName() {
        return Result.success(factoryService.getName());
    }
}
