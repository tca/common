package com.tca.common.spring.test.starter.application.listener;

import com.alibaba.fastjson.JSONObject;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * @author an.zhou
 * @date 2023/5/22 15:23
 */
public class CustomContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        System.out.println(JSONObject.toJSONString(event));
    }
}
