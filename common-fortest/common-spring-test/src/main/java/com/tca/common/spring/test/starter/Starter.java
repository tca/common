package com.tca.common.spring.test.starter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author an.zhou
 * @date 2023/4/28 10:09
 */
public class Starter {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(StarterConfiguration.class);
        Student student = applicationContext.getBean(Student.class);
    }
}
