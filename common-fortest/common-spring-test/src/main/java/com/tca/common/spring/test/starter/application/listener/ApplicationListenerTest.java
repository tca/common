package com.tca.common.spring.test.starter.application.listener;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author an.zhou
 * @date 2023/5/22 15:20
 */
public class ApplicationListenerTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(CustomContextRefreshedListener.class);
        applicationContext.refresh();
    }
}
