package com.tca.common.spring.test.starter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author an.zhou
 * @date 2023/4/28 10:10
 */
@Configuration
public class StarterConfiguration {

    @Bean
    public Student student() {
        return new Student(1L,"张三");
    }
}
