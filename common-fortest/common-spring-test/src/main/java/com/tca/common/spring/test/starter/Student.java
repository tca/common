package com.tca.common.spring.test.starter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author an.zhou
 * @date 2023/4/28 10:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    /**
     * id
     */
    private Long id;

    /**
     * name
     */
    private String name;
}
