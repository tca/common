# 解压

## 1.RAR解压【支持rar5】

### 1.1 maven依赖

```xml
<!-- unrar -->
<dependency>
    <groupId>net.sf.sevenzipjbinding</groupId>
    <artifactId>sevenzipjbinding</artifactId>
    <version>16.02-2.01</version>
</dependency>
<dependency>
    <groupId>net.sf.sevenzipjbinding</groupId>
    <artifactId>sevenzipjbinding-all-platforms</artifactId>
    <version>16.02-2.01</version>
</dependency>
```

### 1.2 参考文档

[7-zip](http://sevenzipjbind.sourceforge.net/extraction_snippets.html)