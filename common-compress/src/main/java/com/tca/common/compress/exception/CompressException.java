package com.tca.common.compress.exception;

/**
 * @author zhoua
 * @Date 2021/9/22
 */
public class CompressException extends RuntimeException {

    public CompressException() {
    }

    public CompressException(String message) {
        super(message);
    }

    public CompressException(String message, Throwable cause) {
        super(message, cause);
    }
}
