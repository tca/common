package com.tca.common.compress.config;

import com.tca.common.compress.impl.RarAdapter;
import com.tca.common.compress.impl.ZipAdapter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author zhoua
 * @Date 2021/9/22
 */
@Configuration
@Import({RarAdapter.class, ZipAdapter.class})
public class CompressAutoConfiguration {

}
