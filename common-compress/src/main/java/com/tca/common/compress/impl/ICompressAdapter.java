package com.tca.common.compress.impl;


import com.tca.common.core.bean.Result;

/**
 * @author zhouan
 * @Date 2021/9/11
 */
public interface ICompressAdapter {

    /**
     * supports
     * @param fileName
     * @return
     */
    boolean supports(String fileName);

    /**
     * uncompress
     * @param fileName
     * @param dir 解压后的文件路径
     * @return 解压后的文件全路径
     */
    Result<String> uncompress(String fileName, String dir);

    /**
     * uncompress 默认解压到当前fileName文件所在路径
     * @param fileName
     * @return 解压后的文件全路径
     */
    Result<String> uncompress(String fileName);
}
