# 日志框架

## 1.日志框架

### 1.1 日志jar分类

日志相关的jar可以基本分为三类

#### 接口类

只提供api定义，没有提供具体实现，目的是为应用层提供标准化的使用方式。

    jcl(Jakarta commons logging) -- commons-logging
    slf4j

#### 实现类

具体的日志实现类，提供对日志的收集/管理功能

```
jul -- jdk-logging(jdk自带)
log4j
log4j2
logback
```

#### 桥接类

多种日志实现框架混用的情况下，需要借助桥接类进行日志的转换，最后统一成一种进行输出

```
slf4j-jdk14
slf4j-log4j2
log4j-slf4j-impl
logback-classic
slf4j-jcl
jul-to-slf4j
log4j-over-slf4j
jcl-over-slf4j
log4j-to-slf4j
```

### 1.2 日志框架演化

#### log4j

```
Apache基金会最早实现的一套日志框架, 在Java1.4之前只有这一种选择。这是第一套日志框架。
log4j 在设计上非常优秀, 对后续的 java log 框架有长久而深远的影响, 它定义的Logger、Appender、Level等概念如今已经被广泛使用。Log4j 的短板在于性能, 在logback 和 log4j2 出来之后, log4j的使用也减少了。
```

#### jul 

```
jul(java.util.logging), 是jdk1.4推出的日志框架实现。
```

#### jcl

```
jcl(jakarta-commons-logging), 因为有了两种选择, 所以导致了日志使用的混乱。所以Apache推出了jcl。
它只是定义了一套日志接口, 支持运行时动态加载日志组件。应用层编写代码时, 只需要使用jcl提供的统一接口来记录日志, 在程序运行时会优先找系统是否集成log4j, 如果集成则使用log4j作为日志实现, 如果没找到则使用 jul 作为日志实现。jul 的出现解决了多种日志框架共存的尴尬, 也是面向接口编程思想的一种具体体现。
不过, jcl(commons-logging.jar)对log4j和jul的配置问题兼容的并不好, 使用commons-loggings还可能会遇到类加载问题, 导致NoClassDefFoundError的错误出现。
```

#### slf4j

```
log4j作者离开apache后, 推出了一套类似于 jcl 的接口类框架, 即 slf4j。原因是作者觉得 jcl 这套接口设计的不好, 容易让开发者写出有性能问题的代码。slf4j 作为一套标准接口, 可以实现无缝与多种实现框架进行对接。它也是现在比较常用的日志集成方式。
```

#### logback

```
log4j作者在推出slf4j后, 顺带开发了slf4j的默认实现, 即为logback。即logback是默认实现slf4j的接口的。
当前分为三个目标模块：
	logback-core：核心模块, 是其它两个模块的基础模块
	logback-classic：是log4j的一个改良版本, 同时完整实现 slf4j api, 使你可以很方便地更换成其它日记系统如			log4j 或 jul
	logback-access：访问模块与Servlet容器集成提供通过Http来访问日记的功能, 是logback不可或缺的组成部分
```

#### log4j2

```
apache推出的新的日志框架, 性能最高, 和 log4j1.x 并不兼容, 设计上很大程度上模仿了 SLF4J/Logback
```



## 2.JCL (Jakarta commons logging)

### 2.1 简介

```
Jakarta Commons Logging (JCL)提供的是一个日志接口, 同时兼顾轻量级和不依赖于具体的日志实现工具。它提供给中间件/日志工具开发者一个简单的日志操作抽象, 允许程序开发人员使用不同的具体日志实现工具
```

### 2.2 原理

```
当commons-logging.jar被加入到CLASSPATH之后, 它会合理地猜测你想用的日志工具, 然后进行自我设置, 用户根本不需要做任何设置。默认的LogFactory是按照下列的步骤去发现并决定那个日志工具将被使用(寻找过程会在找到第一个工具时中止):
        1.寻找当前factory中名叫org.apache.commons.logging.Log配置属性的值
        2.寻找系统中属性中名叫org.apache.commons.logging.Log的值
        3.如果应用程序的classpath中有log4j, 则使用相关的包装类-Log4JLogger
        4.如果应用程序运行在jdk1.4的系统中，使用相关的包装类-Jdk14Logger
        5.使用简易日志包装类SimpleLog
```

### 2.3 使用

```
导入log4j-jcl, 再导入具体的日志实现框架 如 log4j，log4j2等, 并配置对应的日志文件即可
```



## 3.slf4j

### 3.1 简介

```
slf4j跟jcl类似, 也是提供了一个日志接口, slf4j只做两件事:
        1.提供日志接口
        2.提供获取具体日志对象的方法
```



## 4.模块详解

### 4.1 common-log-base

```
该模块提供了一些日志框架的核心工具类, 用于信息脱敏等
```

### 4.2 common-log-jcl-log4j2

```
该模块提供了 jcl 为接口, log4j2 为实现的方式, 同时提供了日志脱敏实现。由于jcl框架本身存在的一些问题, 所以一般也不建议使用。
```

### 4.3 common-log-slf4j-log4j2

```
该模块提供了 slf4j 为接口, log4j2 为实现的方式, 同时提供了日志脱敏实现。
```

### 4.4 common-log-slf4j-logback

```
该模块提供了 slf4j 为接口, logback 为实现的方式, 同时提供了日志脱敏实现。
```

### 4.5 common-log-springboot-log4j2

```
该模块提供了 springboot 中以 slf4j 为接口, log4j2 为实现的方式, 同时提供了日志脱敏实现。
```

### 4.6 common-log-springboot-logback

```
该模块提供了 springboot 中以 slf4j 为接口, logback 为实现的方式, 同时提供了日志脱敏实现。
```



## 5.其他

### 5.1 log4j2

#### 5.1.1 主配置文件-log4j2.xml

[log4j2.xml主配置文件参考](https://blog.csdn.net/zheng0518/article/details/69558893?utm_medium=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromBaidu-1.not_use_machine_learn_pai&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromBaidu-1.not_use_machine_learn_pai)

#### 5.1.2 插件 - converters

[converters](https://blog.csdn.net/lqzkcx3/article/details/82082274)

### 5.2 logback

#### 5.2.1 主配置文件-logback.xml

[logback.xml主配置文件参考](https://www.cnblogs.com/gavincoder/p/10091757.html)

#### 5.2.2 logback脱敏实现

[日志脱敏实现参考](https://blog.csdn.net/fywfengyanwei/article/details/78484590)

###  5.3 springboot集成日志框架

#### 5.3.1 使用

1.spring-boot-starter-web默认使用spring-boot-starter-logging (jcl集成logback)

2.使用common-log-springboot-slg4j2 需要将spring-boot-starter-logging排除

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <exclusions>
        <exclusion>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-logging</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

#### 5.3.2 日志级别配置

```yaml
logging:
    level:
    	root: INFO
    	org.springframework: ERROR
```

### 5.4 常见问题

#### 5.4.1 使用slf4j stackoverflow问题

```
log jar分类:
   接口: 
       commons-logging-api(jcl) 
       java.util.logging(jul) 
       slf4j 
       log4j-api(包括log4j1和log4j2)
   实现: 
       logback(默认实现了slf4j)
       log4j
       log4j2
       commons-logging-impl
   接口到实现的适配器:
       slf4j-jcl(桥接 jcl接口和log4j2实现)
       slf4j-log4j12-impl(桥接 slf4j接口和log4j2实现)
   实现转化接口的适配器:
       jcl-over-slf4j(将已有的jcl的接口实现强转为slf4j的接口实现)
       jul-to-slf4j(将已有的jul的接口实现强转为slf4j的接口实现)
       log4j-to-slf4j(将已有的log4j2的强转为slf4j的接口实现) 
   问题解析: 不能同时引入common-log-springboot-log4j2 和 common-log-springboot-logback
   common-log-springboot-log4j2中有:
       log4j-core(log4j2实现)
       slf4j-log4j12-impl(桥接log4j2和slf4j)
       (即默认使用slf4j接口, log4j2实现)
   common-log-springboot-logging中有:
      slf4j-api(slf4j接口)
      logback(slf4j接口实现)
      log4j-to-slf4j(将已有的log4j2的实现强转为slf4j的接口实现)
      (即默认会将log4j2的实现转化为slf4j接口[如果存在], 再用logback的具体实现实现)
   此时出现了 slf4j-log4j12-impl 和  log4j-to-slf4j 两个jar包
   [The purpose of slf4j-log4j12 module is to delegate or redirect calls made to an SLF4J logger to log4j. 
   The purpose of the log4j-over-slf4j module is to redirect calls made to a log4j logger to SLF4J. 
   If SLF4J is bound withslf4j-log4j12.jar and log4j-over-slf4j.jar is also present on the class path, a Stack-
   OverflowError will inevitably occur immediately after the first invocation of an SLF4J or a log4j logger.]

```

[参考](https://www.jianshu.com/p/191a95ad0b89)

[官网参考](http://www.slf4j.org/codes.html#multiple_bindings)

#### 5.4.2 使用slf4j多绑定问题

```
SLF4J: Found binding in [jar:file:/E:/java_install/apache-maven-repository/org/apache/logging/log4j/log4j-slf4j-impl/2.11.2/log4j-slf4j-impl-2.11.2.jar!/org/slf4j/impl/StaticLoggerBinder.class]
        SLF4J: Found binding in [jar:file:/E:/java_install/apache-maven-repository/ch/qos/logback/logback-classic/1.2.3/logback-classic-1.2.3.jar!/org/slf4j/impl/StaticLoggerBinder.class]
        SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation
解答：找到多个slf4j的实现, 即有多个StaticLoggerBinder.class, jvm会随机加载一个
```

#### 5.4.3 使用
```
注意使用时要将第三方依赖的日志框架移除, 否则可能造成冲突导致日志框架不生效
```

#### 5.4.4 以下这段配置的作用
```xml
<loggers>
    <!--过滤掉spring和mybatis的一些无用的DEBUG信息-->
    <logger name="org.springframework" level="INFO"></logger>
    <logger name="org.mybatis" level="INFO"></logger>
    <root level="debug">
        <appender-ref ref="consoleLog"/>
        <appender-ref ref="infoLog"/>
    </root>
</loggers>
```
上面这个配置有两个作用:
第一, org.springframework和org.mybatis这两个包的日志级别为info, 此外其他包的日志级别为debug.
第二, 对于默认其他包的日志输出, 使用两个添加器: 1.consoleLog, 即输出在控制台; 2.infoLog, 输出到日志文件。那为什么
    org.springframework和org.mybatis没有配置添加器呢? 因为没有配置则使用root的日志添加器, 即也是consoleLog和infoLog