## 说明
1.h2可以作为内嵌数据库, 且可以不用安装服务端, 直接通过java依赖即可, 所以可以用作本地测试使用

2.h2数据库语法与mysql类似, 但是注意建表语句, 不要有存储引擎、数据集这些

3.常用配置如下(集成spring-boot):
jdk依赖
```xml
<dependency>
    <groupId>com.h2database</groupId>
    <artifactId>h2</artifactId>
    <scope>runtime</scope>
</dependency>
```

连接配置
```yaml
spring:
  datasource:
    # 驱动类
    driver-class-name: org.h2.Driver
    # 初始化建表语句 - 在服务启动时执行
    schema: classpath:db/schema-h2.sql
    # 初始化表数据 - 在服务启动时执行
    data: classpath:db/data-h2.sql
    # url中jdbc:h2是固定的, mem表示内存存储, 后面的h2表示数据库名称, 可以自定义
    url: jdbc:h2:mem:h2
    # 用户名密码是用于控制台登录时使用
    username: root
    password: 123456
```

控制台配置
```yaml
spring:
  h2:
    console:
      # 是否开启UI控制台
      enabled: true
      # 控制台地址
      path: /h2
      settings:
        trace: false
        # 是否允许远程连接，false表示只能本机连接
        web-allow-others: false
```

