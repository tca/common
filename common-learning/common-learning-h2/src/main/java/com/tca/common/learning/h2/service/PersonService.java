package com.tca.common.learning.h2.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.tca.common.learning.h2.entity.PersonEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author <a href="https://www.fengwenyi.com?code>lebin@hfitech.com</a>
 * @since 2022-12-10
 */
public interface PersonService extends IService<PersonEntity> {

}
