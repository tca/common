package com.tca.common.learning.h2.controller;


import com.tca.common.learning.h2.entity.PersonEntity;
import com.tca.common.learning.h2.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author <a href="https://www.fengwenyi.com?code>lebin@hfitech.com</a>
 * @since 2022-12-10
 */
@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/list")
    public List<PersonEntity> list() {
        return personService.list();
    }

}

