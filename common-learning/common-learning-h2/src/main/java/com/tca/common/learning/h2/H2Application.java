package com.tca.common.learning.h2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhoua
 * @date 2022/12/10 22:40
 */
@SpringBootApplication
@MapperScan(basePackages = "com.tca.common.learning.h2.dao")
public class H2Application {

    public static void main(String[] args) {
        SpringApplication.run(H2Application.class);
    }
}
