package com.tca.common.learning.h2.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tca.common.learning.h2.entity.PersonEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author <a href="https://www.fengwenyi.com?code>lebin@hfitech.com</a>
 * @since 2022-12-10
 */
public interface PersonMapper extends BaseMapper<PersonEntity> {

}
