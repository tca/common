package com.tca.common.learning.h2.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tca.common.learning.h2.dao.PersonMapper;
import com.tca.common.learning.h2.entity.PersonEntity;
import com.tca.common.learning.h2.service.PersonService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author <a href="https://www.fengwenyi.com?code>lebin@hfitech.com</a>
 * @since 2022-12-10
 */
@Service
public class PersonServiceImpl extends ServiceImpl<PersonMapper, PersonEntity> implements PersonService {

}
