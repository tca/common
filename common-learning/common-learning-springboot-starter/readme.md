# spring-boot-starter机制

## springboot starter简介

```
starter是springboot中一个非常重要的概念，starter相当于模块，它能将模块所需的依赖整合起来并对模块内的Bean根据环境（条件）进行自动配置。
使用者只需要依赖相应功能的starter即可，无需做过多的配置和依赖，spring boot就能自动扫描并加载相应的模块。

总结：
1.starter整合了这个模块需要的所有依赖库；
2.提供对模块的配置项给使用者、并可以对配置项提供默认值，使得使用者可以不指定配置时提供默认配置项，也可以根据需要
    指定配置项值；
3.提供自动配置类对模块内的Bean进行自动配置

比如，在maven的依赖中加入spring-boot-starter-web就能使项目支持Spring MVC，并且spring boot还为我们做了很多默认配置，比如
springmvc中的相关组件，无需再依赖spring-web、spring-webmvc等相关包及做相关配置就能立即使用起来
```

## starter有哪些元素
### 1.mybatis-spring-boot-starter.jar
![mybatis-spring-boot-starter.jar结构图](./img/mybatis-spring-boot-starter的jar包结构.png)
#### 说明
```
可以看到：
1.当前jar里没有自定义代码！
2.META-INF/pom.properties 配置了maven的项目groupId、artifactId以及version
3.META-INF/pom.xml 配置了当前jar所依赖的jar包
4.META-INF/MANIFEST-MF 描述了当前jar的元信息
```

#### pom.xml
```xml
<dependencies>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-jdbc</artifactId>
    </dependency>
    <dependency>
      <groupId>org.mybatis.spring.boot</groupId>
      <artifactId>mybatis-spring-boot-autoconfigure</artifactId>
    </dependency>
    <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis</artifactId>
    </dependency>
    <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis-spring</artifactId>
    </dependency>
</dependencies>
```
**说明**
```
可以看出，pom.xml文件中引入了一些jar。其中除了mybatis原生的jar，mybatis整合spring的jar（原理和源码在spring源码部分）
，还有两个：spring-boot-starter 以及 mybatis-spring-boot-autoconfigure。接下来首先分析
mybatis-spring-boot-autoconfigure。
```

#### mybatis-spring-boot-autoconfigure.jar

![mybatis-spring-boot-autoconfigure.jar](./img/mybatis-spring-boot-autoconfigure的jar包结构.png)



**说明**

```
可以看到，里面包含如下文件：
1.META-INF/pom.properties 配置maven所需要的groupId、artifactId、version
2.META-INF/pom.xml 配置依赖的jar
3.META-INF/MANIFEST.MF 这个文件描述了该jar文件的很多信息
4.META-INF/spring.factories springboot SPI机制读取的文件
5.META-INF/spring-configuration-metadata.json 系统自动生成的ide提示功能
6.META-INF/additional-spring-configuration-metadata.json 自定义添加的ide提示功能
7.一些java对象：MybatisAutoConfiguration、MybatisLanguageDriverAutoConfiguration 自动配置对象，这块在springboot SPI机制中说到
        一般我们会在其中定义一些Bean，同时使用@ConditionalOnXXX注解，并在spring.factories文件中配置
```



**spring-configuration-metadata.json & additional-spring-configuration-metadata.json**

```
这两个json文件有什么关系呢？有什么区别呢？
如果pom.xml中引入了spring-boot-configuration-processor包，则会自动生成spring-configuration-metadata.json，ide就会对其中的内容
进行提示。
如果需要手动修改里面的元数据，则可以在additional-spring-configuration-metadata.json中编辑
最终，两个文件中的元数据会合并到一起
```



#### 总结

##### springboot-starter包的定义规则&编写规范

```
1.需要定义个名称为xxx-spring-boot-starter【这里命名是有规范的：对于spring的内部项目，比如web，命名为spring-boot-starter-web，即spring-boot-starter-xxx，对于mybatis这样的spring的外部项目，如果要集成到spring-boot中，则使用xxx-spring-boot-starter】的空项目，里面不包含任何代码，可以有pom.xml和pom.properties文件
2.pom.xml文件中包含了名称为xxx-spring-boot-autoconfigure的jar，该jar中一般包括xxxAutoConfiguration的配置类，该配置类中定义了一些bean实例。并且使用ConditionalOnXXX注解，spring容器启动时会根据条件判断当前bean是否需要生成并放入容器中管理。同时，在spring.factories配置文件中增加key为EnableAutoConfiguration，value为xxxAutoConfiguration的自动配置类
```



![结构图](./img/xxx-spring-boot-starter.png)



### 2.spring-boot-starter-web.jar

![spring-boot-starter-web.jar](./img/spring-boot-starter-web的jar包结构.png)

#### 说明

```
可以看到：
当前jar中既没有代码，连pom.xml文件也没有，这里pom.xml文件单独以spring-boot-starter-web-2.6.1.pom存储！
```

#### spring-boot-starter-web-2.6.1.pom

```xml
<dependencies>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter</artifactId>
      <version>2.6.1</version>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-json</artifactId>
      <version>2.6.1</version>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-tomcat</artifactId>
      <version>2.6.1</version>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-web</artifactId>
      <version>5.3.13</version>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>5.3.13</version>
      <scope>compile</scope>
    </dependency>
 </dependencies>
```

**说明**

```
按照上述的说法，spring-boot-starter-web应该会依赖类似于 spring-boot-web-autoconfigure，但是实际上没有。原因是：当前项目是spring内部的项目（类似的项目，如spring-boot-starter-jdbc、spring-boot-starter-data-redis），spring内部项目的autoconfigure以及对应autoconfigure包中的xxxAutoConfiguration配置类以及spring.factories配置文件都在统一的包：spring-boot-autoconfigure 中！！！
```

#### spring-boot-autoconfigure.jar

![spring-boot-autoconfigure.jar](./img/spring-boot-autoconfigure的jar包结构.png)

**说明**

```
可以看到：
这个jar中有对应的spring.factories文件以及定义所有的xxxAutoConfiguration类！！！
```



## 自定义stater

### 说明

```
其实上述已经说明了：
1.需要定义个名称为xxx-spring-boot-starter【这里命名是有规范的：对于spring的内部项目，比如web，命名为spring-boot-starter-web，即spring-boot-starter-xxx，对于mybatis这样的spring的外部项目，如果要集成到spring-boot中，则使用xxx-spring-boot-starter】的空项目，里面不包含任何代码，可以有pom.xml和pom.properties文件
2.pom.xml文件中包含了名称为xxx-spring-boot-autoconfigure的jar，该jar中一般包括xxxAutoConfiguration的配置类，该配置类中定义了一些bean实例。并且使用ConditionalOnXXX注解，spring容器启动时会根据条件判断当前bean是否需要生成并放入容器中管理。同时，在spring.factories配置文件中增加key为EnableAutoConfiguration，value为xxxAutoConfiguration的自动配置类

但是需要补充：
additional-spring-configuration-metadata.json文件，在这个文件中进行配置后，当被其他包引用时，ide使用到其中的配置会有提示！
```

### additional-spring-configuration-metadata.json文件编写

[参考](https://blog.csdn.net/z69183787/article/details/108987431?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-108987431-blog-86681010.235%5Ev30%5Epc_relevant_default_base3&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-108987431-blog-86681010.235%5Ev30%5Epc_relevant_default_base3&utm_relevant_index=2)

[官方文档参考](https://docs.spring.io/spring-boot/docs/2.1.7.RELEASE/reference/html/configuration-metadata.html#configuration-metadata-format)