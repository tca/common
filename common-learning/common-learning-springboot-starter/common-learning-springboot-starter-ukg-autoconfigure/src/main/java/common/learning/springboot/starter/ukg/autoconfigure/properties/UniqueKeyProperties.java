package common.learning.springboot.starter.ukg.autoconfigure.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author an.zhou
 * @date 2023/4/21 10:29
 */
@Data
@ConfigurationProperties(prefix = "tca.uk")
public class UniqueKeyProperties {

    /**
     * 前缀
     */
    private String prefix = "default_prefix";

    /**
     * 后缀
     */
    private String suffix = "default_suffix";
}
