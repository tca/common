package common.learning.springboot.starter.ukg.autoconfigure;

import com.tca.common.learning.springboot.starter.ukg.service.UniqueKeyGenerator;
import common.learning.springboot.starter.ukg.autoconfigure.properties.UniqueKeyProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author an.zhou
 * @date 2023/4/21 14:46
 */
@Configuration
@EnableConfigurationProperties(value = {UniqueKeyProperties.class})
public class UkgAutoConfiguration {

    @Autowired
    private UniqueKeyProperties uniqueKeyProperties;

    @Bean
    @ConditionalOnMissingBean(UniqueKeyGenerator.class)
    public UniqueKeyGenerator uniqueKeyGenerator(UniqueKeyProperties uniqueKeyProperties) {
        return new UniqueKeyGenerator(uniqueKeyProperties.getPrefix(), uniqueKeyProperties.getSuffix());
    }


}
