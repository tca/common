package com.tca.common.learning.springboot.starter.ukg.service;

import java.util.UUID;

/**
 * @author an.zhou
 * @date 2023/4/21 10:28
 */
public class UniqueKeyGenerator {

    private final String prefix;

    private final String suffix;

    public UniqueKeyGenerator(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix= suffix;
    }

    public String getUniqueKey() {
        return this.prefix + "-" + UUID.randomUUID().toString() + "-"
                + this.suffix;
    }

}
