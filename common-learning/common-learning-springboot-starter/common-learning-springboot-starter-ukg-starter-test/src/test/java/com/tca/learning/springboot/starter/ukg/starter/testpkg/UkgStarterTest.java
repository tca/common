package com.tca.learning.springboot.starter.ukg.starter.testpkg;

import com.tca.common.learning.springboot.starter.ukg.service.UniqueKeyGenerator;
import com.tca.learning.springboot.starter.ukg.starter.test.UkgStarterApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author an.zhou
 * @date 2023/4/21 15:24
 */
@SpringBootTest(classes = {UkgStarterApplication.class})
@RunWith(SpringRunner.class)
public class UkgStarterTest {

    @Autowired
    private UniqueKeyGenerator uniqueKeyGenerator;

    @Test
    public void testUkg() {
        System.out.println(uniqueKeyGenerator.getUniqueKey());
    }
}
