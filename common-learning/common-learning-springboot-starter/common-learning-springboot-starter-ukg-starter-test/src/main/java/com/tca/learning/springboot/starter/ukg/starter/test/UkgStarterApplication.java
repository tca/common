package com.tca.learning.springboot.starter.ukg.starter.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author an.zhou
 * @date 2023/4/21 15:16
 *
 */
@SpringBootApplication
public class UkgStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(UkgStarterApplication.class, args);
    }
}
