# 下载运行卸载
## 在线下载
```shell
curl -O https://alibaba.github.io/arthas/arthas-boot.jar
```
## 运行
```shell
java -jar arthas-boot.jar
```
注意: 
1.必须有启动的jvm进程
2.第一次运行时, 会下载arthas相关的lib、脚本等, 安装目录默认在:
C:\Users\DELL\.arthas\lib\4.0.4\arthas
3.运行后, 需要选择attach的jvm进程

## 离线下载

## 卸载
### 删除安装目录
C:\Users\DELL\.arthas\lib\4.0.4\arthas
### 删除日志目录
C:\Users\DELL\logs\arthas

# 快速入门
## attach一个jvm进程
step1 启动一个jvm进程
step2 java -jar arthas-boot.jar
step3 选择对应的进程号

## 启动多个arthas进程, 指定端口启动(默认端口3658)
```shell
java -jar arthas-boot.jar --telnet-port 9999
```

## 再次进入arthas交互窗口
```shell
telnet localhost 3658
```
这里也可以通过浏览器直接访问

# 常用命令学习
## dashboard
没有参数, 可以直接使用, 可以查看**线程**, **内存**等使用情况, 默认每s刷新
使用q或ctrl+c退出

## thread
查看jvm线程信息
### 常用参数
* thread [线程id] - 查看查看线程详情
* thread [-n] [线程数] - 显示cpu占比较高的线程 eg: thread n 3
* thread [-b] - 显示处于阻塞状态的线程
* thread [-i] [时间s] - 每隔xx时间输出

## jad
反编译源码, 需要配合参数使用
### 常用参数
jad [class全限定名] - 反编译当前类
jad [class全限定名] [方法] - 只反编译某个方法

## watch
监控方法, 需要配合参数使用
### 常用参数
watch [class全限定名] [方法名] [返回值]
### 基本操作
* watch [class全限定名] [方法名] '{params, returnObj}' -x 2 -- 这里的'{params, returnObj}'是ognl表达式, params表示入参, 
returnObj表示出参, -x 2表示能看到入参和出参的第二层
* watch [class全限定名] [方法名] 'target' -x 2 -- 'target'也是ognl表达式, 表示所有成员变量, 这里是监控所有属性


## quit/exit
使用quit/exit会退出arthas交互界面, 但是arthas进程不会退出(可以再次使用telnet localhost 3658进入), 
使用stop则会直接停止arthas进程

## help
查看命令如何使用
### 常用参数
help [命令名称] eg: help grep

## cat
查看文件内容, 类似于linux中的cat指令

## grep
匹配查找, 和linux里的grep命令类似, 但它只能用于管道命令
### 常用参数
* grep [-n] - 显示行号 eg: cat xxx | grep java -n
* grep [-i] - 不区分大小写 eg: cat xxx | grep java -i
* grep [-e] - 使用正则表达式

## pwd
与linux类似, 显示当前路径

## cls
清空屏幕, 类似于clear

## session
查看当前会话

## reset
对于被增强的类, 进行重置还原

## 常用快捷指令
* ctrl+a - 回到行首
* ctrl+e - 回到行尾

## jvm
查看jvm相关信息

## sysprop(system properties)
查看系统属性
### 常用参数
* sysprop [属性名称] - 查看某个属性名称的值, 可以正则匹配
* sysprop [属性名称] [属性值] - 修改属性值

## sysenv(system environment)
查看jvm相关环境变量

## vmoption
查看vm启动参数
### 常用参数
* vmoption [参数名称] - 查看某个参数
* vmoption [参数名称] [参数值] - 修改参数值

## getstatic
查看静态属性
### 常用参数
* getstatic [class全限定名] [属性名] - 查看类中的静态属性, 用的比较少, 一般都使用ognl

## ognl
ognl表达式
### 常用参数
* ognl ‘@全限定类名@静态属性名称' - 访问静态属性, eg: ognl '@com.nio.qst.common.learning.actuator.service.OgnlService@name'
* ognl '@全限定类名@静态方法名称()' - 调用静态方法(无参), eg: ognl '@com.nio.qst.common.learning.actuator.service.OgnlService@getName()'
* ognl '@全限定类名@静态方法名称(参数)' - 调用静态方法(含参), eg: ognl '@com.nio.qst.common.learning.actuator.service.OgnlService@getGreetName("hello")'

## sc(search class)
查看类的信息
### 常用参数
* sc [class-pattern] - 查找类
* sc [-d]
* sc [-f] - 显示字段信息

## sm(search method)
查看方法的信息
### 常用参数
* sm [class-pattern] [method-pattern] - 查找方法

## redefine
可以用于修改源代码！
### 基本操作
step1.jad --source-only [class的全限定名] > ./xxx.java
step2.使用vim编辑./xxx.java文件, 修改源代码文件
step3.mc ./xxx.java -d ./ (mc用于编译, 此时会生成.class文件)
step4.redefine ./xxx.class

## dump
将已加载的类的字节码文件保存到特定目录中: ${arthas-logs}/arthas/classdump/

## classloader
查看类加载器相关信息
### 基本操作
获取某个类所在的jar
classloader -c [类加载器的hashcode] -r 类的路径
eg: classloader -c 680f2737 -r java/lang/String.class
注意：这里的hashcode, 即680f2737如何得到？通过classloader -l可以查看每个classloader的hashcode

## monitor
监控指定类中方法的执行情况(每2min监控一次)
### 常用参数
* monitor [-c] [s秒] [全限定类名] [方法名] - 每隔s秒监控方法的执行情况

## trace
追踪方法内部的调用路径, 输出方法路径上的每个节点的耗时
### 常用参数
* trace [全限定类名] [方法名]

## tt
记录指定方法每次调用的入参和出参, 并能对这些不同时间下调用的信息进行观测
### 常用参数
* tt [-t] [全限定类名] [方法名]

## options
查看arthas全局属性



