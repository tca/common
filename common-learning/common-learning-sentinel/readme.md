## 雪崩
### 概念
```
微服务调用链路中的某个服务故障，引起整个链路中的所有微服务都不可用
```
### 解决方案
#### 超时处理
```
设定超时时间，请求超过一定时间没有响应，就返回错误信息，不会无休止等待
```
#### 线程隔离（舱壁模式）
```
限定每个业务可以使用的线程数，避免耗尽整个tomcat线程池资源
```
#### 熔断降级
```
由断路器统计业务执行的异常比例，如果超出阈值则会熔断该业务，拦截访问该业务的一切请求
```
#### 流量控制
```
限制业务访问的QPS，避免服务因流量突增而导致故障
```

## sentinel dashboard
### 下载
[下载](https://github.com/alibaba/Sentinel/releases)
### 启动
```shell
java -jar sentinel-dashboard-1.8.3.jar
```
### 访问
```
# 用户名/密码: sentinel/sentinel
http://localhost:8080
```
### 参数修改
```properties
server.port=8080
sentinel.dashboard.auth.username=sentinel
sentinel.dashboard.auth.password=sentinel
```
**启动jar时，使用-D添加以上参数即可**

## sentinel starter
### 微服务整合sentinel
* 添加依赖
```xml
 <!-- sentinel -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
</dependency> 
```
* 添加配置
```yaml
spring:
  cloud:
    sentinel:
      transport:
        dashboard: localhost:8080 
```
* dashboard配置


### Feign整合Sentinel
**注意：这里整合的目的是通过整合，完成服务降级**
* 添加配置
```yaml
feign:
  sentinel:
    # 开启feign的sentinel功能
    enabled: true
```
* 给FeignClient编写降级逻辑
方式一：FallbackClass，无法对远程调用的异常做处理
方式二：FallbackFactory，可以对远程调用的异常做处理
我们一般选择方式二，定义好FallbackFactory之后，需要将当前bean注册到spring容器中
* 给FeignClient配置对应的FallbackFactory

## 线程隔离
### 线程隔离的方式
方式一：信号量【sentinel默认采用信号量隔离方式】
* 轻量级、没有额外开销
* 假设A服务依赖B服务，在A服务中维护一个信号量n，当调用B服务时，信号量-1，调用完成后，信号量+1，当信号量达到0时，
    不允许继续调用B服务，在客户端实现

方式二：线程池隔离
* 基于线程池实现，有额外的开销，隔离控制更强
* 假设A服务同时依赖B服务和C服务，线程隔离的做法是在A服务内设置两个线程池，分别服务于调用B服务和C服务的业务，
    在客户端实现

## 熔断降级
熔断降级是解决雪崩问题的重要手段。其思路是由断路器统计服务调用的异常比例、慢请求比例，如果超出阈值则会熔断该服务。
即拦截访问该服务的一切请求，而当服务恢复时，断路器会放行访问该服务的请求。

## 待解决
* 自定义 RequestOriginParser 失效
* 自定义 BlockExceptionHandler 失效