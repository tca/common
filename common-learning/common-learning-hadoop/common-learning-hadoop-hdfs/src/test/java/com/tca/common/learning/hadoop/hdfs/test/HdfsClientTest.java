package com.tca.common.learning.hadoop.hdfs.test;

import com.tca.common.learning.hadoop.hdfs.HdfsClient;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;

/**
 * @author zhoua
 * @date 2023/10/28 22:02
 */
public class HdfsClientTest {

    private HdfsClient hdfsClient;

    @Before
    public void init() throws Exception {
        this.hdfsClient = new HdfsClient(new URI("hdfs://centos01:8020"), "tca");
    }

    @Test
    public void testMkdirs() {
        this.hdfsClient.mkdirs("/sanguo/shuguo");
    }

    @Test
    public void testCopyFromLocalFile() {
        this.hdfsClient.copyFromLocalFile("C:\\Users\\DELL\\Desktop\\jvm\\性能监控和调优.txt", "/sanguo/shuguo");
    }

    @Test
    public void testCopy2LocalFile() {
        this.hdfsClient.copy2LocalFile("/sanguo/shuguo/性能监控和调优.txt", "C:\\Users\\DELL\\Desktop\\jvm\\性能监控和调优222.txt");
    }

    @Test
    public void testRename() {
        this.hdfsClient.rename("/sanguo/readme.txt", "/sanguo/README.txt");
    }

    @Test
    public void testDelete() {
        this.hdfsClient.delete("/sanguo/shuguo");
    }

    @Test
    public void testListFiles() {
        this.hdfsClient.listFiles("/");
    }


}
