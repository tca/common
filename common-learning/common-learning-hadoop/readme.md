## MapTask工作机制
MapTask一共分为5个阶段：
read阶段: 在接收到文件分片之后，默认使用TextInputFormat进行文件读取
map阶段：在读取文件后, 进入到map阶段, 即我们自定义的Mapper
collect阶段: 经过Mapper后, 将Mapper的输出放入环形缓冲区, 环形缓冲区中一侧存入数据, 一侧存数据对应的元数据
                环形缓冲区的大小达到80%后, 进行溢写(写入文件), 溢写前需要【分区】,同时会对元数据进行
                排序。
溢写阶段: 在分区和排序后, 进行溢写
merge阶段：溢写时, 需要进行归并排序

MapTask的并行度由切片个数决定！


## ReduceTask工作机制
ReduceTask一共分为3个阶段:
Copy阶段: 将上面merge阶段之后的文件进行拉取
Sort阶段: 拉取到内存的文件, 进行排序
Reduce阶段: 排序后的数据进行处理并输出

ReduceTask的并行度可以由参数指定！
ReduceTask = 0, 表示没有Reduce阶段, 输出文件为Map的输出;
ReduceTask = 1(默认), 输出文件个数为1;
如果数据分布不均匀, 就有可能在Reduce阶段产生数据倾斜

## Partitioner分区个数 & NumberReduceTask ReduceTask个数关系
默认情况下, numberReduceTask = 1, 此时只会走一个内部类的Partitioner,
该Partitioner默认返回0, 即默认情况下输出为一个文件;
如果我们设置numberReduceTask > 1, 则默认的分区策略为HashPartitioner, 即：key的hash值%numberReduceTask;


实际项目中, numberReduceTask一般情况下要等于分区的个数(每个ReduceTask拉对应分区的数据, 
如果分区多了, 就报错, 如果分区少了, 则多出的ReduceTask拉出的文件是空的, 输出也为空文件)
如果 1 < numberReduceTask < 分区个数, 报错
如果 numberReduceTask > 分区个数, 则会生成部分空文件
如果 numberReduceTask = 1, 则不会走分区