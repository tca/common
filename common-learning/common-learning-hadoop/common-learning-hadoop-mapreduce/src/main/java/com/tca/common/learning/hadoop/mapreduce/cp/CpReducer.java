package com.tca.common.learning.hadoop.mapreduce.cp;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author zhoua
 * @date 2023/11/5 13:01
 */
public class CpReducer extends Reducer<CpFlowBean, Text, Text, CpFlowBean> {

    @Override
    protected void reduce(CpFlowBean key, Iterable<Text> values, Reducer<CpFlowBean, Text, Text, CpFlowBean>.Context context) throws IOException, InterruptedException {
        for (Text phone : values) {
            context.write(phone, key);
        }
    }
}
