package com.tca.common.learning.hadoop.mapreduce.cp;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author zhoua
 * @date 2023/11/5 12:57
 */
public class CpMapper extends Mapper<LongWritable, Text, CpFlowBean, Text> {

    private CpFlowBean outKey = new CpFlowBean();

    private Text outValue = new Text();

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, CpFlowBean, Text>.Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] wordsArr = line.split(" ");

        String phone = wordsArr[1];
        String upFlow = wordsArr[3];
        String downFlow = wordsArr[4];

        outKey.setUpFlow(Long.parseLong(upFlow));
        outKey.setDownFlow(Long.parseLong(downFlow));
        outKey.setTotalFlow();

        outValue.set(phone);

        context.write(outKey, outValue);
    }
}
