package com.tca.common.learning.hadoop.mapreduce.cp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author zhoua
 * @date 2023/11/5 12:51
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CpFlowBean implements WritableComparable<CpFlowBean> {

    private Long upFlow;

    private Long downFlow;

    private Long totalFlow;

    public void setTotalFlow() {
        this.totalFlow = this.upFlow + this.downFlow;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeLong(upFlow);
        out.writeLong(downFlow);
        out.writeLong(totalFlow);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.upFlow = in.readLong();
        this.downFlow = in.readLong();
        this.totalFlow = in.readLong();
    }

    @Override
    public String toString() {
        return this.upFlow + " " + this.downFlow + " " + this.totalFlow;
    }

    @Override
    public int compareTo(CpFlowBean o) {
        if (this.totalFlow > o.getTotalFlow()) {
            return 1;
        } else if (this.totalFlow < o.getTotalFlow()) {
            return -1;
        }
        if (this.upFlow > o.getUpFlow()) {
            return 1;
        } else if (this.upFlow < o.getUpFlow()){
            return -1;
        }
        return 0;
    }
}
