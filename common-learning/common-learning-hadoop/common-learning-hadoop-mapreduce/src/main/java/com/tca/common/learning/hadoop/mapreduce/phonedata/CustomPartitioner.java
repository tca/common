package com.tca.common.learning.hadoop.mapreduce.phonedata;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * @author zhoua
 * @date 2023/11/5 11:26
 * Partitioner的泛型为Mapper输出的key、value对应的泛型
 */
public class CustomPartitioner extends Partitioner<Text, FlowBean> {

    private static final String PHONE_135 = "135";
    private static final String PHONE_136 = "136";

    @Override
    public int getPartition(Text text, FlowBean flowBean, int i) {
        String phone = text.toString();

        String prePhone = phone.substring(0, 3);

        if (PHONE_135.equals(prePhone)) {
            return 0;
        } else if (PHONE_136.equals(prePhone)) {
            return 1;
        } else {
            return 2;
        }
    }
}
