package com.tca.common.learning.hadoop.mapreduce.wordcount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author zhoua
 * @date 2023/10/28 23:32
 * Text - Mapper出参key
 * IntWritable - Mapper出参value
 * Text - Reducer出参key
 * IntWritable - Reducer出参value
 */
public class WordCountReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

    private int sum;

    private IntWritable v = new IntWritable();

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        // 1.累加求和
        sum = 0;
        for (IntWritable count : values) {
            sum += count.get();
        }

        // 2.输出
        v.set(sum);
        context.write(key, v);
    }
}
