package com.tca.common.learning.hadoop.mapreduce.phonedata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author zhoua
 * @date 2023/11/4 12:04
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class FlowBean implements Writable {

    private Long upFlow;

    private Long downFlow;

    private Long totalFlow;

    public void setTotalFlow() {
        this.totalFlow = this.upFlow + this.downFlow;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeLong(upFlow);
        out.writeLong(downFlow);
        out.writeLong(totalFlow);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.upFlow = in.readLong();
        this.downFlow = in.readLong();
        this.totalFlow = in.readLong();
    }

    @Override
    public String toString() {
        return this.upFlow + " " + this.downFlow + " " + this.totalFlow;
    }
}
