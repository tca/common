package com.tca.common.learning.hadoop.mapreduce.phonedata;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author zhoua
 * @date 2023/11/4 12:19
 */
public class PhoneDataReducer extends Reducer<Text, FlowBean, Text, FlowBean> {

    private FlowBean outValue = new FlowBean();

    @Override
    protected void reduce(Text key, Iterable<FlowBean> values, Reducer<Text, FlowBean, Text, FlowBean>.Context context) throws IOException, InterruptedException {

        Long totalUp = 0L;
        Long totalDown = 0L;

        for (FlowBean value : values) {
            totalUp += value.getUpFlow();
            totalDown += value.getDownFlow();
        }

        outValue.setUpFlow(totalUp);
        outValue.setDownFlow(totalDown);
        outValue.setTotalFlow();

        context.write(key, outValue);
    }
}
