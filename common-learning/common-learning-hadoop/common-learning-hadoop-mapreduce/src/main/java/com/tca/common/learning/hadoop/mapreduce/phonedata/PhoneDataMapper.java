package com.tca.common.learning.hadoop.mapreduce.phonedata;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author zhoua
 * @date 2023/11/4 12:10
 */
public class PhoneDataMapper extends Mapper<LongWritable, Text, Text, FlowBean> {

    private Text outKey = new Text();

    private FlowBean outValue = new FlowBean();

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, FlowBean>.Context context) throws IOException, InterruptedException {
        // 1.获取一行数据，转成字符串
        String text = value.toString();
        // 2.截取
        String[] words = text.split(" ");
        String phone = words[1];
        String upFlow = words[3];
        String downFlow = words[4];
        // 3.设置outKey, outValue
        outKey.set(phone);
        outValue.setUpFlow(Long.parseLong(upFlow));
        outValue.setDownFlow(Long.parseLong(downFlow));
        outValue.setTotalFlow();
        // 4.write
        context.write(outKey, outValue);
    }
}
