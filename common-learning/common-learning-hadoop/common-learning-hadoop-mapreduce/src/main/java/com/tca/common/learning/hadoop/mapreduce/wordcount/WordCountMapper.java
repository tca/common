package com.tca.common.learning.hadoop.mapreduce.wordcount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author zhoua
 * @date 2023/10/28 23:25
 * 四个泛型
 * LongWritable - 输入key, 入参偏移量(固定)
 * Text - 输入value, 每行内容
 * Text - 输出key
 * IntWritable - 输出value
 */
public class WordCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private Text k = new Text();

    private IntWritable v = new IntWritable(1);


    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        // 1.获取一行
        String line = value.toString();

        // 2.split
        String[] words = line.split(" ");

        // 3.输出
        for (String word : words) {
            k.set(word);
            context.write(k, v);
        }
    }
}
