package com.tca.common.learning.mockito.init;

import com.tca.common.learning.mockito.service.UserService;
import com.tca.common.learning.mockito.service.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/**
 * @author zhoua
 * @date 2024/10/29 23:49
 *
 * 方式二:
 *  在@BeforeEach的方法中使用Mockito的api
 *   Mockito.mock(xxx.class)
 *   Mockito.spy(xxx.class)
 */
@Slf4j
public class MockitoInit2Test {

    private UserService mockUserService;

    private UserService spyUserService;

    @BeforeEach
    public void init() {
        mockUserService = Mockito.mock(UserServiceImpl.class);
        spyUserService = Mockito.spy(UserServiceImpl.class);
    }

    @Test
    void test() {
        // mock对象不是spy对象, 但spy对象是mock对象
        Assertions.assertTrue(Mockito.mockingDetails(mockUserService).isMock());
        Assertions.assertFalse(Mockito.mockingDetails(mockUserService).isSpy());
        Assertions.assertTrue(Mockito.mockingDetails(spyUserService).isMock());
        Assertions.assertTrue(Mockito.mockingDetails(spyUserService).isSpy());
    }

    @Test
    void testMockAndSpy() {
        String mockUsername = mockUserService.getUsernameByNumber(10);
        String spyUsername = spyUserService.getUsernameByNumber(10);
        // mockUsername = null, spyUsername = Messi
        // mock对象没有调用实现类的实际方法, spy对象调用了实现类的实际方法
        log.info("mockUsername = {}, spyUsername = {}", mockUsername, spyUsername);
        Assertions.assertNotEquals(mockUsername, spyUsername);
    }



}
