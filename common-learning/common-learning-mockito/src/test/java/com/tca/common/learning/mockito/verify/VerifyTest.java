package com.tca.common.learning.mockito.verify;

import com.tca.common.learning.mockito.service.UserService;
import com.tca.common.learning.mockito.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/**
 * @author zhoua
 * @date 2024/10/30 23:22
 */
public class VerifyTest {

    private UserService mockUserService;

    @BeforeEach
    public void init() {
        mockUserService = Mockito.mock(UserServiceImpl.class);
    }

    @Test
    void testVerifyTimes() {
        mockUserService.getUsernameByNumber(10);
        // mockUserService.getUsernameByNumber(10)被调用了一次
        Mockito.verify(mockUserService, Mockito.times(1)).getUsernameByNumber(10);
    }
}
