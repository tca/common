package com.tca.common.learning.mockito.injectMock;

import com.tca.common.learning.mockito.service.UserDetailService;
import com.tca.common.learning.mockito.service.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author zhoua
 * @date 2024/10/30 23:27
 * 1.@InjectMocks不能加在接口上, 需要加到类上
 * 2.被@InjectMocks标注的属性必须是实现类, 因为mockito会创建对应的实例对象, 默认创建的对象是
 *      未经过mockito处理的普通对象, 因此常配合@Spy注解使其变为默认调用真实方法的mock对象
 * 3.mockito会使用spy对象或mock对象注入到被@InjectMocks标注的实例对象中
 * 4.被测试的类上一般都需要添加 @InjectMocks 和 @Spy 注解
 */
@ExtendWith(MockitoExtension.class)
public class InjectMockTest {

    @InjectMocks
    @Spy
    private UserServiceImpl mockUserService;

    @Spy
    private UserDetailService userDetailService;

    @Test
    void testInjectMock() {
        Assertions.assertEquals(mockUserService.detail(), "detail");
    }

}
