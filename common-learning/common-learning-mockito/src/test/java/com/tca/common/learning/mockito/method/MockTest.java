package com.tca.common.learning.mockito.method;

import com.tca.common.learning.mockito.service.UserService;
import com.tca.common.learning.mockito.service.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 * @author zhoua
 * @date 2024/10/30 0:20
 * mock 测试
 */
public class MockTest {

    private UserService mockUserService;

    @BeforeEach
    public void init() {
        mockUserService = Mockito.mock(UserServiceImpl.class);
    }

    /**
     * 对于mock对象, 不会调用真实的方法, 直接返回方法的默认值
     * int-0, Object-null, list-空集合
     */
    @Test
    void testMock() {
        String username = mockUserService.getUsernameByNumber(10);
        Assertions.assertNull(username);
    }

    /**
     * 插桩的两种方式：
     * 当调用方法, 且入参为10时, 返回值为Pele
     * 这里展示了两种方式, 两种方式的区别在于?
     * 先doReturn再when, 不会调用when中的方法
     * 先when再thenReturn, 会先调用when中的方法
     */
    @Test
    void testMockReturn() {
        Mockito.doReturn("Pele").when(mockUserService).getUsernameByNumber(10);
        Assertions.assertEquals(mockUserService.getUsernameByNumber(10), "Pele");

        Mockito.when(mockUserService.getUsernameByNumber(9)).thenReturn("Eto");
        Assertions.assertEquals(mockUserService.getUsernameByNumber(9), "Eto");
    }

    /**
     * ArgumentMatchers使用
     * 当调用方法时, 入参为任何int值, 都返回 Lionel Messi
     */
    @Test
    void testArgumentMatchers() {
        Mockito.doReturn("Lionel Messi").when(mockUserService).getUsernameByNumber(ArgumentMatchers.anyInt());
        Assertions.assertEquals(mockUserService.getUsernameByNumber(1000), "Lionel Messi");
    }

    /**
     * 当调用方法时, 返回值为void时
     */
    @Test
    void testMockVoidReturnAndVerifyTimes() {
        Mockito.doNothing().when(mockUserService).addUser(10, "Messi");
        mockUserService.addUser(10, "Messi");
        // 这里验证调用addUser(10, "Messi")了1次
        Mockito.verify(mockUserService, Mockito.times(1)).addUser(10, "Messi");
    }

    /**
     * 测试异常处理
     */
    @Test
    void testException() {
        // 方式一
        Mockito.doThrow(RuntimeException.class).when(mockUserService).getUsernameByNumber(1);
        try {
            mockUserService.getUsernameByNumber(1);
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof RuntimeException);
        }

        // 方式二
        Mockito.when(mockUserService.getUsernameByNumber(2)).thenThrow(RuntimeException.class);
        try {
            mockUserService.getUsernameByNumber(2);
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof RuntimeException);
        }

    }

    /**
     * 多次插桩
     */
    @Test
    void testMultiReturn() {
        // 方式一
        // 第一次调用getUsernameByNumber(100)时, 返回Messi, 第二次返回Xavi, 第三次及之后返回Suarez
        Mockito.doReturn("Messi", "Xavi", "Suarez")
                .when(mockUserService).getUsernameByNumber(100);
        Assertions.assertEquals("Messi", mockUserService.getUsernameByNumber(100));
        Assertions.assertEquals("Xavi", mockUserService.getUsernameByNumber(100));
        Assertions.assertEquals("Suarez", mockUserService.getUsernameByNumber(100));

        // 方式二
        Mockito.doReturn("Messi").doReturn("Xavi").doReturn("Suarez")
                .when(mockUserService).getUsernameByNumber(200);
        Assertions.assertEquals("Messi", mockUserService.getUsernameByNumber(200));
        Assertions.assertEquals("Xavi", mockUserService.getUsernameByNumber(200));
        Assertions.assertEquals("Suarez", mockUserService.getUsernameByNumber(200));
    }

    /**
     * 测试 thenAnswer 方法, 指定插桩逻辑
     */
    @Test
    void testThenAnswer() {
        Mockito.when(mockUserService.getUsernameByNumber(ArgumentMatchers.anyInt())).thenAnswer(
                new Answer<String>() {
                    @Override
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                        Integer number = invocationOnMock.getArgument(0, Integer.class);
                        if (number == 10) {
                            return "Pele";
                        }
                        if (number == 8) {
                            return "Inesta";
                        }
                        return null;
                    }
                }
        );
        Assertions.assertEquals(mockUserService.getUsernameByNumber(8), "Inesta");
    }

    @Test
    void testRealMethod() {
        Mockito.when(mockUserService.getUsernameByNumber(ArgumentMatchers.anyInt())).thenCallRealMethod();
        Assertions.assertEquals(mockUserService.getUsernameByNumber(10), "Messi");
    }
}

