package com.tca.common.learning.mockito.init;

import com.tca.common.learning.mockito.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

/**
 * @author zhoua
 * @date 2024/10/29 23:49
 *
 * 方式三:
 *  注入的代理对象上添加 @Mock 或者 @Spy
 *  在@BeforeEach的方法中使用Mockito的api
 */
public class MockitoInit3Test {

    @Mock
    private UserService mockUserService;

    @Spy
    private UserService spyUserService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void test() {
        // mock对象不是spy对象, 但spy对象是mock对象
        Assertions.assertTrue(Mockito.mockingDetails(mockUserService).isMock());
        Assertions.assertFalse(Mockito.mockingDetails(mockUserService).isSpy());
        Assertions.assertTrue(Mockito.mockingDetails(spyUserService).isMock());
        Assertions.assertTrue(Mockito.mockingDetails(spyUserService).isSpy());
    }



}
