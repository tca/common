package com.tca.common.learning.mockito.init;

import com.tca.common.learning.mockito.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author zhoua
 * @date 2024/10/29 23:49
 *
 * 方式一:
 *  类上添加 @ExtendWith(MockitoExtension.class)
 *  注入的代理对象上添加 @Mock 或者 @Spy
 *  @Mock 和 @Spy的区别？ TODO
 */
@ExtendWith(MockitoExtension.class)
public class MockitoInit1Test {

    @Mock
    private UserService mockUserService;

    @Spy
    private UserService spyUserService;

    @Test
    void test() {
        // mock对象不是spy对象, 但spy对象是mock对象
        Assertions.assertTrue(Mockito.mockingDetails(mockUserService).isMock());
        Assertions.assertFalse(Mockito.mockingDetails(mockUserService).isSpy());
        Assertions.assertTrue(Mockito.mockingDetails(spyUserService).isMock());
        Assertions.assertTrue(Mockito.mockingDetails(spyUserService).isSpy());
    }



}
