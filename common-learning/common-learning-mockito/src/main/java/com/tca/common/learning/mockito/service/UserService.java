package com.tca.common.learning.mockito.service;

/**
 * @author zhoua
 * @date 2024/10/29 23:51
 */
public interface UserService {

    /**
     * 根据number获取球员名称
     * @param number
     * @return
     */
    public String getUsernameByNumber(Integer number);

    /**
     * 添加球员
     * @param number
     * @param username
     */
    public void addUser(Integer number, String username);

    /**
     * 获取详情
     * @return
     */
    public String detail();
}
