package com.tca.common.learning.mockito.service;

import org.springframework.stereotype.Service;

/**
 * @author zhoua
 * @date 2024/10/30 23:28
 */
@Service
public class UserDetailService {

    public String detail() {
        return "detail";
    }
}
