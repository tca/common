package com.tca.common.learning.mockito.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author zhoua
 * @date 2024/10/29 23:51
 */
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDetailService userDetailService;

    @Override
    public String getUsernameByNumber(Integer number) {
        if (number == 10) {
            return "Messi";
        }
        if (number == 9) {
            return "Suarez";
        }
        if (number == 11) {
            return "Neymar";
        }
        return null;
    }

    @Override
    public void addUser(Integer number, String username) {
        if (number < 0) {
            throw new RuntimeException("number must greater than zero!");
        }
        if (number == 9 || number == 10 || number == 11) {
            throw new RuntimeException("player number already exists");
        }
        log.info("add player success!");
    }

    @Override
    public String detail() {
        return userDetailService.detail();
    }
}
