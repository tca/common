package com.tca.common.learning.skywalking.controller;

import com.tca.common.core.bean.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.Tag;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author zhoua
 * @Date 2021/12/10
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @GetMapping("/hello/{name}")
    public Result<String> hello(@PathVariable(value = "name") String name) {
        log.info("name = {}", name);
        sleep(3L);
        return Result.success("hello, " + name);
    }

    @Trace
    @Tag(key = "sleepTime", value = "arg[0]")
    private void sleep(long time) {
        try {
            TimeUnit.SECONDS.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
