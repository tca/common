package com.tca.common.learning.skywalking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhoua
 * @date 2024/8/22 8:15
 */
@SpringBootApplication
public class SkywalkingLearningApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkywalkingLearningApplication.class);
    }

}
