package com.tca.common.learning.guava.bloomfilter;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.nio.charset.Charset;

/**
 * @author zhoua
 * @Date 2021/9/25
 */
@Slf4j
public class BloomFilterTest {

    @Test
    public void test() {
        // 1.创建BloomFilter
        // 参数 Funnel --> 存储的key
        // 参数 10 --> 期望插入10个数据
        // 参数 100 --> 错误率控制0.01
        BloomFilter<String> bloomFilter = BloomFilter.create(
                Funnels.stringFunnel(Charset.defaultCharset()),
                10,
                0.01d);
        // 2.插入10个测试数据
        for (int i = 0; i < 10; i++) {
            bloomFilter.put(String.valueOf(i));
        }
        // 3.测试 11 - 110
        int errCount = 0;
        for (int i = 11; i < 110; i++) {
            if (bloomFilter.mightContain(String.valueOf(i))) {
                errCount++;
            }
        }
        log.info("错误率为{}%", errCount);
        int rightCount = 0;
        for (int i = 0; i < 10; i++) {
            if (bloomFilter.mightContain(String.valueOf(i))) {
                rightCount++;
            }
        }
        log.info("正确率为{}%", rightCount * 10);
    }
}
