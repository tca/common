package com.tca.common.learning.guava.bloomfilter;

import com.google.common.util.concurrent.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author zhoua
 * @Date 2021/9/25
 */
@Slf4j
public class RateLimitTest {

    @Test
    public void test() throws InterruptedException {
        /**
         * guava 的 RateLimiter 采用令牌桶算法
         *
         * create的参数表示 每秒发放令牌数
         */
        RateLimiter rateLimiter = RateLimiter.create(5);
        TimeUnit.SECONDS.sleep(1);

        int count = 10;
        ExecutorService executorService = Executors.newFixedThreadPool(count);
        for (int i = 0; i < count; i++) {
            executorService.submit(() -> {
                if (rateLimiter.tryAcquire(200, TimeUnit.MILLISECONDS)) {
                    log.info("yeah ~~ 获取成功");
                } else {
                    log.info("获取失败");
                }
            });
        }

//        TimeUnit.SECONDS.sleep(1L);
        executorService.shutdown();
    }
}
