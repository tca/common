## actuator使用参考
[actuator使用参考](https://zhuanlan.zhihu.com/p/543521192?utm_id=0)

[actuator-1.x和2.x配置](https://blog.csdn.net/wiseyl/article/details/106444788?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_baidulandingword~default-5-106444788-blog-81321269.235^v38^pc_relevant_default_base&spm=1001.2101.3001.4242.4&utm_relevant_index=8)

[actuator视频讲解](https://www.bilibili.com/video/BV11L4y1a77w/?spm_id_from=333.337.search-card.all.click&vd_source=8672f11d288f773b003b1ae375ba21cd)

### actuator集成prometheus
默认情况下,如果只引用spring-boot-starter-actuator时,是没有prometheus关注的metrics信息的，即没有暴露/actuator/prometheus接口，
此时,需要引入micrometer-registry-prometheus,prometheus才能采集到相关的metrics信息！

### 使用actuator动态修改日志级别
* 通过actuator查看所有日志级别
GET: http://ip:port/actuator/loggers
* 通过actuator查看具体路径的日志级别
GET: http://ip:port/actuator/loggers/{path}
* 通过actuator修改日志级别
POST: http://ip:port/actuator/loggers/{path} 
-H: Content-Type:application/json
--data: {{path}:{level}}

### log集成MDC
#### 什么是MDC
```
MDC链路追踪是一种应用在分布式系统中的追踪技术，通过在应用中加入MDC（Mapped Diagnostic Context，映射调试上下文）的上下文信息，以实现链路追踪和故障排查的目的12。
MDC可以看成是一个与当前线程绑定的Map，可以往其中添加键值对，用于记录和存储有关当前线程的一些诊断信息，如请求ID、调用链路、方法调用信息等。MDC中包含的内容可以被同一线程中执行的代码所访问，可以帮助开发人员快速定位和解决系统中的问题
```
#### logback集成mdc
* 自定义Interceptor, 并注入到spring容器中
```java
 @Slf4j
public class MdcInterceptor implements HandlerInterceptor {

    private static final String REQUEST_ID = "mdc";

    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 为当前请求生成唯一uuid
        String uuid = UUID.randomUUID().toString();
        // 将当前请求的uuid存储到mdc中
        MDC.put(REQUEST_ID, uuid);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // 移除mdc
        MDC.remove(REQUEST_ID);
    }
}
```
* 修改logback.xml配置文件
在[pattern]中添加mdc
```xml
<!-- 这里在pattern中添加了%X{mdc}, %X必填, {mdc}中的取值为interceptor中定义的key！！！ -->
<pattern>%date{yyyy-MM-dd HH:mm:ss.SSS}|%-5level|%thread|%logger:%line|%X{mdc}| %msg%n</pattern> 
```
#### log4j2集成mdc
与logback集成类似！