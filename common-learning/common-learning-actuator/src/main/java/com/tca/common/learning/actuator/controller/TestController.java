package com.tca.common.learning.actuator.controller;

import com.tca.common.core.bean.Result;
import com.tca.common.web.utils.IpAddressUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.concurrent.TimeUnit;

/**
 * @author an.zhou
 * @date 2023/5/12 16:22
 */
@RestController
@RequestMapping("/test/")
@Slf4j
public class TestController {

    // 一个用于演示的http接口
    @GetMapping(path = "/sleep/{sleepTime}")
    public Result sleep(@PathVariable Integer sleepTime, HttpServletRequest request) {
        try {
            log.trace("trace log");
            log.debug("debug log");
            log.info("info log");
            log.warn("warn log");
            log.error("error log");

            Enumeration<String> headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();
                log.info("header --> {}, {}", headerName, request.getHeader(headerName));
            }

            String clientIp = IpAddressUtil.getIpAddress(request);
            log.info("client ip = {}", clientIp);

            TimeUnit.MILLISECONDS.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Result.success();
    }

}
