package com.tca.common.learning.actuator.config;

import com.tca.common.learning.actuator.interceptor.MdcInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author an.zhou
 * @date 2023/9/9 15:03
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(mdcInterceptor()).addPathPatterns("/**");
    }

    @Bean
    public MdcInterceptor mdcInterceptor() {
        return new MdcInterceptor();
    }
}
