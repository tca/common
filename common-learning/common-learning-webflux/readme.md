# Spring WebFlux

## 1.介绍

```
1.spring webflux是spring5添加的新的模块, 用于web开发的, 功能和springmvc类似的, webflux属于响应式编程框架
2.使用传统 web 框架, 比如springmvc, 这些基于servlet容器, webflux是一种异步非阻塞的框架, 异步非阻塞的框架在servlet3.1以后才支持
3.spring webflux 的核心是基于 Reactor 的相关api实现的
```

[参考](https://blog.csdn.net/get_set/category_9272724.html)

## 2.特点

### 1.1 变化传递

```
一种生产者只负责生成并发出数据/事件, 消费者来监听并负责定义如何处理数据/事件的变化传递方式。
响应式编程的核心特点之一: 变化传递 (propagation of change)
```

### 1.2 数据流

```
响应式编程的另一个核心特点: 基于数据流
```

### 1.3 声明式

```
响应式编程的第三个核心特点: 声明式。命令式是面向过程的, 声明式是面向结构的。
```

### 总结

```
总结起来, 响应式编程 (reactive programming) 是一种基于数据流 (data stream) 和变化传递 (propagation of change) 的声明式 (declarative) 的编程范式
```

## 3.响应式流

### 3.1 响应式流和java  stream区别

```
1.web应用具有i/o密集的特点, i/o阻塞会带来比较大的性能损失或浪费资源, java stream是一种同步 api, 响应式流需要具有异步非阻塞的特点
2.java stream不具备完善的对接数据流的流量控制的能力
```

### 3.2 概念

```
具备 "异步非阻塞" 特性和 "流量控制" 能力的数据流, 我们称之为响应式流 (Reactive Stream)
```

### 3.3 常用类库

```
RxJava和Reactor
Reactor专注于Server端的响应式开发, RxJava更多倾向于Android端的响应式开发。
在Java 9版本中, 响应式流的规范被纳入到了JDK中, 相应的API接口是java.util.concurrent.Flow
```

### 3.4 特点

#### 3.4.1 异步非阻塞

##### 举例

```
从调用者和服务提供者的角度来看, 阻塞、非阻塞以及同步、异步可以这么理解：

	阻塞和非阻塞反映的是调用者的状态, 当调用者调用了服务提供者的方法后, 如果一直在等待结果返回, 否则无法执行后续的操作, 那就是阻塞状态; 如果调用之后直接返回, 从而可以继续执行后续的操作, 那可以理解为非阻塞的。
	同步和异步反映的是服务提供者的能力, 当调用者调用了服务提供者的方法后, 如果服务提供者能够立马返回, 并在处理完成后通过某种方式通知到调用者, 那可以理解为异步的; 否则, 如果只是在处理完成后才返回, 或者需要调用者再去主动查询处理是否完成, 就可以理解为是同步的。
	举个例子, 小牛买了个洗衣机, 当她启动了洗衣机后如果一直在等待洗衣机工作结束好晾衣服, 那她就是阻塞的; 如果她启动洗衣机之后就去看电视了, 估摸快洗完了就时不时来看看, 那她就是非阻塞的, 因为小牛可以去做另一件事。但小牛不能知道这洗衣机啥时候洗完/是否洗完, 那么这台洗衣机就是同步方式工作的; 小牛后来换了一台可以在洗完衣服播放音乐的洗衣机, 这样就不用时不时来看了, 虽然启动之后洗衣机不能立刻返回给小牛干净的衣服, 但是可以在工作完成之后通知在看电视的小牛, 所以新的洗衣机就是异步工作的。
```

#### 3.4.2 流量控制 - 回压

##### 回压策略

- ERROR

```
当下游跟不上节奏的时候发出一个错误信号
```

- DROP

```
当下游没有准备好接收新的元素的时候抛弃这个元素
```

- LATEST

```
让下游只得到上游最新的元素
```

- BUFFER

```
缓存下游没有来得及处理的元素(如果缓存不限大小的可能导致OutOfMemoryError)
```



### 3.5 响应式流编程规范

#### 响应式流接口

##### Publisher

```java
public interface Publisher<T> {
    public void subscribe(Subscriber<? super T> s);
}
```

##### Subscriber

```java
/**
 * 当执行上述发布者Publisher的subscribe方法时, 发布者会回调订阅者的onSubscribe方法, 这个方法中, 通常订阅者会借助传入的Subscription向发布者请求n个   数据。然后发布者通过不断调用订阅者的onNext方法向订阅者发出最多n个数据。如果数据全部发完, 则会调用onComplete告知订阅者流已经发完; 如果有错误发生则     通过onError发出错误数据, 同样也会终止流
  订阅后的回调用表达式表示就是onSubscribe onNext* (onError | onComplete)?, 即以一个onSubscribe开始, 中间有0个或多个onNext, 最后有0个或1个         onError或onComplete事件
  Publisher和Subscriber融合了迭代器模式和观察者模式
 */
public interface Subscriber<T> {
    public void onSubscribe(Subscription s);
    public void onNext(T t);
    public void onError(Throwable t);
    public void onComplete();
}
```

##### Subscription

```java
/**
 * Subscription是Publisher和Subscriber的"中间人".当发布者调用subscribe方法注册订阅者时, 会通过订阅者的回调方法onSubscribe传入Subscription对象,     之后订阅者就可以使用这个Subscription对象的request方法向发布者"要"数据了。回压机制正是基于此来实现的
 */
public interface Subscription {
    public void request(long n);
    public void cancel();
}
```

##### Processor

```java
/**
 * Processor集Publisher和Subscriber于一身
 */
public interface Processor<T, R> extends Subscriber<T>, Publisher<R> {
}
```

##### 原理图



## 4.响应式编程实现

```
1.响应式编程操作中, Reactor 是满足 Reactive 规范的框架
2.Reactor 有两个核心类 Mono 和 Flux, 这两个类实现接口 Publisher, 提供丰富操作符。Flux 对象实现发布者, 返回N个元素; Mono 也实现发布者, 返回0或1个元素
3.Flux和Mono都是数据流的发布者, 使用 Flux 和 Mono 都可以发出三种数据信号:元素值, 错误信号, 完成信号。其中, 错误信号和完成信号都代表终止信号, 终止信号表示订阅者数据流结束了。错误信号终止数据流的同时把错误信息传递给订阅者。 
4.三种信号特点
	错误信号和完成信号都是终止信号, 不能共存
	如果没有发送任何元素值, 而是直接发送错误或者完成信号, 表示是空数据流
	如果没有错误信号, 没有完成信号, 表示是无限数据流    
```

具体代码见 common-learning-webflux-reactor



## 5.spring-webflux

### 5.1 服务端

```
1.spring-webflux 是基于响应式流的, 因此可以用来建立异步的、非阻塞的、事件驱动的服务。它们采用Reactor作为首选的响应式流的实现库, 同时也提供了RxJava的支持
2.由于响应式编程的特性, spring webflux 和 Reactor 底层需要支持异步的运行环境, 比如Netty和Undertow; 也支持运行在支持异步i/o的servlet3.1的容器上, 比如tomcat(8.0.23及以上)和Jetty(9.0.4及以上)
3.spring-webflux的上层支持两种开发模式:
	第一种: 类似于springmvc的基于注解(@Controller, @RequestMapping)的开发模式;
	第二种: java8 lambda风格的函数式开发模式
4.spring-webflux也支持响应式的websocket服务端开发
```

### 5.2 客户端

spring-webflux也提供了类似于restTemplate一样的http客户端，但是与restTemplate相比，其具有以下特点

```
1.是非阻塞的, 可以基于少量的线程处理更高的并发
2.可以使用java8 lambda表达式
3.支持异步的同时, 也可以支持同步的方式
4.可以通过数据流的方式和服务端进行双向通讯
```

### 5.3 基于springmvc的注解式的方式开发

略，见 common-learning-webflux-springboot 代码

### 5.4 基于函数式的方式开发

#### HandlerFunction

HandlerFunction相当于springmvc中Controller中的具体处理方法, 输入为请求, 输出为装在Mono中的响应

```java
Mono<T extends ServerResponse> handle(ServerRequest request);
```

#### RouterFunction

路由，相当于`@RequestMapping`，用来判断什么样的url映射到那个具体的`HandlerFunction`，输入为请求，输出为装在Mono里边的`Handlerfunction`

```java
Mono<HandlerFunction<T>> route(ServerRequest request);
```

注意：这里的请求和响应不再是 HttpServletRequest 和 HttpServletResponse，而是 ServerRequest 和 ServerResponse

