package com.tca.common.learning.webflux.mvc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author zhoua
 * @date 2022/1/7 15:04
 */
@RestController
@RequestMapping("/mvc")
public class MvcController {

    @GetMapping("/{latency}")
    public String hello(@PathVariable int latency) {
        try {
            TimeUnit.MILLISECONDS.sleep(latency);
        } catch (InterruptedException e) {
        }
        return "Welcome to reactive world ~";
    }
}
