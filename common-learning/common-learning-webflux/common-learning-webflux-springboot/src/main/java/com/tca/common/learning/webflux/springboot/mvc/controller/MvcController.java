package com.tca.common.learning.webflux.springboot.mvc.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.time.Duration;

/**
 * @author zhoua
 */
@RestController
@Slf4j
@RequestMapping("/mvc")
public class MvcController {

    @GetMapping("/webflux")
    public Mono<String> hello() {
        return Mono.just("hello mvc flux");
    }

    @GetMapping("/{latency}")
    public Mono<String> hello(@PathVariable int latency) {
        return Mono.just("Welcome to reactive world ~")
                .delayElement(Duration.ofMillis(latency));
    }
}
