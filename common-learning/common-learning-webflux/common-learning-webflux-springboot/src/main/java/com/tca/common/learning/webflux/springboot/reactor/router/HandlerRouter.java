package com.tca.common.learning.webflux.springboot.reactor.router;

import com.tca.common.learning.webflux.springboot.reactor.handler.ReactorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * @author zhoua
 */
@Component
public class HandlerRouter {

    @Autowired
    private ReactorHandler reactorHandler;

    @Bean
    public RouterFunction<ServerResponse> router() {
        return RouterFunctions.route(RequestPredicates.GET("/reactor/time"), req -> reactorHandler.getTime(req))
                .andRoute(RequestPredicates.GET("/reactor/date"), req -> reactorHandler.getDate(req))
                .andRoute(RequestPredicates.GET("/reactor/sendTime"), req -> reactorHandler.sendTimePerSec(req));
    }
}
