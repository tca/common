## 1.核心概念
### 度量指标和标签
```
1.每个时间序列由度量指标和一组标签键值对唯一确定
2.度量指标：用于描述对监控系统的某个测量特征（比如http_request_total表示http请求总数）。
3.标签：开启了Prometheus的多维数据模型。对于同一个度量指标, 不同标签键值组合会形成特定的维度。
```
### 度量指标类型
#### 计数器 - counter
```
计数器是一种累计型的度量指标, 它是一个只能递增的数值。计数器主要用于统计类似于服务请求数、任务完成数和错误出现次数这样的数据
```
#### 计量器 - gauge
```
计量器表示一个既可以增加, 也可以减少的一种度量指标值。计量器主要用于测量类似于温度、内存使用量这样的瞬时数据。
```
#### 直方图
#### 汇总
### 采样值
```
时序数据本质上就是一系列采样值。每个采样值包括：
1.一个64位的浮点数值
2.一个精确到毫秒的时间戳
```
### 注解
```
一个注解由一个度量指标和一组标签键值对构成, 如下：
[metric_name]{[label_key]=[label_value], ...}
```
### 任务和实例
```
实例：可以从中抓取采样值的端点称为实例
任务：为了性能扩展而复制出来的多个实例形成一个任务


Prometheus抓取完采样值后, 会自动给采样值添加 job 和 instance 的值
```

## 2.altermanager核心概念
### 分组
```
分组将类似性质的警报分类为单个通知
```
### 抑制
```
如果某些其他警报已经出发, 则抑制是抑制某些警报的通知的概念
```
### 沉默
```
沉默是在给定时间内简单的静音警报的简单方法
```

## 3.监控的层面
### 业务监控
```
可以包含 用户访问QPS、DAU日活、访问状态（http）、业务接口（登录、注册、聊天、上传、留言、短信、搜索）、产品转化率等
```
### 系统监控
```
主要是根操作系统相关的基本监控项, 可以包括 CPU、内存、磁盘、IO、TCP连接、流量 等
```
### 网络监控
```
对网络状态的监控, 可以包括：丢包率、延迟 等
```
### 日志监控
```
监控中的重点, 一般会单独设计或搭建, 全部种类的日志都有需要采集
```
### 程序监控
```
一般需要和开发人员配合, 程序中嵌入各种接口, 直接获取数据, 或者特质的日志格式
```

## 4.下载安装
### 下载
[官方下载地址](https://prometheus.io/download/)
### 安装
直接解压缩即可
### 验证
#### 本地验证
[本地验证](http://localhost:9090)
#### node_exporter验证
##### 作用
可以装在被监控服务器上, 在prometheus上添加对其监控, prometheus会通过http请求定时采集数据
##### 下载
[官方下载地址](https://prometheus.io/download/)
##### 安装
直接解压即可
##### 启动
##### 验证
[验证node_exporter](http://localhost:9100/metrics)
```
注意：window中node_exporter的默认端口是9182！所以可以通过访问http://localhost:9182/metrics 来验证
```

#### 举例

启动prometheus和windows_exporter，打开prometheus界面 localhost:9090，输入表达式：windows_cpu_clock_interrupts_total

查看数据如下：

```
windows_cpu_clock_interrupts_total{core="0,0", instance="localhost:9182", job="windows_node_exporter"}
```

metric为：windows_cpu_clock_interrupts_total

label包括：core、instance、job



## 5.基本配置

### prometheus.yml 配置文件解析
* 全局属性配置
```yaml
# my global config
global:
  # 数据抓取(采样)间隔
  scrape_interval: 15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  # 数据评估间隔
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  # scrape_timeout is set to the global default (10s).
```
* 告警配置
```yaml
# Alertmanager configuration
alerting:
  alertmanagers:
    - static_configs:
        - targets:
          # - alertmanager:9093
```
* 规则文件
```yaml
# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  # - "first_rules.yml"
  # - "second_rules.yml"
```
* 抓取配置（核心配置）
```yaml
# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  # 任务名称
  - job_name: "prometheus"
    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.
    # 任务配置
    static_configs:
      # 监控主机列表  
      - targets: ["localhost:9090","localhost:9100"]
  - job_name: "common-learning-actuator"
    # 采集间隔
    scrape_interval: 5s
    # 采集路径
    metrics_path: '/actuator/prometheus'
    static_configs:
      - targets: ['127.0.0.1:8080']
```
[prometheus.yml配置文件解析参考](https://www.cnblogs.com/liwenchao1995/p/16895710.html)

## 6.基本使用

### promeQL

#### sum 求和

#### increase 时间段总增长量

```
increase(node_cpu_seconds_total{instance="10.0.0.41:9100",mode="idle"}[1m])
```

## 7.grafana
[参考](https://yanglinwei.blog.csdn.net/article/details/124313953?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-124313953-blog-125721975.235%5Ev35%5Epc_relevant_increate_t0_download_v2&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-124313953-blog-125721975.235%5Ev35%5Epc_relevant_increate_t0_download_v2&utm_relevant_index=2)

### 7.1 下载

### 7.2 安装

### 7.3 验证

localhost:3000（用户名/密码，默认为：admin/admin）

### 7.4 使用

```
可以在grafana官网(https://grafana.com/grafana/dashboards)下载demo对应的json文件并导入使用！
```



## 8.集成springboot

[参考](https://blog.csdn.net/wdj_yyds/article/details/123843862)

### 8.1 说明

```
1.需要借助 actuator 来提供扩展端点
2.java应用中引入相关依赖，包括：micrometer-registry-prometheus 以及 spring-boot-starter-actuator 后，会有Prometheus数据格式相关数据，在Prometheus中配置引入即可（参考配置文件）
```

### 8.2 核心依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
 
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
 
    <dependency>
        <groupId>io.micrometer</groupId>
        <artifactId>micrometer-registry-prometheus</artifactId>
    </dependency>
</dependencies>
```

### 8.3 yaml配置 prometheus

```yml
spring:
  application:
    name: common-learning-prometheus
management:
  endpoints:
    web:
      exposure:
        include: "*"
  metrics:
    tags:
      application: ${spring.application.name}
```

## 9.pushgateway

### 9.1 说明

```
Pushgateway为Prometheus整体监控方案的功能组件之一，并作为一个独立的工具存在。它主要用于Prometheus无法直接拿到监控指标的场景，如监控源位于防火墙之后，Prometheus无法穿透防火墙；目标服务没有可抓取监控数据的端点等多种情况。
在类似场景中，可通过部署Pushgateway的方式解决问题。当部署该组件后，监控源通过主动发送监控数据到Pushgateway，再由Prometheus定时获取信息，实现资源的状态监控。
```

### 9.2 下载安装

### 9.3 启动

### 9.4 验证

http://localhost:9091/

### 9.5 配置prometheus抓取pushgateway数据

```yaml
scrape_configs:
  - job_name: "prometheus-pushgateway"
    #如果pushgateway组件的标签，和prometheus服务的标签冲突了，如何解决？
    #1，false（默认），将标签加上前缀“exporter_”，不覆盖；
    #2，true，覆盖原来的标签；
    honor_labels: false
    static_configs:
    - targets: ["localhost:9091"]
```

### 9.6 模拟客户端发送数据至pushgateway

```shell
 echo "custom_metrics 102" | curl --data-binary @-http://localhost:9091/metrics/job/custom_job/localhost/127.0.0.1

## 说明:
echo "key  value" | curl --data-binary @-  http://pushgatway的ip:端口号/metrics/job/自定义job名称/instance/被监控节点的ip地址
```



## 10.alertmanager

### 9.1 说明

```
Alertmanager 主要用于接收 Prometheus 发送的告警信息，它支持丰富的告警通知渠道，而且很容易做到告警信息进行去重，降噪，分组，策略路由，是一款前卫的告警通知系统
```

### 9.2 配置邮件告警

#### step1 配置alertmanager.yml配置文件

```yaml
#一、发件人信息配置
global:
  #解析失败超时时间；
  resolve_timeout: 5m
  #【发件人】邮箱
  smtp_from: '870235784@qq.com'
  #【邮箱官方主机】地址及端口
  smtp_smarthost: 'smtp.qq.com:465'
  #【发件人】邮箱
  smtp_auth_username: '870235784@qq.com'
  #【发件人】邮箱授权码
  smtp_auth_password: 'lfkluytpslcbbdac'
  #发送信息是否tls加密
  smtp_require_tls: false
  smtp_hello: 'qq.com'
#二、报警的间隔信息配置；
route:
  group_by: ['alertname']
  group_wait: 5s
  group_interval: 5s
  #重复报警的间隔时间，如果报警问题没有解决，则会间隔指定的时间继续触发报警，比如5分钟；
  repeat_interval: 5m
  #采用什么报警方式？本次学习，我们使用邮箱；
  receiver: 'email'
#三、接收告警的目标信息编辑；谁来接收告警？
receivers:
#定义接收者名称
- name: 'email'
  email_configs:
  #【收件人】
  - to: '870235784@qq.com'
    send_resolved: true
  - to: '1454320955@qq.com'
    send_resolved: true
inhibit_rules:
  - source_match:
      #匹配的告警级别
      severity: 'critical'
    target_match:
      severity: 'warning'
    equal: ['alertname', 'dev', 'instance']
```

#### step2 启动alertmanager并验证

localhost:9093

#### step3 配置prometheus.yml配置文件

```yaml
rule_files:
  # - "first_rules.yml"
  # - "second_rules.yml"
  # 告警规则配置文件
  - E:\\java_install\\prometheus\\prometheus-2.53.0.windows-amd64\\custom_rule.yaml
```

#### step4 配置custom.yaml规则配置文件

```yaml
groups:
- name: custom-alert
  rules:
  - alert: common-learning-prometheus服务停止          
    #当promeQL这个语句=0时（节点挂掉），开始报警
    expr: up{instance="127.0.0.1:8080"} == 0
    #连续3s=0才触发报警；
    for: 3s
    labels:
      #被监控节点ip
      node: 127.0.0.1
      instance: common-learning-prometheus
    annotations:
      summary: "{{ $labels.instance }} 已停止运行超过 3s！"
```
















