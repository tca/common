package com.tca.common.learning.prometheus.controller;

import com.tca.common.core.bean.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author an.zhou
 * @date 2023/5/12 16:22
 */
@RestController
@RequestMapping("/test")
public class TestController {

    /**
     * 一个用于演示的http接口
     */
    @GetMapping(path = "/sleep/{sleepTime}")
    public Result sleep(@PathVariable Integer sleepTime) {
        try {
            TimeUnit.MILLISECONDS.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Result.success();
    }

}
