package com.tca.common.learning.jmh.test;

import org.junit.Test;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

/**
 * @author an.zhou
 * @date 2023/5/25 16:47
 */
@BenchmarkMode(Mode.All)
@Fork(1)
@Threads(2)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@State(Scope.Thread)
public class TestBenchmark {

    @Test
    @Benchmark
    public void test() {
        try {
            System.out.println("test benchmark");
            TimeUnit.MILLISECONDS.sleep(500L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
