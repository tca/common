## 概述
ApplicationEvent以及Listener是Spring为我们提供的一个事件监听、订阅的实现，内部实现原理是观察者设计模式，设计初衷也是为了系统业务逻辑之间
的解耦，提高可扩展性以及可维护性。事件发布者并不需要考虑谁去监听，监听具体的实现内容是什么，发布者的工作只是为了发布事件而已

## 核心接口
* ApplicationEvent
* ApplicationListener

## 内置ApplicationEvent
* ContextRefreshedEvent - 容器刷新事件
* ContextStartedEvent - 容器启动事件
* ContextStopEvent - 容器停止事件
* ContextClosedEvent - 容器关闭事件

## 内置ApplicationEvent的使用
* 方式一 implements ApplicationListener<具体事件类型，如ContextRefreshedEvent>
* 方式二 方法上使用@EventListener，方法参数使用具体的事件，如ContextRefreshedEvent
* 方式三 方法上使用@EventListener({事件类型.class})
**注意**
```
以上自定义的listener都需要注册到spring容器中
```

## 拓展event的使用
step1 自定义Event extends ApplicationEvent
step2 按照<内置ApplicationEvent的使用>的方式自定义Listener
step3 使用ApplicationContext的publishEvent方法来发布自定义事件

## 原理解析
[参考](https://blog.csdn.net/u022812849/article/details/124530419)
### 发布消息入口
AbstractApplicationContext#publish方法，在该方法中使用ApplicationEventMulticaster
### ApplicationEventMulticaster
spring中内置了实现类 - SimpleApplicationEventMulticaster，在AbstractApplicationContext#refresh方法中的
    initApplicationEventMulticaster方法中创建并注册到spring容器中
### ApplicationListener
在AbstractApplicationContext#refresh方法中的registerListeners方法中完成ApplicationListener的创建并注册到
    spring容器中

## SpringBoot集成
[参考](https://blog.csdn.net/Zong_0915/article/details/126525246)
**说明**
```
springboot中不建议使用上述方式，springboot新增了很多事件
```

## SpringBoot的内置event和顺序
* ApplicationStartingEvent - 在运行开始时发送 ，但在进行任何处理之前（侦听器和初始化程序的注册除外）发送
* ApplicationEnvironmentPreparedEvent - 当被发送Environment到中已知的上下文中使用，但是在创建上下文之前
* ApplicationContextInitializedEvent - 在ApplicationContext准备好且已调用ApplicationContextInitializers之后但任何bean定义未加载之前发送
* ApplicationPreparedEvent - 在刷新开始之前但在加载bean定义之后发送
* ContextRefreshedEvent - 容器刷新事件
* ApplicationStartedEvent - 上下文已被刷新后发送，但是任何应用程序和命令行都被调用前
* ApplicationReadyEvent - 在所有的命令行应用启动后发送此事件，可以处理请求
* ApplicationFailedEvent - 在启动时异常发送


