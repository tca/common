package com.tca.common.learning.spring.event.external.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author an.zhou
 * @date 2023/5/22 16:58
 */
public class CustomEvent extends ApplicationEvent {

    public CustomEvent(Object source) {
        super(source);
    }
}
