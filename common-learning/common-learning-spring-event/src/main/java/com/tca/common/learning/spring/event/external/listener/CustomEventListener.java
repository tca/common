package com.tca.common.learning.spring.event.external.listener;

import com.tca.common.learning.spring.event.external.event.CustomEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;

/**
 * @author an.zhou
 * @date 2023/5/22 16:59
 */
@Slf4j
public class CustomEventListener {

    @EventListener({CustomEvent.class})
    public void customEvent(ApplicationEvent event) {
        log.info("custom event listener, event = {}", event);
    }
}
