package com.tca.common.learning.spring.event.inner.listener.v1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextStoppedEvent;

/**
 * @author an.zhou
 * @date 2023/5/22 16:24
 */
@Slf4j
public class CustomContextClosedListener implements ApplicationListener<ContextClosedEvent> {
    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        log.info("closed event = {}", event);
    }
}
