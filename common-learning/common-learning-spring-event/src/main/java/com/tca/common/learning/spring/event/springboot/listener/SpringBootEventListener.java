package com.tca.common.learning.spring.event.springboot.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.*;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author an.zhou
 * @date 2023/5/22 17:31
 */
//@Component
@Slf4j
public class SpringBootEventListener {

    @EventListener(ApplicationStartingEvent.class)
    public void applicationStartingEventListener(ApplicationEvent event) {
        log.info("ApplicationStartingEvent");
    }

    @EventListener(ApplicationEnvironmentPreparedEvent.class)
    public void applicationEnvironmentPreparedEvent(ApplicationEvent event) {
        log.info("ApplicationEnvironmentPreparedEvent");
    }

    @EventListener(ApplicationContextInitializedEvent.class)
    public void applicationContextInitializedEvent(ApplicationEvent event) {
        log.info("ApplicationContextInitializedEvent");
    }

    @EventListener(ApplicationPreparedEvent.class)
    public void applicationPreparedEvent(ApplicationEvent event) {
        log.info("ApplicationPreparedEvent");
    }

    @EventListener(ApplicationStartedEvent.class)
    public void applicationStartedEvent(ApplicationEvent event) {
        log.info("ApplicationStartedEvent");
    }

    @EventListener(ApplicationReadyEvent.class)
    public void applicationReadyEvent(ApplicationEvent event) {
        log.info("ApplicationReadyEvent");
    }

    @EventListener(ApplicationFailedEvent.class)
    public void applicationFailedEvent(ApplicationEvent event) {
        log.info("ApplicationFailedEvent");
    }

    @EventListener(ContextRefreshedEvent.class)
    public void contextRefreshedEvent(ApplicationEvent event) {
        log.info("ContextRefreshedEvent");
    }





}
