package com.tca.common.learning.spring.event.inner.listener.v3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

/**
 * @author an.zhou
 * @date 2023/5/22 16:44
 */
@Slf4j
public class CustomContextRefreshedListenerV3 {

    @EventListener({ContextRefreshedEvent.class})
    public void contextRefreshedEvent(ApplicationEvent event) {
        log.info("v3 listener, refreshed event = {}", event);
    }
}
