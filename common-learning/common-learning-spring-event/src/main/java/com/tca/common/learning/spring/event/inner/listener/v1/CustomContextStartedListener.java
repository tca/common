package com.tca.common.learning.spring.event.inner.listener.v1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;

/**
 * @author an.zhou
 * @date 2023/5/22 16:24
 */
@Slf4j
public class CustomContextStartedListener implements ApplicationListener<ContextStartedEvent> {
    @Override
    public void onApplicationEvent(ContextStartedEvent event) {
        log.info("started event = {}", event);
    }
}
