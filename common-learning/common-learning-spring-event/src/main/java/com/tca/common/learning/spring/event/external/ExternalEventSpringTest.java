package com.tca.common.learning.spring.event.external;

import com.tca.common.learning.spring.event.external.event.CustomEvent;
import com.tca.common.learning.spring.event.external.listener.CustomEventListener;
import com.tca.common.learning.spring.event.inner.listener.v3.CustomContextRefreshedListenerV3;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author an.zhou
 * @date 2023/5/22 16:55
 */
@Slf4j
public class ExternalEventSpringTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext();

        // register inner listeners
        ac.register(CustomContextRefreshedListenerV3.class);

        // register external listeners
        ac.register(CustomEventListener.class);

        ac.refresh();

        log.info("ac refreshed");

        // publish custom event
        ac.publishEvent(new CustomEvent("custom event"));
    }
}
