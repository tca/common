package com.tca.common.learning.spring.event.inner.listener.v1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * @author an.zhou
 * @date 2023/5/22 16:24
 */
@Slf4j
public class CustomContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.info("refreshed event = {}", event);
    }
}
