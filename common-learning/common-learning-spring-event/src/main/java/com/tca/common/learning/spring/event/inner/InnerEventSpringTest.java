package com.tca.common.learning.spring.event.inner;

import com.tca.common.learning.spring.event.inner.listener.v1.CustomContextClosedListener;
import com.tca.common.learning.spring.event.inner.listener.v1.CustomContextRefreshedListener;
import com.tca.common.learning.spring.event.inner.listener.v1.CustomContextStartedListener;
import com.tca.common.learning.spring.event.inner.listener.v1.CustomContextStoppedListener;
import com.tca.common.learning.spring.event.inner.listener.v2.CustomContextRefreshedListenerV2;
import com.tca.common.learning.spring.event.inner.listener.v3.CustomContextRefreshedListenerV3;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author an.zhou
 * @date 2023/5/22 15:51
 */
@Slf4j
public class InnerEventSpringTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext();
        // register v1 listeners
        ac.register(CustomContextRefreshedListener.class, CustomContextClosedListener.class
            , CustomContextStartedListener.class, CustomContextStoppedListener.class);
        // register v2 listeners
        ac.register(CustomContextRefreshedListenerV2.class);
        // register v3 listeners
        ac.register(CustomContextRefreshedListenerV3.class);

        ac.refresh();
        log.info("ac refreshed");

        ac.start();
        log.info("ac started");

        ac.stop();
        log.info("ac stopped");

        ac.close();
        log.info("ac closed");
    }
}
