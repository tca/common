package com.tca.common.learning.spring.event.inner.listener.v2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

/**
 * @author an.zhou
 * @date 2023/5/22 16:41
 */
@Slf4j
public class CustomContextRefreshedListenerV2 {

    @EventListener
    public void contextRefreshedEvent(ContextRefreshedEvent event) {
        log.info("v2 listener, refreshed event = {}", event);
    }
}
