package com.tca.common.learning.oss.minio.test.controller;

import cn.hutool.core.util.CharsetUtil;
import com.amazonaws.services.s3.model.S3Object;
import com.tca.common.core.bean.Result;
import com.tca.common.core.utils.ValidateUtils;
import com.tca.common.learning.oss.minio.bean.OssTemplate;
import com.tca.common.learning.oss.minio.test.req.WriteObjectReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author zhoua
 * @date 2022/11/25 19:09
 */
@RestController
@Slf4j
@RequestMapping("/oss/object")
public class ObjectController {

    @Autowired
    private OssTemplate ossTemplate;

    /**
     * 上传文件
     * @param writeObjectReq
     * @return
     */
    @PutMapping("/write")
    public Result writeObject(WriteObjectReq writeObjectReq) {
        try {
            ossTemplate.putObject(writeObjectReq.getBucketName(),
                    URLEncoder.encode(writeObjectReq.getObjectName(), CharsetUtil.UTF_8),
                    writeObjectReq.getMultipartFile().getInputStream(),
                    writeObjectReq.getMultipartFile().getSize(),
                    writeObjectReq.getMultipartFile().getContentType());
        } catch (Exception e) {
            log.error("写数据失败", e);
            return Result.failure();
        }
        return Result.success();
    }

    /**
     * 获取文件详情
     * @param bucketName
     * @param objectName
     * @return
     */
    @GetMapping("/{bucketName}/{objectName}")
    public Result getObject(@PathVariable(value = "bucketName") String bucketName,
                            @PathVariable(value = "objectName") String objectName) {

        S3Object object = null;
        try {
            object = ossTemplate.getObject(bucketName,
                    URLEncoder.encode(objectName, CharsetUtil.UTF_8));
        } catch (UnsupportedEncodingException e) {
            log.error("操作失败", e);
        }
        if (ValidateUtils.isEmpty(object)) {
            return Result.failure("文件不存在");
        }
        return Result.success(object);
    }

    /**
     * 获取外链
     * @param bucketName
     * @param objectName
     * @return
     */
    @GetMapping("/accessUrl/{bucketName}/{objectName}")
    public Result<String> getObjectUrl(@PathVariable(value = "bucketName") String bucketName,
                               @PathVariable(value = "objectName") String objectName) {
        try {
            // 判断文件是否存在
            S3Object object = ossTemplate.getObject(bucketName,
                    URLEncoder.encode(objectName, CharsetUtil.UTF_8));
            if (ValidateUtils.isEmpty(object)) {
                return Result.failure("文件不存在");
            }
            // 获取外链
            return Result.success(ossTemplate.getObjectURL(bucketName, URLEncoder.encode(objectName, CharsetUtil.UTF_8), 2));

        } catch (UnsupportedEncodingException e) {
            log.error("操作失败", e);
        }
        return Result.failure();
    }

    /**
     * 获取文件上传url
     * @param bucketName
     * @param objectName
     * @return
     */
    @GetMapping("/uploadUrl/{bucketName}/{objectName}")
    public Result<String> getUploadUrl(@PathVariable(value = "bucketName") String bucketName,
                                       @PathVariable(value = "objectName") String objectName) {

        try {
            String uploadUrl = ossTemplate.getUploadUrl(bucketName, URLEncoder.encode(objectName, CharsetUtil.UTF_8));
            return Result.success(uploadUrl);
        } catch (UnsupportedEncodingException e) {
            log.error("获取上传路径失败", e);
        }
        return Result.failure();
    }

    @DeleteMapping("/{bucketName}/{objectName}")
    public Result delete(@PathVariable(value = "bucketName") String bucketName,
                         @PathVariable(value = "objectName") String objectName) {
        try {
            // 判断文件是否存在
            S3Object object = ossTemplate.getObject(bucketName,
                    URLEncoder.encode(objectName, CharsetUtil.UTF_8));
            if (ValidateUtils.isEmpty(object)) {
                return Result.failure("文件不存在");
            }
            // 删除文件
            ossTemplate.removeObject(bucketName, URLEncoder.encode(objectName, CharsetUtil.UTF_8));
            return Result.success();
        } catch (Exception e) {
            log.error("操作失败", e);
        }
        return Result.failure();

    }


}
