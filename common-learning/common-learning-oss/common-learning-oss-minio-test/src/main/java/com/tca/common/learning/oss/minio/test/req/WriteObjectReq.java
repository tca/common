package com.tca.common.learning.oss.minio.test.req;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author zhoua
 * @date 2022/11/25 19:41
 */
@Data
public class WriteObjectReq {

    /**
     * bucketName
     */
    private String bucketName;

    /**
     * objectName
     */
    private String objectName;

    /**
     * 二进制文件
     */
    private MultipartFile multipartFile;

}
