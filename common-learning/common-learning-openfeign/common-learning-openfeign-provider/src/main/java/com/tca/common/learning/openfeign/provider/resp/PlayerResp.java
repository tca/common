package com.tca.common.learning.openfeign.provider.resp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author an.zhou
 * @date 2022/12/28 10:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerResp {

    private Integer id;

    private String name;

    private Integer age;
}
