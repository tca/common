package com.tca.common.learning.openfeign.provider.controller;

import com.alibaba.fastjson.JSONObject;
import com.tca.common.core.bean.Result;
import com.tca.common.learning.openfeign.provider.resp.PlayerResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author an.zhou
 * @date 2022/12/28 10:21
 */
@RestController
@RequestMapping("/player")
@Slf4j
public class PlayerController {

    @GetMapping("/getForString/{id}")
    public String getForString(@PathVariable Integer id) {
        log.info("获取用户, id = {}", id);
        if (id == 10) {
            return JSONObject.toJSONString(new PlayerResp(10, "Lionel Messi", 35));
        }
        if (id == 7) {
            return JSONObject.toJSONString(new PlayerResp(7, "Ronaldo", 37));
        }
        return "no such player";
    }

    @GetMapping("/getForResult/{id}")
    public Result<PlayerResp> getForResult(@PathVariable Integer id) {
        log.info("获取用户, id = {}", id);
        if (id == 10) {
            return Result.success(new PlayerResp(10, "Lionel Messi", 35));
        }
        if (id == 7) {
            return Result.success(new PlayerResp(7, "Ronaldo", 37));
        }
        return Result.failure("no such player");
    }

    @GetMapping("/getForException")
    public Result getForException() throws InterruptedException {
        TimeUnit.HOURS.sleep(1L);
        return Result.success();
    }

}
