package com.tca.common.learning.openfeign.feign.core;

import com.alibaba.fastjson.JSONObject;
import com.tca.common.core.bean.Result;
import com.tca.common.learning.openfeign.feign.core.client.PlayerFeignClient;
import feign.Feign;
import feign.FeignException;
import feign.Logger;
import feign.Response;
import feign.codec.DecodeException;
import feign.codec.Decoder;
import feign.gson.GsonDecoder;
import feign.httpclient.ApacheHttpClient;
import feign.slf4j.Slf4jLogger;
import lombok.extern.slf4j.Slf4j;


/**
 * @author an.zhou
 * @date 2022/12/28 13:52
 */
@Slf4j
public class MainTest {

    private static final String PROVIDER_URL = "http://localhost:10001";

    public static void main(String[] args) {

        testGetForString();

        testGetForResult();

    }

    /**
     * testGetForResult
     */
    private static void testGetForResult() {
        // 当返回类型不是String时，需要使用自定义解码器
        PlayerFeignClient playerFeignClient = Feign.builder()
                // 这里我们自定义的decoder使用的是feign-gson的GsonDecoder
                .decoder(new GsonDecoder())
                // 这里我们使用feign-httpclient作为客户端, 默认使用jdk自带的httpclient
                .client(new ApacheHttpClient())
                // 请求日志输出
                // 我们这里使用的是 common-log-slf4j-log4j2，即使用slf4j接口和log4j2的实现
                // 因为我们默认的log4j2.xml配置文件中 <root level="info"> 默认的日志级别是info，所以导致无法输出
                // 解决方案是 讲log4j2.xml配置文件拷贝到当前项目中，并且修改日志级别改为debug，日志可以正常输出
                .logger(new Slf4jLogger()).logLevel(Logger.Level.FULL)
                .target(PlayerFeignClient.class, PROVIDER_URL);
        Result result = playerFeignClient.getForResult(10);
        log.info("通过feign获取用户成功, result = {}", result);
        System.out.println("result = " + result);

    }

    /**
     * testGetForString
     */
    private static void testGetForString() {
        PlayerFeignClient playerFeignClient = Feign.builder()
                // 使用Logger.ErrorLogger时会有输出，其他Logger实现中不会输出
                .logger(new Logger.ErrorLogger()).logLevel(Logger.Level.FULL)
                .target(PlayerFeignClient.class, PROVIDER_URL);
        String result = playerFeignClient.getForString(10);
        log.info("通过feign获取用户成功, result = {}", result);
        System.out.println("result = " + result);
    }


}
