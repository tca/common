package com.tca.common.learning.openfeign.feign.core.client;

import com.tca.common.core.bean.Result;
import feign.Param;
import feign.RequestLine;

/**
 * @author an.zhou
 * @date 2022/12/28 13:32
 */
public interface PlayerFeignClient {

    /**
     * 获取用户
     * @param id
     * @return
     */
    @RequestLine("GET /player/getForString/{id}")
    String getForString(@Param("id") Integer id);

    /**
     * 获取用户
     * @param id
     * @return
     */
    @RequestLine("GET /player/getForResult/{id}")
    Result getForResult(@Param("id") Integer id);

}
