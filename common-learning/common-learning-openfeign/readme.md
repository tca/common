## 1.feign原理
![feign的原理图](./img/feign的原理图.png)

根据上图，可以看到feign中使用到的一些核心组件：contract、encoder、decoder、interceptor、logger、httpclient、retryer等

## 2.feign-core基本使用
[参考](https://cloud.tencent.com/developer/inventory/12988)

demo见 common-learning-openfeign-feign-core 代码部分

## 3.openfeign基本使用
[参考1](https://juejin.cn/post/7068179877047828517)
[参考2](https://juejin.cn/post/7079379888411115533)

#### 3.1 openfeign集成日志
###### 问题描述
```
有时候我们需要查看请求的报文, 则需要集成openfeign日志组件
```

###### 解决方案
step1 application.yml添加配置, 需要将feign接口的日志级别改为debug
```yaml
logging:
  level:
    root: info
    com.tca.common.learning.openfeign.consumer.client: debug
```

step2 自定义FeignClient配置类 CommonFeignConfig, 配置类中添加Logger.Level.FULL, 这里配置类上不需要添加@Configuration注解, 但是里面的
@Bean需要添加
```java
public class CommonFeignConfig {

    @Bean
    public Logger.Level loggerLevel(){
        return Logger.Level.FULL;
    }
}
```

step3 @FeignClient添加configuration配置类
```java
@FeignClient(name = "openfeign-provider", url = "localhost:10001", configuration = CommonFeignConfig.class)
public interface PlayerFeignClient {
    
}
```

###### 验证
```
2023-01-05 14:01:57.942|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] ---> GET http://localhost:10001/player/getForResult/10 HTTP/1.1
2023-01-05 14:01:57.942|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] ---> END HTTP (0-byte body)
2023-01-05 14:01:57.974|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] <--- HTTP/1.1 200 OK (28ms)
2023-01-05 14:01:57.974|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] connection: keep-alive
2023-01-05 14:01:57.975|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] content-type: application/json
2023-01-05 14:01:57.975|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] date: Thu, 05 Jan 2023 06:01:57 GMT
2023-01-05 14:01:57.975|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] transfer-encoding: chunked
2023-01-05 14:01:57.975|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] 
2023-01-05 14:01:57.976|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] {"code":200,"msg":"操作成功","data":{"id":10,"name":"L**********i","age":35}}
2023-01-05 14:01:57.978|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] <--- END HTTP (81-byte body)
```
可以看到, 在日志中可以看到请求报文


#### 3.2 OpenFeign拦截器
###### 问题描述
```
需要添加拦截器, 拦截请求报文, 并在请求头中添加：app-name属性和request-id属性
```

###### 解决方案
step1 自定义拦截器实现 RequestInterceptor 接口, 重写apply方法, 不需要添加@Configuration注解
```java
public class HeaderRequestInterceptor implements RequestInterceptor {

    @Value("${spring.application.name}")
    private String appName;

    private static final String APP_KEY = "app-name";

    private static final String REQUEST_ID = "request-id";

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(APP_KEY, appName);
        requestTemplate.header(REQUEST_ID, UUID.randomUUID().toString());
    }

}
```

step2 在配置类中添加HeaderRequestInterceptor, 并且将配置类作为@FeignClient的configuration属性
```java
public class CommonFeignConfig {

    @Bean
    public Logger.Level loggerLevel(){
        return Logger.Level.FULL;
    }

    @Bean
    public HeaderRequestInterceptor headerRequestInterceptor() {
        return new HeaderRequestInterceptor();
    }
}
```

###### 验证
```
2023-01-05 15:20:48.610|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] ---> GET http://localhost:10001/player/getForResult/10 HTTP/1.1
2023-01-05 15:20:48.610|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] app-name: ********************************er
2023-01-05 15:20:48.611|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] request-id: 55a1bc3b-1df9-4dd7-8cc0-34123d5f679c
2023-01-05 15:20:48.611|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] ---> END HTTP (0-byte body)
2023-01-05 15:20:48.653|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] <--- HTTP/1.1 200 OK (35ms)
2023-01-05 15:20:48.653|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] connection: keep-alive
2023-01-05 15:20:48.653|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] content-type: application/json
2023-01-05 15:20:48.653|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] date: Thu, 05 Jan 2023 07:20:48 GMT
2023-01-05 15:20:48.653|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] transfer-encoding: chunked
2023-01-05 15:20:48.653|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] 
2023-01-05 15:20:48.654|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] {"code":200,"msg":"操作成功","data":{"id":10,"name":"L**********i","age":35}}
2023-01-05 15:20:48.654|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] <--- END HTTP (81-byte body)
```
可以看到，在请求报文中多了 app-name 和 request-id 请求头信息

#### 3.3 OpenFeign常用配置文件配置
###### 问题描述
```
上述log组件以及拦截器组件, 我们都是通过configuration来配置的, 我们现在希望通过application.yml主配置文件进行相关配置
```

###### 解决方案
[参考](https://blog.csdn.net/Saintmm/article/details/125533249)
```
1.可以针对某个feign或者全部feign作配置
2.常用的feign组件配置：超时配置、日志级别、编解码器、拦截器、契约、重试策略
```
常用配置参考
```yaml
feign:
  client:
    config:
      # 针对所有的服务, 针对某一个具体服务则需要修改为对应的服务名称
      default:
        # Feign的连接建立超时时间, 默认为10秒
        connectTimeout: 5000
        # Feign的请求处理超时时间, 默认为60秒
        readTimeout: 5000
        # 日志级别
        loggerLevel: full
        # 拦截器配置（和@Bean的方式二选一）
        requestInterceptors:
          - com.tca.common.learning.openfeign.consumer.client.interceptor.HeaderRequestInterceptor
        # 错误解码器
        # errorDecoder:
        # 重试策略
        # retryer:
        # 是否对404错误码解码
        # decode404:
        # 编码器
        # encoder:
        # 解码器
        # decoder:
        # 契约
        # contract:
```
**注意**
```
1.这里我们配置了自定义的 requestInterceptors - HeaderRequestInterceptor, 在 HeaderRequestInterceptor 中我们使用
@Value注入spring.application.name注入属性, 但是采用配置的方式, @Value会失效, 导致属性值为null
```


## 4.待解决
#### 4.1 发送日志中, 请求报文中时间少8H, 时区不对
```
2023-01-05 15:20:48.653|DEBUG|XNIO-1 task-1|com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient:72|[PlayerFeignClient#getForResult] date: Thu, 05 Jan 2023 07:20:48 GMT
```

