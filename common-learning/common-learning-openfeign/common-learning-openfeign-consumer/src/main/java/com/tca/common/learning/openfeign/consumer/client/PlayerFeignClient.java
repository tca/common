package com.tca.common.learning.openfeign.consumer.client;

import com.tca.common.core.bean.Result;
import com.tca.common.learning.openfeign.consumer.config.CommonFeignConfig;
import com.tca.common.learning.openfeign.consumer.resp.PlayerResp;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author an.zhou
 * @date 2023/1/5 10:38
 */
// 使用configuration配置OpenFeign组件
//@FeignClient(name = "openfeign-provider", url = "localhost:10001", configuration = CommonFeignConfig.class)
// 使用application.yml主配置文件的方式配置OpenFeign组件, 不需要单独指定configuration属性
@FeignClient(name = "openfeign-provider", url = "localhost:10001")
public interface PlayerFeignClient {

    /**
     * 获取用户
     * @param id
     * @return
     */
    @GetMapping("/player/getForString/{id}")
    String getForString(@PathVariable("id") Integer id);

    /**
     * 获取用户
     * @param id
     * @return
     */
    @GetMapping("/player/getForResult/{id}")
    Result<PlayerResp> getForResult(@PathVariable("id") Integer id);

    /**
     * 获取异常
     * @return
     */
    @GetMapping("/player/getForException")
    Result getForException();

}
