package com.tca.common.learning.openfeign.consumer.client.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Value;

import java.util.UUID;

/**
 * @author an.zhou
 * @date 2023/1/5 14:56
 */
public class HeaderRequestInterceptor implements RequestInterceptor {

    @Value("${spring.application.name}")
    private String appName;

    private static final String APP_KEY = "app-name";

    private static final String REQUEST_ID = "request-id";

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(APP_KEY, appName);
        requestTemplate.header(REQUEST_ID, UUID.randomUUID().toString());
    }

}
