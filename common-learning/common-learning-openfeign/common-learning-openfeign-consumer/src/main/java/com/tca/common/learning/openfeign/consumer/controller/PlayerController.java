package com.tca.common.learning.openfeign.consumer.controller;

import com.tca.common.core.bean.Result;
import com.tca.common.learning.openfeign.consumer.client.PlayerFeignClient;
import com.tca.common.learning.openfeign.consumer.resp.PlayerResp;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author an.zhou
 * @date 2023/1/5 10:45
 */
@RestController
@RequestMapping("/player")
@AllArgsConstructor
public class PlayerController {

    private final PlayerFeignClient playerFeignClient;

    @GetMapping("/getForString/{id}")
    public String getForString(@PathVariable Integer id) {
        return playerFeignClient.getForString(id);
    }

    @GetMapping("/getForResult/{id}")
    public Result<PlayerResp> getForResult(@PathVariable Integer id) {
        return playerFeignClient.getForResult(id);
    }

    @GetMapping("/getForException")
    public Result getForException() {
        return playerFeignClient.getForException();
    }
}
