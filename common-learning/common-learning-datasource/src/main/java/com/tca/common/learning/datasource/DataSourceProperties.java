package com.tca.common.learning.datasource;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author zhoua
 * @date 2024/1/6 11:02
 */
@Data
@AllArgsConstructor
public class DataSourceProperties {

    public DataSourceProperties(String url, String driver, String username, String password) {
        this.url = url;
        this.driver = driver;
        this.username = username;
        this.password = password;
    }

    /**
     * url
     */
    private String url;

    /**
     * driver
     */
    private String driver;

    /**
     * username
     */
    private String username;

    /**
     * password
     */
    private String password;

    /**
     * 最大连接数
     */
    private Integer maxConnectionNum = 10;

    /**
     * 最大等待连接数
     */
    private Integer maxIdleConnectionNum = 10;

    /**
     * 最大等待时间, 单位:ms
     */
    private Integer poolTimeToWait = 3000;

}
