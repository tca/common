package com.tca.common.learning.datasource;

import java.sql.*;

/**
 * @author zhoua
 * @date 2024/1/6 11:25
 */
public class RawJdbcTest {

    public static void main(String[] args) throws SQLException {
        // 获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_test?serverTimezone=GMT%2B8&useSSL=false&useUnicode=true&characterEncoding=utf-8", "root", "admin");
        // 创建语句
        PreparedStatement statement = connection.prepareStatement("select * from info");
        // 执行语句
        ResultSet resultSet = statement.executeQuery();
        // 遍历结果集
        while (resultSet.next()) {
            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            System.out.println("id = " + id + " name = " + name);
        }
        // 关闭结果集
        resultSet.close();
        // 关闭连接
        connection.close();
    }
}
