package com.tca.common.learning.datasource;

import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zhoua
 * @date 2024/1/6 19:21
 */
@Slf4j
public class CustomDataSourceTest {


    static final DataSourceProperties dataSourceProperties = new DataSourceProperties("jdbc:mysql://localhost:3306/db_test?serverTimezone=GMT%2B8" +
            "&useSSL=false&useUnicode=true&characterEncoding=utf-8", "com.mysql.cj.jdbc.Driver", "root", "admin");

    static final DataSource dataSource = new CustomDataSource(dataSourceProperties);

    public static void main(String[] args) throws SQLException {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            executorService.execute(() -> {
                try {
                    execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        }
        executorService.shutdown();
    }

    static void execute() throws SQLException {
        // 获取连接
        Connection connection = dataSource.getConnection();
        // 创建语句
        PreparedStatement statement = connection.prepareStatement("select * from info");
        // 执行语句
        ResultSet resultSet = statement.executeQuery();
        // 遍历结果集
        while (resultSet.next()) {
            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
//            System.out.println("id = " + id + " name = " + name);
        }
        // 关闭结果集
        resultSet.close();
        // 关闭连接
        connection.close();
    }
}
