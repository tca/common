# 自定义数据库连接池

step1 自定义 CustomDataSource extends DataSource, 实现默认非核心方法

step2 自定义 DataSourceProperties, 存储数据库连接核心配置以及连接池的核心配置

step3 创建 Connection 的代理类, 为什么??? 
1.在没有连接池时, 我们使用原生jdbc操作数据库如下, 使用DriverManager获取连接对象Connection, 
操作结束之后, 需要调用连接对象Connection的close方法, 关闭连接
```java
public static void main(String[] args) throws SQLException {
    // 获取连接
    Connection connection = DriverManager.getConnection(
            "jdbc:mysql://localhost:3306/db_test?serverTimezone=GMT%2B8" +
        "&useSSL=false&useUnicode=true&characterEncoding=utf-8", "root", "admin");
    // 创建语句
    PreparedStatement statement = connection.prepareStatement("select * from info");
    // 执行语句
    ResultSet resultSet = statement.executeQuery();
    // 遍历结果集
    while (resultSet.next()) {
        Long id = resultSet.getLong("id");
        String name = resultSet.getString("name");
        System.out.println("id = " + id + " name = " + name);
    }
    // 关闭结果集
    resultSet.close();
    // 关闭连接
    connection.close();
}
```

2.在使用连接池之后, 我们使用连接池操作数据库如下, 希望使用DataSource替代DriverManager对象, 来获取连接, 
但操作完之后在调用Connection对象的close方法时, 不能直接关闭连接, 需要将连接对象返回给连接池
```java
static final DataSourceProperties dataSourceProperties = new DataSourceProperties("jdbc:mysql://localhost:3306/db_test?serverTimezone=GMT%2B8" +
        "&useSSL=false&useUnicode=true&characterEncoding=utf-8", "com.mysql.cj.jdbc.Driver", "root", "admin");

static final DataSource dataSource = new CustomDataSource(dataSourceProperties);

public static void main(String[] args) throws SQLException{
    // 获取连接
    Connection connection = dataSource.getConnection();
    // 创建语句
    PreparedStatement statement = connection.prepareStatement("select * from info");
    // 执行语句
    ResultSet resultSet = statement.executeQuery();
    // 遍历结果集
    while (resultSet.next()) {
    Long id = resultSet.getLong("id");
    String name = resultSet.getString("name");
    System.out.println("id = " + id + " name = " + name);
    }
    // 关闭结果集
    resultSet.close();
    // 关闭连接
    connection.close();
}

```
通过上述代码, 我们可以预见: 使用原生jdbc的方式下, 获取的连接对象Connection就是具体的实现, 如mysql的实现,
所以调用其close方法, 一定是直接关闭连接操作。因此在使用连接池时, 我们返回的Connection对象就不一定不能是原生
的mysql连接实现对象, 需要作代理, 重写其close方法！！！

step4 为什么使用动态代理, 而不使用静态代理?
1.这里为什么用动态代理，不用静态代理。如果使用静态代理，我们有两种方式，一种是has-a方式，
即代理对象包含了目标对象，但是此时，我们的代理对象需要重写全部的目标对象的方法，显然不太好，
因为目标对象可能有很多，比如oracle的、mysql的等等；第二种是is-a方式，即采用继承的方式，
此时我们只需要重写部分方法即可，但是又有一个问题，我们的代理对象需要继承很多目标对象，
比如oracle的、mysql的，等等。
2.所以这里采用动态代理的方式，不管目标对象是oracle的还是mysql的，都没关系，我只需要重写部分方法，如close方法，
其他都直接在invoke方法中调用目标对象方法即可

step5 在代理对象中重写close方法，调用close方法时，使用代理对象, 其他方法都使用目标方法

