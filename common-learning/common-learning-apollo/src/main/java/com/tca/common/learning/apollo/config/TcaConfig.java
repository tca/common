package com.tca.common.learning.apollo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author an.zhou
 * @date 2022/12/29 20:56
 */
@ConfigurationProperties(prefix = "tca")
@Data
@Component
public class TcaConfig {

    private String name;

    private String family;

    private Integer age;
}


