package com.tca.common.learning.apollo.controller;

import com.tca.common.core.bean.Result;
import com.tca.common.core.utils.BeanUtils;
import com.tca.common.learning.apollo.config.TcaConfig;
import com.tca.common.learning.apollo.resp.TcaConfigResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author an.zhou
 * @date 2022/12/29 20:39
 *
 * 1.使用 @ConfigurationProperties 注入属性的 Config 无法感知到配置中心配置文件的变化
 * 2.使用 @Value 注入属性的变量 可以感知到配置中心配置文件的变化
 */
@RestController
@RequestMapping("/config")
public class ConfigController {

   @Autowired
   private TcaConfig tcaConfig;

   @Value("${tca.age}")
   private Integer age;

    @GetMapping("/tca")
    public Result<TcaConfigResp> getTcaConfig() {
        return Result.success(BeanUtils.copyProperties(tcaConfig, TcaConfigResp.class));
    }

    @GetMapping("/tca/age")
    public Result<Integer> getTcaAge() {
        return Result.success(age);
    }
}
