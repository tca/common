package com.tca.common.learning.apollo.resp;

import lombok.Data;

/**
 * @author an.zhou
 * @date 2022/12/29 21:11
 */
@Data
public class TcaConfigResp {

    private String name;

    private String family;

    private Integer age;
}
