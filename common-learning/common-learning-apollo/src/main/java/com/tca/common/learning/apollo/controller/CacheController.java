package com.tca.common.learning.apollo.controller;

import com.tca.common.cache.api.CacheHelper;
import com.tca.common.core.bean.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

/**
 * @author an.zhou
 * @date 2022/12/30 14:31
 */
@RestController
@RequestMapping("/cache")
public class CacheController {

    @Autowired
    private CacheHelper cacheHelper;

    private static final String CACHE_KEY = "player:10";

    private static final String CACHE_VALUE = "Lionel Messi";

    @PostMapping("/set")
    public Result set() {
        cacheHelper.set(CACHE_KEY, CACHE_VALUE, 2, TimeUnit.HOURS);
        return Result.success();
    }

    @DeleteMapping("/delete")
    public Result delete() {
        cacheHelper.delete(CACHE_KEY);
        return Result.success();
    }

    @GetMapping("/get")
    public Result<String> get() {
        return Result.success(cacheHelper.get(CACHE_KEY));
    }
}
