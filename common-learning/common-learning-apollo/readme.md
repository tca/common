## 1.搭建Apollo本地服务端
[参考](https://github.com/apolloconfig/apollo-quick-start)

## 2.Apollo官方文档
[官方使用指南](https://www.apolloconfig.com/#/zh/usage/apollo-user-guide)
[官方java使用文档](https://www.apolloconfig.com/#/zh/usage/java-sdk-user-guide)

## 3.核心概念
#### 3.1 应用(项目)
#### 3.2 环境
#### 3.3 集群
#### 3.4 命名空间
###### 公共组件命名空间
```
我们的一些公共配置，比如几个应用连接的是同一个redis数据库配置，我们可以为当前redis公共组件创建命名空间，在当前命名空间中
，其他应用都可以引用
```
###### 多个应用共享namespace
[官方文档指南](https://www.apolloconfig.com/#/zh/usage/apollo-user-guide?id=_22-%e5%85%ac%e5%85%b1%e7%bb%84%e4%bb%b6%e6%8e%a5%e5%85%a5%e6%ad%a5%e9%aa%a4)
###### 最佳实践
```
这里需要注意的是：namespace是隶属于项目的，所以对于公共的配置，如公共的mysql连接配置，公共的redis连接配置，
我们可以创建一个common的项目，在common项目下创建mysql的命名空间，redis的命名空间，提供给其他项目进行引用
```

## 4.其他使用说明
#### 4.1 文本配置格式
```
在 web 端使用文本方式编辑配置项时，默认只支持properties语法格式，不支持yaml语法
```

#### 4.2 客户端感知配置变化
```
1.使用 @Value 注入属性的变量 可以感知到配置中心配置文件的变化
2.使用 @ConfigurationProperties 注入属性的 Config 无法感知到配置中心配置文件的变化，即使添加了springcloud的@RefreshScope注解也不起作用
```
**特别地, 项目中如果依赖了spring-boot-devtools时, 会开启热部署, 此时修改配置时项目会重启启动, 加载新的配置**

#### 4.3 客户端感知@ConfigurationProperties配置变化
具体见 @ApolloTcaConfigListener
```java
@Component
@Slf4j
public class ApolloTcaConfigListener {

    @ApolloConfig
    private Config config;

    @Autowired
    private TcaConfig tcaConfig;

    @ApolloConfigChangeListener
    public void configChangeListener(ConfigChangeEvent configChangeEvent) {
        refreshTcaConfig(configChangeEvent);
    }

    private void refreshTcaConfig(ConfigChangeEvent configChangeEvent) {
        Set<String> changedKeys = configChangeEvent.changedKeys();
        List<Method> methodList = Arrays.stream(tcaConfig.getClass().getMethods()).filter(m -> m.getName().startsWith("set"))
                .collect(Collectors.toList());

        for (String changedKey : changedKeys) {
            if (changedKey.startsWith("tca")) {
                ConfigChange configChange = configChangeEvent.getChange(changedKey);
                log.info("key : {}, has changed, oldValue = {}, newValue = {}", changedKey, configChange.getOldValue()
                        , configChange.getNewValue());
                String key = changedKey.substring(4);
                try {
                    List<Method> matchedMethodList = methodList.stream().filter(m -> m.getName().equalsIgnoreCase("set" + key))
                            .collect(Collectors.toList());
                    if (ValidateUtils.isEmpty(matchedMethodList)) {
                        throw new RuntimeException("no such method, methodName = " + "get" + key);
                    }
                    Method method = matchedMethodList.get(0);
                    Class<?> parameterType = method.getParameterTypes()[0];
                    if (parameterType == Integer.class) {
                        method.invoke(tcaConfig, Integer.parseInt(configChange.getNewValue()));
                    } else if (parameterType == Long.class) {
                        method.invoke(tcaConfig, Long.parseLong(configChange.getNewValue()));
                    } else if (parameterType == Double.class) {
                        method.invoke(tcaConfig, Double.parseDouble(configChange.getNewValue()));
                    } else {
                        method.invoke(tcaConfig, configChange.getNewValue());
                    }
                } catch (Exception e) {
                    log.error("转换出错", e);
                }
            }
        }
    }
}
```
[参考](https://www.cnblogs.com/qdhxhz/p/13449285.html)

