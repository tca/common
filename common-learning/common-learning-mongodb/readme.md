# MongoDB
## 基本概念
* database：数据库（对应关系型数据库中的数据库） 
* collection：集合（对应关系型数据库中的表）
* document：文档（对应关系型数据库中的行数据）
* field：字段、域（对应关系型数据库中的列）
* index：索引（对应关系型数据库中的索引）
* primary key：主键索引（对应关系型数据库中的主键索引）

## 数据类型
### 说明
```
MongoDB文档存储是使用BSON类型，BSON（short for Binary JSON，is a binary-encoded serialization of JOSN）是二进制序列化形式。
类似JSON，同样支持内嵌各种类型。
```
### 常用的数据类型
* String
* Integer
* Boolean
* Double
* Min/Max keys - 将一个值与 BSON（二进制的 JSON）元素的最低值和最高值相对比。
* Array
* Timestamp
* Object - 用于内嵌文档
* Null
* Symbol - 符号。该数据类型基本上等同于字符串类型，但不同的是，它一般用于采用特殊符号类型的语言。
* Date - 日期时间。用 UNIX 时间格式来存储当前日期或时间。你可以指定自己的日期时间：创建 Date 对象，传入年月日信息。
* Object ID - 对象 ID。用于创建文档的 ID。(每个文档都有)
* Binary Data - 二进制数据。用于存储二进制数据。
* Code - 代码类型。用于在文档中存储 JavaScript 代码。
* Regular expression - 正则表达式

## 安装&启动
[参考](https://blog.51cto.com/u_13657808/5657758)
### 服务端启动
```shell
./bin/mongod -dbpath ${data存储路径}
```
### 客户端接入
```shell
./bin/mongo
```

## 基本操作
[参考](https://www.cnblogs.com/jingzh/p/16858276.html)
### 用户管理
1.添加用户
```shell
# user：用户名、pwd：密码、roles：角色 【用户是跟数据库绑定的】
# admin数据库比较特殊，从权限的角度来看，这是root数据库。要是将一个用户添加到这个数据库，这个用户自动继承所有数据库的权限
db.createUser(
{"user":"tca", "pwd":"123456", "roles":[{"role":"root", "db":"admin"}]}
)
```
2.查看用户
```shell
show users
```
3.认证用户
```shell
# db.auth(用户名, 密码)
db.auth("tca", "123456")
```
4.删除用户
```shell
# db.dropUser(用户名)
db.dropUser("tca")
```

### 数据库管理
1.查看数据库
```shell
# 除了系统自带的三个库：admin、config、local
show dbs
show databases
```
2.创建数据库/切换数据库
```shell
# 当数据库存在时会切换数据库；数据库不存在时会先创建数据库后再切换
# 注意：如果只是创建数据库但是没有数据时，使用 show dbs 时是看不到该数据库实例的
use ${dbname}
```
3.删除数据库
```shell
# 删除当前数据库
db.dropDatabase()
```

### 集合操作
1.查看集合
```shell
show collections
show tables
```
2.创建集合
```shell
# name: 集合名称，options：集合参数
# 并不一定要创建集合，可以直接向某个不存在的集合中插入文档，集合会自动创建，如: db.student.insert({"name":"tca"})，会创建student库
db.createCollection(name, options)
```
3.查看集合详情
```shell
db.${集合名称}.stats()
```
4.删除集合
```shell
db.${集合名称}.drop()
```

### 文档操作
1.插入文档
```shell
# 可以插入单条或多条数据，多条数据时使用jsonArray
db.${集合名称}.insert(${json数据})
# 插入单条数据
db.${集合名称}.insertOne(${json数据})
# 插入多条数据
db.${集合名称}.insertMany(${jsonArray数据})

# 该命令比较特殊，可以是新增数据，当传入的json数据中带有"_id"属性时，会全量更新，注意这个指令一次只能更新一条数据
db.${集合名称}.save(${json数据})
```
2.更新文档
update根据条件更新
```shell
# eg：db.person.update({}, {"$set": {"club": "Barcelona"}})  将person集合中所有文档的club更新为Barcelona，这里需要注意update默认只更新符合条件的第一条数据，当用了第三个参数{"multi":true}时，才会
#	全量更新

# 常用操作符: inc, set, unset, push, pushAll, pull, pullAll

# 两个参数 【找到符合更新条件的第一条数据会更新，后面的文档数据即使符合更新条件，也不会更新】
db.${集合名称}.update(${更新条件},{"$set":更新内容})
# 三个参数 【相比于两个参数的，第三个参数的意思是只要匹配条件就都会批量更新】
db.${集合名称}.update(${更新条件},{"$set":更新内容},{"multi":true})
# 四个参数 【第三个参数表示upsert(upsert表示如果没有匹配到数据，就会执行插入操作)，第四个参数表示multi】
db.${集合名称}.update(${更新条件}, {"$set":更新内容}, true|false, true|false)
```

3.查询文档

这里的查询包括：

​	1.条件查询

​		大小比较、and、or多条件查询、in、not in 、not equal、正则表达式

​	2.投影字段，即：查询哪些字段

```shell
# 查询条件使用json， 如 {"name": "tca"} ，查询中常包含的：大小比较、and、or连接、in、nin（not in）等
# 如：{"$or": [{"name": "张三"}, {"age": 30}]} ，注意：1.这里or需要使用$符号 2.$or后面必须接jsonArray 3.当然，$and，$in，$nin 也是，后面需要接jsonArray
# 如：db.person.find({"$or": [{"name":"张三"}, {"age": {"$gte": 30}}]}).pretty()，这里查询name为张三或年龄大于等于30的人员信息

# 投影使用json，类似于sql中的select，1表示只展示什么字段，0表示除了某个字段都进行展示，如 {"name": 1, "age": 1}表示展示name和age字段

# 可以加pretty()，以换行的json进行展示

# 特殊的，查看文档中是否存在某个字段：$exist
# 如：找到没有age字段的文档，{"age": {"exists": 0}}

# 正则表达式，如：{"name": {"$regex":/^张.*/}}，这里查找name以张开头的文档

db.${集合名称}.find(${查询条件}, ${投影})
db.${集合名称}.findOne(${查询条件}, ${投影})
```

4.分页和排序

```shell
# 排序字段，如{"name": 1}表示按name正序排，-1表示倒序
# {"name":1, "age":-1}表示先按name正序，name相同时，按age倒序
db.${集合名称}.find(${查询条件}, ${投影}).sort(${排序条件})
# .skip().limit() 中的两个参数类似于 mysql中limit后面的两个参数
db.${集合名称}.find(${查询条件}, ${投影}).skip(${跳过多少条}).limit(${本页展示几条数据})
```

5.聚合查询

```shell
# aggregate里面接收的是json数组
# 如：db.${集合名称}.aggregate([{"$group":{"_id":null, "count":{"$sum":1}}}])，"_id":null 表示不进行分组，"count":{"$sum":1} count表示别名，"$sum":1 表示每统计一行，进行加一，即当前语句类似于sql中 select count(*) as `count` from table
```

求和操作

```sql
# 统计条数
# 如：db.${集合名称}.aggregate([{"$group":{"_id":null, "count":{"$sum":1}}}])，"_id":null 表示不进行分组，"count":{"$sum":1} count表示别名，"$sum":1 表示每统计一行，进行加一，即当前语句类似于sql中 select count(*) as `count` from table

# 对字段求和
# 如: db.${集合名称}.aggregate([{"$group": {"_id":null, "count": {"$sum":"$age"}}}])，这里表示对age年龄求和
```

分组操作

```sql
# 分组统计
# 如：db.player.aggregate([{"$group": {"_id": "$club", "count": {"$sum": 1}}}]) 这里，分组字段是club，类似于sql：
#	select club, count(1) as `count` from player group by club
```

先过滤再分组/先分组再过滤

```sql
# 有时候，我们需要先过滤之后再进行分组，类似于mysql中 select xxx from xxx where xxx group by xxx
# 比如下面就是先过滤年龄大于30之后的，再进行分组
db.player.aggregate([
    {
        "$match": {"age": {"$gt": 30}}
    },
    {
        "$group": {"_id": "$club", "count": {"$sum": 1}}   
    }
])

# 有时候，我们需要先分组再过滤，类似于mysql中 select xxx from xxx group by xxx having xxx
db.player.aggregate([
    {
        "$group": {"_id": "$club", "count": {"$sum": 1}}   
    },
    {
        "$match": {"_id": "FCB"}
    }
])

# 可以看出，这里取决于$match所在的位置，在$group之前则类似于where，在$group之后则类似于having
```

最大、最小、平均

```sql
# 最大、最小、平均的函数为：$max、$min、$avg
db.player.aggregate([
    {
        "$group": {
            "_id": "$club",
            "max_age": {"$max": "$age"}
        }
    }    
])

# 如上面，根据club分组后，取出分组后每一组age最大的年龄数
```

特殊处理 push

```sql
# 这里有一个sql不具备的功能，sql中使用group by聚合函数后，select中除了分组字段，涉及到其他字段的需要使用聚合函数，如：max、min、avg、count等，但是mongodb中提供了一种功能，如下，这里会先根据club字段分组，分组后，将该分组内所有的age都添加到一个数组中，字段名称为all_age：
db.player.aggregate([
    {
       "$group": {"_id": "$club", "all_age": {"$push": "$age"}} 
    }
])
```



6.删除文档

```shell
db.${集合名称}.deleteOne(${删除条件})
db.${集合名称}.delete(${删除条件})
```



## 索引

**说明**

```
1.索引不能修改，只能删除并重新创建
```

### 创建索引

```shell
# 参数一指定索引关联的列，正数表示正序，负数表示倒序，如db.person.createIndex({"age":1})，表示以年龄创建正序索引
# 参数二：background，默认false，表示前台创建，创建索引时会阻塞对集合的增删改操作（类似于mysql的锁表），一般用true，表示后台创建，不会锁表；参数二还可以添加其他索引，如: name - 索引名称
db.${集合名称}.createIndex({参数一},{参数二})
```

### 查询索引

```shell
db.${集合名称}.getIndexes()

> db.person.getIndexes()
[
	{
		"v" : 2, # 版本，固定值
		"key" : {
			"_id" : 1
		},
		"name" : "_id_",
		"ns" : "phoenix-person.person"
	},
	{
		"v" : 2,
		"key" : {
			"name" : 1
		},
		"name" : "name_1",
		"ns" : "phoenix-person.person"
	}
]

```

### 查询创建索引的列

```shell
db.${集合名称}.getIndexKeys()

# 举例
> db.person.getIndexKeys();
[ { "_id" : 1 }, { "name" : 1 } ]
```

### 查询索引总大小

```shell
# 说明 可以传参数，当 不穿参数或者参数传0或者参数传false，都是索引总大小
# 如果参数传其他任意，都是查看每个索引的大小以及总大小
db.${集合名称}.totalIndexSize()

# 举例
> db.person.totalIndexSize();
57344
> db.person.totalIndexSize(1);
_id_	36864
name_1	20480
57344
```

### 删除索引

```shell
# 删除单个索引，这里直接传索引名称字符串就ok，不用json！！！
db.${集合名称}.dropIndex(${索引名称})
# 删除所有自建索引（所以主键索引不会删除）
db.${集合名称}.dropIndexes()
```

### 重建索引

```shell
# 数据量慢慢变大时，需要删除之前的索引，重新创建索引（重新达到平衡），尽量少用
db.${集合名称}.reIndex()
```

### 索引类型

* 单字段索引

```
创建索引时对应的字段只有一个
```

* 交叉索引

```
当查询条件有多个字段，且多个字段都有对应索引
```

* 复合索引

```
类似mysql中的联合索引，即使用多个字段作为索引
```

* 多key索引

```
字段类型为数组
```

### 查询分析 - explain

```shell
> db.person.find({"name":"张三"}).explain();

{
	"queryPlanner" : {
		"plannerVersion" : 1,
		"namespace" : "phoenix-person.person",
		"indexFilterSet" : false,
		"parsedQuery" : {
			"name" : {
				"$eq" : "张三"
			}
		},
		"queryHash" : "01AEE5EC",
		"planCacheKey" : "4C5AEA2C",
		"winningPlan" : {
			"stage" : "FETCH",
			"inputStage" : {
				# IXSCAN表示使用到了索引扫描 - index scan
				"stage" : "IXSCAN",
				"keyPattern" : {
					"name" : 1
				},
				"indexName" : "name",
				# 是都为多key索引
				"isMultiKey" : false,
				"multiKeyPaths" : {
					"name" : [ ]
				},
				# 是否为唯一索引
				"isUnique" : false,
				# 是否为稀疏索引
				"isSparse" : false,
				# 是否为部分索引
				"isPartial" : false,
				"indexVersion" : 2,
				"direction" : "forward",
				"indexBounds" : {
					"name" : [
						"[\"张三\", \"张三\"]"
					]
				}
			}
		},
		"rejectedPlans" : [ ]
	},
	"serverInfo" : {
		"host" : "MBYDVPFW5GKP.local",
		"port" : 27017,
		"version" : "4.2.24",
		"gitVersion" : "5e4ec1d24431fcdd28b579a024c5c801b8cde4e2"
	},
	"ok" : 1
}
```

### 索引特性

* 唯一索引

```shell
# 创建索引时使用 unique:true(默认是false)
db.${集合名称}.createIndex({索引字段:排序规则}, {"unique":true, "name":"xxx", "background":true})

# 注意：在使用某个key创建唯一索引时，如果集合中包含了该key对应同一个value的文档多于一个，则会创建失败。同样的，如果在某个key上已经创建了唯一索引，则新增文档时不能违反唯一性原则，否则会创建失败
```

* 部分索引

```shell
# 创建索引时，只有满足条件的文档才会被维护在索引树里 partialFilterExpression，后面可以接条件
# 在查询语句中使用到查询条件，才会用到该索引
db.${集合名称}.createIndex({索引字段:排序规则}, {"partialFilterExpression":{}, "name":"xxx", "background":true})
```

* 稀疏索引

```sql
# 索引的稀疏属性确保索引「只包含具有索引字段的文档」的条目，索引将跳过没有索引字段的文档
# 如果稀疏索引会导致查询和排序操作的结果集不完整，MongoDB将不会使用该索引，除非hint()明确指定索引
# 同时具有稀疏性和唯一性的索引可以防止集合中存在字段值重复的文档，但允许不包含此索引字段的文档插入！！！
```

[稀疏索引参考](https://blog.csdn.net/li1325169021/article/details/124578060)

* 索引覆盖

```sql
# 类似于mysql中的索引覆盖
```



## 高可用

### 复制集（Replication Set）

#### **说明**

```
分为主节点、从节点、仲裁节点，类似于redis中的主从架构+哨兵模式。

主节点负责处理客户端读写请求，从节点负责复制主节点上的数据，（从节点也可以处理客户端的读请求，默认配置下，从节点不处理客户端请求）。仲裁节点不存储数据，作用是当主节点出现故障时，选举出某个备用节点成为主节点，保证mongo的正常服务。

主节点记录在其上的所有操作oplog（操作日志），从节点定期轮询主节点获取这些操作，然后对自己的数据副本执行这些操作，从而保证从节点的数据与主节点一致。
```

#### 搭建

##### 创建文件目录

```
数据文件目录
配置文件目录
日志文件目录
进程文件目录
```

##### 配置文件

[配置文件详解参考](https://www.cnblogs.com/oldSimon/p/16894714.html)

#### 设置从节点是否可读

```shell
# 此方法在从节点上执行，param的值可以是：primary - 主节点读、 primaryPreferred - 优先从主节点读、secondary - 从节点读、secondaryPreferred - 优先从从节点读、nearest - 从最近的节点读
db.getMongo().setReadPref(param)
```

### 

### 分片集群（shard cluster）

#### 说明

```
sharding方案将整个数据集拆分成为多个更小的chunk，并分布在整个集群中多个mongod节点上，最终达到存储和负载能力扩容、压力分流的作用。在sharding架构中，每个负责存储一部分数据的mongod节点称为shard（分片），shard上分布的数据块成为chunk，collections可以根据“shard key”（称为分片键）将数据集拆分为多个chunks，并相对均衡地分布在多个shards上。（同一个collection的）
```

#### 重要概念

##### Shard

```
用于存储实际的数据块，实际生产环境中一个shard server角色可由几台机器组成一个replica set承担，防止单点故障
```

##### Config Server

```
mongod实例中，存储了整个 ClusterMetadata（集群中的元数据），其中包括chunk信息
```

##### Routers

```
前端路由，客户端由此接入，且让整个集群看上去像单一的数据库，前端应用可以透明使用
```

##### Shard Key

```
数据的分区根据“shard key”，对于每个需要sharding的collection，都需要指定shard key；
shard key必须是索引字段或者为组合索引的左前缀；
mongo根据分片键将数据分为多个chunks，并将它们均匀分布在多个shards节点上。目前，mongodb支持两种分区算法：区间分区（Range）和哈希（Hash）分区
```



















































