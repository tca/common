package com.tca.common.learning.mongodb.test;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.tca.common.learning.mongodb.MongoDBApplication;
import com.tca.common.learning.mongodb.entity.Player;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

/**
 * @author an.zhou
 * @date 2023/4/17 16:20
 */
@SpringBootTest(classes = {MongoDBApplication.class})
@RunWith(SpringRunner.class)
@Slf4j
public class MongoDBApplicationTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * client:
     *  insert
     *  insertOne
     *  insertMany
     */
    @Test
    public void testInsert() {
        Player player = Player.builder()
                .name("Suarez")
                .club("FCB")
                .age(35).build();
        player = mongoTemplate.insert(player);
        log.info("插入成功, player = {}", player);
    }

    /**
     * 全量替换
     * 使用save方法
     * save方法用于保存数据，如果数据不包含主键id信息，则一定是新增
     * 如果包含主键信息，则匹配检查，相同则全量覆盖，否则新增
     */
    @Test
    public void testSave() {
        Player player = Player.builder()
                .id("643d13999149fa391171ffbf")
                .name("Luis Suarez")
                .club("FCB").build();
        player = mongoTemplate.save(player);
        log.info("更新完成, player = {}", player);
    }

    /**
     * 根据条件更新具体投影
     * updateFirst - 相当于 db.collectionName.update({}, {}, {"multi": false})
     * updateMulti - 相当于 db.collectionName.update({}, {}, {"multi": true})
     */
    @Test
    public void testUpdate() {
        Query query = Query.query(
                Criteria.where("club").is("FCB")
        );

        Update update = Update.update("club", "Barcelona");

        UpdateResult updateResult = mongoTemplate.updateMulti(query, update, Player.class);
//        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Player.class);
        log.info("更新完成, updateResult = {}", updateResult);
    }

    /**
     * 根据主键id删除
     * remove(Object pojo)
     */
    @Test
    public void testDeleteById() {
        DeleteResult deleteResult = mongoTemplate.remove(Player.builder().id("643d13999149fa391171ffbf").build());
        log.info("删除完成, deleteResult = {}", deleteResult);
    }

    /**
     * 删除全表
     * remove(Class clazz)
     */
    @Test
    public void testDeleteCollection() {

    }

    /**
     * 根据条件删除
     * remove(Query query)
     */
    @Test
    public void testDeleteByCondition() {
        Query query = new Query(
                Criteria.where("age").lt(20)
        );
        DeleteResult deleteResult = mongoTemplate.remove(query);
        log.info("删除成功: deleteResult = {}", deleteResult );
    }

    /**
     * 查询全部
     * findAll(Class clazz)
     */
    @Test
    public void testFindAll() {
        List<Player> playerList = mongoTemplate.findAll(Player.class);
        log.info("查询成功 result = {}", playerList);
    }

    /**
     * 根据主键id查询
     * findById(String id, Class clazz)
     */
    @Test
    public void testFindById() {
        Player player = mongoTemplate.findById("6437b62023001763e806ccba", Player.class);
        log.info("查询成功 result = {}", player);
    }

    /**
     * 根据条件查询一个
     */
    @Test
    public void testFindOne() {
        Player player = mongoTemplate.findOne(new Query(Criteria.where("club").is("Barcelona"))
                , Player.class);
        log.info("查询成功 player = {}", player);
    }

    /**
     * 条件查询
     * find(Query query, Class clazz)
     */
    @Test
    public void testFindByCondition() {
        List<Player> playerList = mongoTemplate.find(new Query(
                Criteria.where("club").is("Barcelona")
        ), Player.class);
        playerList.forEach(System.out::println);
    }

    @Test
    public void testFindByAndCondition() {
        // 等同于 db.player.find({"$and": [{"age": {"$gt": 34}}, {"club": "Barcelona"}]})
        List<Player> playerList = mongoTemplate.find(new Query(
                new Criteria().andOperator(
                        Criteria.where("age").gt(34),
                        Criteria.where("club").is("Barcelona")
                )
        ), Player.class);
        playerList.forEach(System.out::println);

        System.out.println("===================");

        playerList = mongoTemplate.find(new Query(
                new Criteria().orOperator(
                        Criteria.where("age").gt(34),
                        Criteria.where("club").is("Barcelona")
                )
        ), Player.class);
        playerList.forEach(System.out::println);
    }

    /**
     * 排序
     */
    @Test
    public void testSort() {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.ASC, "name"));
        List<Player> playerList = mongoTemplate.find(query, Player.class);
        playerList.forEach(System.out::println);

        System.out.println("=================================================");

        Query anotherQuery = new Query();
        // 这里先根据age升序，age相同，根据name升序
        anotherQuery.with(Sort.by(Sort.Order.asc("age"), Sort.Order.asc("name")));
        playerList = mongoTemplate.find(anotherQuery, Player.class);
        playerList.forEach(System.out::println);
    }

    /**
     * 分页
     */
    @Test
    public void testPage() {
        Query query = new Query();
        query.with(PageRequest.of(1, 2, Sort.by(Sort.Order.asc("age"), Sort.Order.asc("name"))));
        List<Player> playerList = mongoTemplate.find(query, Player.class);
        playerList.forEach(System.out::println);
    }

    /**
     * 分组聚合
     * aggregate(TypedAggregation agg, Class clazz)
     */
    @Test
    public void testAggregate() {
        TypedAggregation<Player> typedAggregation = TypedAggregation.newAggregation(Player.class
                , Aggregation.group("club").count().as("total"));
        AggregationResults<Map> result = mongoTemplate.aggregate(typedAggregation, Map.class);
        log.info("聚合查询 result = {}", result.getMappedResults());
    }
}
