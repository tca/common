package com.tca.common.learning.mongodb.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author an.zhou
 * @date 2023/4/17 17:06
 *
 * @Document注解  描述当前类型和MongoDB中的一个集合对应，可以省略
 *
 */
@Document(value = "player")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Player {

    /**
     * 主键字段
     * 可以使用注解@Id描述。代表此字段是主键字段。
     * 如果属性命名是：id 或者 _id，注解也可以省略。
     *
     * MongoDB中，默认的主键类型是ObjectId，对应java中的类型是String或ObjectId（是Bson包中的，一般不用）
     */
    @Id
    private String id;

    /**
     * name
     * 可以使用@Field描述属性，用于配置java实体属性和mongoDB集合Field字段的映射关系
     * 默认映射关系是同名映射
     */
    private String name;

    /**
     * club
     */
    private String club;

    /**
     * age
     */
    private Integer age;
}
