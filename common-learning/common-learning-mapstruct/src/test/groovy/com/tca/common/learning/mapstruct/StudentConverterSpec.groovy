package com.tca.common.learning.mapstruct

import com.tca.common.learning.mapstruct.convertor.StudentConvertor
import com.tca.common.learning.mapstruct.dto.AddressDTO
import com.tca.common.learning.mapstruct.dto.StudentDTO
import org.assertj.core.util.Lists
import spock.lang.Specification

/**
 * @author zhoua
 * @date 2025/1/22 22:05
 */
class StudentConverterSpec extends Specification{

    def testDto2vo() {
        given: "准备测试数据"
        def studentDTO = new StudentDTO(
                id: 10L,
                birthday: new Date(),
                name: "Messi",
                address: new AddressDTO(
                        province: "安徽",
                        city: "合肥",
                        street: "海恒街"
                )
        )

        when: "执行转换"
        def studentVO = StudentConvertor.convertor.dto2vo(studentDTO)

        then: "验证结果"
        studentVO != null
        studentVO.id == studentDTO.id
        studentVO.name == studentDTO.name
        studentVO.address.province == studentDTO.address.province
        studentVO.address.city == studentDTO.address.city
        studentVO.address.streetName == studentDTO.address.street
        noExceptionThrown()
    }

    def testDtoList2voList() {
        given: "准备测试数据"
        def studentDTO = new StudentDTO(
                id: 10L,
                birthday: new Date(),
                name: "Messi",
                address: new AddressDTO(
                        province: "安徽",
                        city: "合肥",
                        street: "海恒街"
                )
        )
        def dtoList = Lists.newArrayList(studentDTO)

        when:
        def studentVOList = StudentConvertor.convertor.dtoList2voList(dtoList)
        println(studentVOList)

        then:
        noExceptionThrown()
    }
}
