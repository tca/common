package com.tca.common.learning.mapstruct

import com.tca.common.learning.mapstruct.convertor.StudentSpringConvertor
import com.tca.common.learning.mapstruct.dto.AddressDTO
import com.tca.common.learning.mapstruct.dto.StudentDTO
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import javax.annotation.Resource
/**
 * @author zhoua
 * @date 2025/1/22 23:03
 */
@SpringBootTest(classes = Application.class)
class StudentSpringConvertorSpec extends Specification {

    @Resource
    StudentSpringConvertor studentSpringConvertor

    def setup() {
        assert studentSpringConvertor != null
    }

    def "should convert DTO to VO"() {
        given: "准备测试数据"
        def studentDTO = new StudentDTO(
            id: 10L,
            birthday: new Date(),
            name: "Messi",
            address: new AddressDTO(
                province: "安徽",
                city: "合肥",
                street: "海恒街"
            )
        )

        when: "执行转换"
        def studentVO = studentSpringConvertor.dto2vo(studentDTO)

        then: "验证结果"
        studentVO != null
        studentVO.id == studentDTO.id
        studentVO.name == studentDTO.name
        studentVO.address.province == studentDTO.address.province
        studentVO.address.city == studentDTO.address.city
        studentVO.address.streetName == studentDTO.address.street
        noExceptionThrown()
    }
}
