package com.tca.common.learning.mapstruct.vo;

import lombok.Data;

/**
 * @author zhoua
 * @date 2025/1/22 21:46
 */
@Data
public class StudentVO {

    private Long id;

    private String name;

    private String birthday;

    private AddressVO address;

    private String fullMsg;

}
