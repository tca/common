package com.tca.common.learning.mapstruct.convertor;

import com.tca.common.learning.mapstruct.dto.StudentDTO;
import com.tca.common.learning.mapstruct.vo.StudentVO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author zhoua
 * @date 2025/1/22 21:59
 */
@Mapper
public abstract class StudentConvertor {

    public static final StudentConvertor convertor = Mappers.getMapper(StudentConvertor.class);

    /**
     * @Mapping注解的使用
     * @param dto
     * @return
     */
    @Mapping(source = "birthday", target = "birthday", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "address.street", target = "address.streetName")
    public abstract StudentVO dto2vo(StudentDTO dto);

    /**
     * list转换
     * 注意: 这里就不需要再写@Mapping了
     * @param studentDTOList
     * @return
     */
    public abstract List<StudentVO>  dtoList2voList(List<StudentDTO> studentDTOList);

    @AfterMapping
    public void fullMsgUpdate(StudentDTO dto, @MappingTarget StudentVO vo) {
        vo.setFullMsg(vo.getName() + vo.getId());
    }

}
