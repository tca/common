package com.tca.common.learning.mapstruct.convertor;

import com.tca.common.learning.mapstruct.dto.StudentDTO;
import com.tca.common.learning.mapstruct.vo.StudentVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

/**
 * @author zhoua
 * @date 2025/1/22 23:02
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface StudentSpringConvertor {

    /**
     * @Mapping注解的使用
     * @param dto
     * @return
     */
    @Mapping(source = "birthday", target = "birthday", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "address.street", target = "address.streetName")
    StudentVO dto2vo(StudentDTO dto);
    
}
