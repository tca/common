package com.tca.common.learning.mapstruct.dto;

import lombok.Data;

/**
 * @author zhoua
 * @date 2025/1/22 21:39
 */
@Data
public class AddressDTO {

    private String province;

    private String city;

    private String street;
}
