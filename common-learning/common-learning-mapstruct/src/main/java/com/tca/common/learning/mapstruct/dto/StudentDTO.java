package com.tca.common.learning.mapstruct.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author zhoua
 * @date 2025/1/22 21:42
 */
@Data
public class StudentDTO {

    private Long id;

    private String name;

    private Date birthday;

    private AddressDTO address;

}
