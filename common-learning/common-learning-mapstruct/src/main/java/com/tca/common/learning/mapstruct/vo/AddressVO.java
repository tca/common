package com.tca.common.learning.mapstruct.vo;

import lombok.Data;

/**
 * @author zhoua
 * @date 2025/1/22 21:45
 */
@Data
public class AddressVO {

    private String province;

    private String city;

    private String streetName;
}
