# quick start
step1 引入sdk
```xml
<dependency>
    <groupId>org.mapstruct</groupId>
    <artifactId>mapstruct</artifactId>
    <version>${mapstruct.version}</version>
</dependency>
```

step2 定义抽象类convertor, 使用@Mapper注解
```java
@Mapper
public abstract class StudentConvertor {

    public static final StudentConvertor convertor = Mappers.getMapper(StudentConvertor.class);

    /**
     * @Mapping注解的使用
     * @param dto
     * @return
     */
    @Mapping(source = "birthday", target = "birthday", dateFormat = "yyyy-MM-dd") // 将birthday属性进行类型转换
    @Mapping(source = "address.street", target = "address.streetName") // 将address.street属性copy至address.streetName属性中
    public abstract StudentVO dto2vo(StudentDTO dto);

    @AfterMapping // 在映射之后的操作
    public void fullMsgUpdate(StudentDTO dto, @MappingTarget StudentVO vo) {
        vo.setFullMsg(vo.getName() + vo.getId());
    }

}
```

step3 使用
```groovy
def testDto2vo() {
    given:
        def studentDTO = new StudentDTO()
        studentDTO.setId(10L)
        studentDTO.setBirthday(new Date())
        studentDTO.setName("Messi")

        def addressDTO = new AddressDTO()
        addressDTO.setProvince("安徽")
        addressDTO.setCity("合肥")
        addressDTO.setStreet("海恒街")
        studentDTO.setAddress(addressDTO)

    when: 
        def studentVO = StudentConvertor.convertor.dto2vo(studentDTO)
        println(studentVO)

    then:
        notThrown(Exception.class)
}
```

# 与spring集成
step1 引入sdk
```xml
<dependency>
    <groupId>org.mapstruct</groupId>
    <artifactId>mapstruct</artifactId>
    <version>${mapstruct.version}</version>
</dependency>
```

step2 定义convertor接口, 使用@Mapper注解, 注意, mapstruct类似于lombok, 也是基于jsr实现的, 在编译时生成具体实现的代码
```java
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface StudentSpringConvertor {

    /**
     * @Mapping注解的使用
     * @param dto
     * @return
     */
    @Mapping(source = "birthday", target = "birthday", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "address.street", target = "address.streetName")
    StudentVO dto2vo(StudentDTO dto);
    
}
```

step3 实现
```groovy

```
