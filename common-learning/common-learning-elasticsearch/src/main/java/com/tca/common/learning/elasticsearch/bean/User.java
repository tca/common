package com.tca.common.learning.elasticsearch.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhoua
 * @date 2022/1/11 22:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    /**
     * name
     */
    private String name;

    /**
     * sex
     */
    private String sex;

    /**
     * age
     */
    private Integer age;

}
