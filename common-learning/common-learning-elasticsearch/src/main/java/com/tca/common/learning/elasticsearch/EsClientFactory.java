package com.tca.common.learning.elasticsearch;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.util.Objects;

/**
 * @author zhoua
 * @date 2022/1/11 22:34
 */

public class EsClientFactory {

    private static String ip = "192.168.1.105";

    private static Integer port = 9200;

    private static String schema = "http";

    private static volatile RestHighLevelClient client;

    private EsClientFactory(){}

    public static RestHighLevelClient getClient() {
        if (Objects.isNull(client)) {
            synchronized (EsClientFactory.class) {
                if (Objects.isNull(client)) {
                    client = new RestHighLevelClient(
                            RestClient.builder(new HttpHost(ip, port, schema)));
                }
                return client;
            }
        }
        return client;
    }
}
