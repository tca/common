package com.tca.common.learning.maven.pkg.dependency;

import com.tca.common.learning.maven.pkg.PropertiesReader;

import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

public class Start {

    public static void main(String[] args) throws Exception{
        Properties properties = PropertiesReader.readProperties("location-config.properties");
        System.out.println(properties.getProperty("location"));

        Properties propertiesOuter = PropertiesReader.readProperties("location-config-outer.properties");
        System.out.println(propertiesOuter.getProperty("location"));

        Enumeration<URL> resources = PropertiesReader.class.getClassLoader().getResources("location-config.properties");
        while (resources.hasMoreElements()) {
            URL url = resources.nextElement();
            System.out.println(url);
        }
    }
}
