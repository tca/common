package com.tca.common.learning.maven.pkg;

import com.tca.common.core.utils.ValidateUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Properties;

/**
 * @author zhoua
 * @Date 2021/12/20
 */
public class PropertiesReader {

    public static void main(String[] args) throws IOException {
        Properties properties = readProperties("location-config.properties");
        String location = properties.getProperty("location");
        System.out.println(location);
    }

    public static Properties readProperties(String propertiesPath) throws IOException {
        Properties properties = new Properties();
        if (ValidateUtils.isEmpty(propertiesPath) || !propertiesPath.endsWith(".properties")) {
            return properties;
        }
        properties.load(PropertiesReader.class.getClassLoader().getResourceAsStream(propertiesPath));
        return properties;
    }

}
