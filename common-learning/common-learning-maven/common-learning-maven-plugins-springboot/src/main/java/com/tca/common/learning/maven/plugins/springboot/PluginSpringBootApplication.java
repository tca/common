package com.tca.common.learning.maven.plugins.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhoua
 * @date 2022/1/6 21:56
 */
@SpringBootApplication
public class PluginSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(PluginSpringBootApplication.class);
    }

}
