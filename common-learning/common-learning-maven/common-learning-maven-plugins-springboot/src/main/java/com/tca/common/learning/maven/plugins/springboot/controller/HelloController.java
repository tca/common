package com.tca.common.learning.maven.plugins.springboot.controller;

import com.tca.common.core.bean.Result;
import com.tca.common.learning.maven.pkg.PropertiesReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author zhoua
 * @date 2022/1/6 21:57
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

    @Value("${location:}")
    private String location;

    @GetMapping("/application")
    public Result<String> hello() {
        return Result.success("hello, plugin-spring-boot, my location is " + location);
    }

    @GetMapping("/location")
    public Result<String> location() throws IOException {
        try {
            PropertiesReader.main(null);
        } catch (IOException e) {
        }
        System.out.println(this.getClass().getClassLoader());
        return Result.success(PropertiesReader.readProperties("location-config.properties").getProperty("location"));
    }
}
