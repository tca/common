## 1.common-learning-maven-package
### 1.1 pom.xml
```xml
 <dependencies>
    <dependency>
        <groupId>tca</groupId>
        <artifactId>common-core</artifactId>
        <version>1.0.0</version>
    </dependency>
</dependencies>
```
当前项目依赖 common-utils.jar, 没有依赖其他maven插件, 即打包时使用maven默认方式打包, 
执行 mvn clean package, 输出在 target 下

### 1.2 target结构
```
target
    -- classes // 存放了当前项目的classpath下普通的java文件编译后的class文件, 以及classpath下面的配置文件
    -- generated-sources // 暂略
    -- maven-archiver // 生成了 pom.properties, 其中主要是当前项目的 maven 配置信息
    -- maven-status // 当前项目使用的maven插件
    -- common-learning-maven-package-1.0.0.jar // 打成的jar, 默认情况下, jar中不包含依赖的第三方jar
```

### 1.3 common-learning-maven-package-1.0.0.jar
```
com // 项目中的class文件, 即classpath下的java编译后的文件(不包含配置文件)  
META-INF
    -- maven // 存放了pom.xml配置文件和pom.properties(当前项目相关的groupId, artifactId, version)
    -- MANIFEST.MF // jar包中的核心配置文件
location-config.properties // 项目中resources下的配置文件
```
项目结构与非maven项目的结构大体相同, 主要包含了当前classpath下的所有class文件和配置文件, 以及META-INF等, 不同的地方在于:
没有将第三方依赖的jar的class文件和配置文件放到jar的主目录下, 而是在META-INF下添加了maven文件夹, maven文件夹中包含了当前maven
项目的pom.xml配置文件等！

### 1.4 META-INF/MANIFEST.MF
```
Manifest-Version: 1.0
Archiver-Version: Plexus Archiver
Built-By: DELL
Created-By: Apache Maven 3.8.3
Build-Jdk: 1.8.0_171
```
注意, 当前核心配置文件中没有Main-Class配置, 所以打成的jar不可以通过java -jar执行, 否则会报错:
common-learning-maven-package-1.0.0.jar中没有主清单属性

### 1.5 本地仓库

```
apache-maven-repository (本地仓库)
	-- tca (groupId)
		-- common-learning-maven-package (artifactId)
			-- 1.0.0 (version)
				-- _remote.repository
				-- common-learning-maven-package-1.0.0.jar (打成的jar)
				-- common-learning-maven-package-1.0.0.pom (项目的pom.xml文件)
			-- maven-metadata-local.xml (当前maven的元数据)
```

可以看到maven仓库中打成的jar和target中的jar是一样的，都不包含第三方类库，第三方类库体现在pom.xml配置文件中

### 总结

```
使用maven默认方式打包时:
1.打成的jar中没有包含第三方jar, 这个与非maven项目打包的结果完全不同
2.含有pom.xml主配置文件
3.打成的jar中的核心配置文件(META-INF/MANIFEST.MF)中没有Main-Class属性, 所以打成的jar是不可执行文件
```



## 2.common-learning-maven-plugins

常用的插件学习

pom.xml

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-core</artifactId>
    </dependency>
</dependencies>
```



通过@1.common-learning-maven-package的学习，可以看到默认情况下，maven打包后的结果，而当前项目则可以看到使用一些maven插件打包后的结果

### 2.1 maven-compiler-plugin

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>  
    <artifactId>maven-compiler-plugin</artifactId> 
    <version>3.1</version> 
    <configuration>
        <source>1.8</source> <!-- 源代码使用的JDK版本 -->                                           
        <target>1.8</target> <!-- 需要生成的目标class文件的编译版本 -->                               
        <encoding>UTF-8</encoding> <!-- 字符集编码 -->               
    </configuration>           
</plugin>
```

该插件只是配置了当前项目使用的jdk版本，编译版本，字符集等，打包后的结构目录上没有变化

### 2.2 maven-jar-plugin

```xml
<!-- 打包jar文件时，配置manifest文件，加入lib包的jar依赖 -->
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-jar-plugin</artifactId>
    <version>3.1.0</version>
    <configuration>
        <classesDirectory>target/classes/</classesDirectory>
        <archive>
            <manifest>
                <!-- 启动类 -->
                <mainClass>com.tca.common.learning.maven.plugins.PluginStart</mainClass>
                <!-- 打包时 MANIFEST.MF文件不记录的时间戳版本 -->
                <useUniqueVersions>false</useUniqueVersions>
                <!-- 是否添加依赖的jar路径配置 -->
                <!-- 为依赖包添加路径, 这些路径会写在MANIFEST文件的Class-Path下 -->
                <addClasspath>true</addClasspath>
                <!-- 依赖的jar包存放路径, 和生成的jar放在同一级目录下的lib -->
                <!-- 这个jar所依赖的jar包添加classPath的时候的前缀，如果这个jar本身
				和依赖包在同一级目录，则不需要添加 -->
                <classpathPrefix>lib</classpathPrefix>
            </manifest>
            <manifestEntries>
                <Class-Path>.</Class-Path>
            </manifestEntries>
        </archive>
    </configuration>
</plugin>
```

#### 说明

查看生成的jar中的META-INF/MANIFEST.MF文件

```properties
Manifest-Version: 1.0
Built-By: DELL
# Class-path很重要, 其可以作为类加载器的搜索路径, 即除了当前jar为类加载路径外, Class-path中定义的路径也是类加载路径
Class-Path: lib/spring-core-5.1.10.RELEASE.jar lib/spring-jcl-5.1.10.RELEASE.jar .
Created-By: Apache Maven 3.8.3
Build-Jdk: 1.8.0_171
Main-Class: com.tca.common.learning.maven.plugins.PluginStart
```

可以看到，在文件中添加了两个重要属性：

1.Class-Path：表示classpath路径，即App类加载器可以加载的路径，可以看到路径中包含了 "."，表示当前路径，其次还包含了 lib/*.jar，即当前路径下的lib下的所有jar

2.Main-Class：为打成jar之后的启动类

#### 问题

1.我们在项目启动类PluginStart中编写代码（如下），打成包之后运行：java -jar common-learning-maven-plugins-1.0.0.jar

```java
package com.tca.common.learning.maven.plugins;

import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

public class PluginStart {

    public static void main(String[] args) throws IOException {
        System.out.println("plugin start");

        // 打印类加载器
        ClassLoader classLoader = PluginStart.class.getClassLoader();
        System.out.println("当前类加载器为: " + classLoader);

        // 打印classpath
        Enumeration<URL> resources = classLoader.getResources("");
        while (resources.hasMoreElements()) {
            URL url = resources.nextElement();
            System.out.println(url);
        }

        // 打印依赖的第三方jar
        ClassLoader thirdJarClassLoader = StringUtils.class.getClassLoader();
        System.out.println("spring-core.jar的类加载器为: " + thirdJarClassLoader);
    }
}
```

结果运行报错：找不到第三方jar中的类org/springframework/util/StringUtils

```
file:/E:/idea_work_space/common/common-learning/common-learning-maven/common-learning-maven-plugins/target/
Exception in thread "main" java.lang.NoClassDefFoundError: org/springframework/util/StringUtils
        at com.tca.common.learning.maven.plugins.PluginStart.main(PluginStart.java:26)
Caused by: java.lang.ClassNotFoundException: org.springframework.util.StringUtils
        at java.base/jdk.internal.loader.BuiltinClassLoader.loadClass(BuiltinClassLoader.java:581)
        at java.base/jdk.internal.loader.ClassLoaders$AppClassLoader.loadClass(ClassLoaders.java:178)      
        at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:521)
        ... 1 more
```

分析

```
使用maven打成的jar中只包含当前项目的class和resources, 虽然使用了maven-jar-plugin插件, 指定了Main-Class和Class-Path, Main-Class指定了当前jar的入口方法, Class-Path制定了classpath路径, 但是看到classpath中的第三方jar都在lib中, 但是我们没有单独用lib, 所以从classpath中读取不到spring-core.jar
```

解决方案

```
在common-learning-maven-plugins-1.0.0.jar的同层路径下添加lib目录, 并将spring-core-5.1.10.RELEASE.jar放入其中, 目录结构为:
target
	-- common-learning-maven-plugins-1.0.0.jar
		-- META-INF
			-- MANIFEST.MF
	-- lib
		-- spring-core-5.1.10.RELEASE.jar
```

运行，java -jar common-learning-maven-plugins-1.0.0.jar，执行成功！即根据 maven-jar-plugin 插件的配置，将lib单独放在与common-learning-maven-plugins-1.0.0.jar的同层路径下即可，maven-dependency-plugin 插件也有相同的功能，不需要手动拷贝依赖的第三方jar，可以直接将依赖的第三方jar输出到当前jar的同级目录下

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-dependency-plugin</artifactId>
    <executions>
        <execution>
            <id>copy-dependencies</id>
            <phase>package</phase>
            <goals>
                <goal>copy-dependencies</goal>
            </goals>
            <configuration>
                <type>jar</type>
                <includeTypes>jar</includeTypes>
                <outputDirectory>${project.build.directory}/lib</outputDirectory>
            </configuration>
        </execution>
    </executions>
</plugin>
```

#### 总结

```
使用 maven-jar-plugin 插件, 核心是指定了启动类, 同时指定了依赖的第三方jar的存放位置并将其作为classpath, 但是并没有将第三方jar也打进去。但是同时, 修改了jar的核心文件 -- META-INF/MANIFEST.MF, 修改了该文件中的 Main-Class 以及 Class-Path, 其中 Main-Class 指向了启动类, Class-Path 指定了类加载路径, 即classpath, 即App类加载器加载的路径。另外, 通过运行结果可以看到, 使用的类加载器确实为App类加载器,  jdk.internal.loader.ClassLoaders$AppClassLoader@6d5380c2, 这里跟springboot打包后的结果不一样 (见springboot源码分析中)
```

### 2.3 maven-source-plugin

```xml
<plugin>  
    <groupId>org.apache.maven.plugins</groupId>  
    <artifactId>maven-source-plugin</artifactId>  
    <version>3.1.0</version>  
    <configuration>  
        <attach>true</attach>  
    </configuration>  
    <executions>  
        <execution>  
            <phase>compile</phase>  
            <goals>  
                <goal>jar</goal>  
            </goals>  
        </execution>  
    </executions>  
</plugin>  
```

#### 说明

```
可以对源码进行打包, 效果为: install后会将源码的jar放到target目录下, 同时也会放到本地maven仓库中
```

### 2.4 maven-resource-plugin

使用  maven-resource-plugin，一般需要配置两部分：

1.plugin

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-resources-plugin</artifactId>
    <version>3.1.0</version>
    <configuration>
        <!-- 在构建路径中 指定源文件编译的字符编码 -->
        <encoding>UTF-8</encoding>
    </configuration>
</plugin>
```

如果只配置了 plugin，效果会在以下说明中解释

2.resource

```xml
<resource>
		<!-- 从此目录下读取全部以.properties和.xml结尾的文件-->
        <directory>src/main/resources/</directory>
        <includes>
          <include>**/*.properties</include>
        </includes>
</resource>
<resource>
     <!-- 从此目录下读取全部以.md结尾的文件-->
     <directory>C:/Users/DELL/Desktop</directory>
     <includes>
         <include>**/*.md</include>
     </includes>
</resource>
```

resource标签用于配置将指定的资源（如文件等）打包到jar中

#### 说明

```
1.如果只配置了plugin部分, 则跟不配置几乎没区别, 都是默认会将src/main/java下的class和src/main/resources下的配置文件打包到jar中。即, 如果在当前src/main/resources下添加 location-config.properties 以及 mapper/demo-mapper.xml文件, 打包后, 都会在target目录的 classes 文件夹中, 同时也在
common-learning-maven-plugins-1.0.0.jar 中
2.当我们搭配<resource>标签使用时, 则将指定的资源打包进来, 如我们按照上述的配置, 则打包后的target目录如下:

target
	-- classes
		-- com (当前项目中的所有class)
		-- location-config.properties (注意: 因为我们配置的是<include>**/*.properties</include>, 所以同样是src/main/resources下的mapper目录及*-mapper.xml文件都没有打包)
		-- *** (桌面上所有的 md 文件)
	-- generated-sources
	-- maven-archiver
	-- maven-status
	-- common-learning-maven-plugins-1.0.0.jar
	
3.同时jar包中也包含了我们配置的resource！！！
```

### 2.5 maven-assembly-plugin

使用 maven-assembly-plugin，一般分为两部分：

1.plugin

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-assembly-plugin</artifactId>
            <version>3.1.1</version>
            <executions>
                <execution>
                    <id>make-assembly</id>
                    <!-- 绑定到package生命周期 -->
                    <phase>package</phase>
                    <goals>
                        <!-- 只运行一次 -->
                        <goal>single</goal>
                    </goals>
                </execution>
            </executions>
            <configuration>
                <descriptors>
                    <!-- 配置描述符文件 -->
                    <descriptor>src/main/assembly/assembly.xml</descriptor>
                </descriptors>
                <!-- 也可以使用Maven预配置的描述符
                <descriptorRefs>
                    <descriptorRef>jar-with-dependencies</descriptorRef>
                </descriptorRefs> -->
            </configuration>
        </plugin>
    </plugins>
</build>
```

2.assembly.xml

```xml
<assembly>
    <!-- id会添加到打包文件名的标识符，用来做后缀 -->
    <id>assembly</id>
	
    <!-- formats是assembly插件支持的打包文件格式, 有zip、tar、tar.gz、tar.bz2、jar、war。可以同时定义多个format -->
    <!-- 按照这里id和formats的配置, 打包出来为 artifactId−{version}-assembly.tar.gz -->
    <formats>
        <format>tar.gz</format>
    </formats>
	
    <includeBaseDirectory>true</includeBaseDirectory>

    <!-- 用来设置一组文件在打包时的属性 -->
    <fileSets>
        <fileSet>
            <!-- 源目录的路径 -->
            <directory>src/main/bin</directory>
            <!-- includes/excludes: 设定包含或排除哪些文件，支持通配符 -->
            <includes>
                <include>*.sh</include>
            </includes>
            <!-- 输出的目录路径 -->
            <outputDirectory>bin</outputDirectory>
            <!-- 指定该目录下的文件属性, 默认为0755 -->
            <fileMode>0755</fileMode>
        </fileSet>
        <fileSet>
            <directory>src/main/conf</directory>
            <outputDirectory>conf</outputDirectory>
        </fileSet>
        <fileSet>
            <directory>src/main/sql</directory>
            <includes>
                <include>*.sql</include>
            </includes>
            <outputDirectory>sql</outputDirectory>
        </fileSet>
        <fileSet>
            <directory>target/classes/</directory>
            <includes>
                <include>*.properties</include>
                <include>*.xml</include>
                <include>*.txt</include>
            </includes>
            <outputDirectory>conf</outputDirectory>
        </fileSet>
    </fileSets>

    <!-- 与fileSets大致相同, 不过是指定单个文件, 并且还可以通过destName属性来设置与源文件不同的名称 -->
    <files>
        <file>
            <source>target/${project.artifactId}-${project.version}.jar</source>
            <outputDirectory>.</outputDirectory>
        </file>
    </files>

    <!-- 用来设置工程依赖文件在打包时的属性 -->
    <dependencySets>
        <dependencySet>
            <!-- 布尔值, false表示将依赖以原来的JAR形式打包, true则表示将依赖解成*.class文件的目录结构打包 -->
            <unpack>false</unpack>
            <!-- 表示符合哪个作用范围的依赖会被打包进去。compile与provided都不用管, 一般是写runtime -->
            <scope>runtime</scope>
            <outputDirectory>lib</outputDirectory>
        </dependencySet>
    </dependencySets>
</assembly>
```

#### 说明

```
maven-assembly-plugin 可以自定义打包后的输出文件, 可以将shell脚本、sql脚本等都一起打包, 功能比较强大
```



### 2.6 maven-checkstyle-plugin

代码规范的插件，它有一个核心目标：check，通常我们将它绑定到maven某个生命周期的某个阶段，比如绑定到 default生命周期的package阶段，它也包含两部分

1.plugin

```xml
<plugin>
  <artifactId>maven-checkstyle-plugin</artifactId>
  <version>3.0.0</version>
    <executions>
        <execution>
            <!-- 绑定pmd:pmd到validate生命周期，在validate时会自动进行代码规范检查 -->
            <id>package</id>
            <phase>package</phase>
            <configuration>
                <!-- 配置文件的路径，在style文件夹下 -->
                <configLocation>codestyle/checkstyle.xml</configLocation>
                <!-- 编码 -->
                <encoding>UTF-8</encoding>
                <!-- 是否输出到target目录下 -->
                <consoleOutput>true</consoleOutput>
                <!-- 发生error时终止, 需要配合checkstyle.xml文件中的severity属性配合使用 -->
                <failsOnError>true</failsOnError>
                <includeTestSourceDirectory>false</includeTestSourceDirectory>
            </configuration>
            <goals>
                <goal>check</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

2.checkstyle.xml

这里是参考网上华为的规范

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE module PUBLIC "-//Puppy Crawl//DTD Check Configuration 1.2//EN" "http://www.puppycrawl.com/dtds/configuration_1_2.dtd">

<module name="Checker"> 
	
    <!-- 字符编码 -->
    <property name="charset" value="UTF-8"/>
    <!-- 级别 -->
    <property name="severity" value="warning"/>

    <!-- Checks for Size Violations.  --> 
    <!-- 检查文件的长度(行) default max=2000 --> 
    <module name="FileLength">         
        <property name="max" value="2500"/>        
    </module>  


    <module name="TreeWalker">
        <!-- Checks for imports    -->               
        <!-- 必须导入类的完整路径，即不能使用*导入所需的类 -->  
        <module name="AvoidStarImport"/>  

        <!-- 检查是否从非法的包中导入了类 illegalPkgs: 定义非法的包名称-->  
        <module name="IllegalImport"/> <!-- defaults to sun.* packages -->  

        <!-- 检查是否导入了不必显示导入的类-->  
        <module name="RedundantImport"/>  

        <!-- 检查是否导入的包没有使用-->  
        <module name="UnusedImports"/>

        <!-- 检查类和接口的javadoc 默认不检查author 和version tags       
      authorFormat: 检查author标签的格式
            versionFormat: 检查version标签的格式
            scope: 可以检查的类的范围，例如：public只能检查public修饰的类，private可以检查所有的类
            excludeScope: 不能检查的类的范围，例如：public，public的类将不被检查，但访问权限小于public的类仍然会检查，其他的权限以此类推
            tokens: 该属性适用的类型，例如：CLASS_DEF,INTERFACE_DEF -->
        <module name="JavadocType">  
            <property name="authorFormat" value="\S"/>  
            <property name="scope" value="protected"/>        
            <property name="tokens" value="CLASS_DEF,INTERFACE_DEF"/>  
        </module>

        <!-- 检查方法的javadoc的注释
            scope: 可以检查的方法的范围，例如：public只能检查public修饰的方法，private可以检查所有的方法
            allowMissingParamTags: 是否忽略对参数注释的检查
            allowMissingThrowsTags: 是否忽略对throws注释的检查
            allowMissingReturnTag: 是否忽略对return注释的检查 -->
        <module name="JavadocMethod">  
            <property name="scope" value="private"/>  
            <property name="allowMissingParamTags" value="false"/>  
            <property name="allowMissingThrowsTags" value="false"/>  
            <property name="allowMissingReturnTag" value="false"/>  
            <property name="tokens" value="METHOD_DEF"/>  
            <property name="allowUndeclaredRTE" value="true"/>  
            <property name="allowThrowsTagsForSubclasses" value="true"/>  
            <!--允许get set 方法没有注释-->
            <property name="allowMissingPropertyJavadoc" value="true"/>
        </module>  

        <!-- 检查类变量的注释
            scope: 检查变量的范围，例如：public只能检查public修饰的变量，private可以检查所有的变量 -->    
        <module name="JavadocVariable">  
            <property name="scope" value="private"/>  
        </module>  

        <!--option: 定义左大括号'{'显示位置，eol在同一行显示，nl在下一行显示  
      maxLineLength: 大括号'{'所在行行最多容纳的字符数  
      tokens: 该属性适用的类型，例：CLASS_DEF,INTERFACE_DEF,METHOD_DEF,CTOR_DEF -->  
        <module name="LeftCurly"> 
            <property name="option" value="eol"/>
        </module>

        <!-- NeedBraces 检查是否应该使用括号的地方没有加括号 tokens: 定义检查的类型 -->  
     
        <module name="NeedBraces"/>  

        <!-- Checks the placement of right curly braces ('}') for  else, try, and catch tokens. The policy to verify is specified using property  option.   
      option: 右大括号是否单独一行显示  
      tokens: 定义检查的类型  -->  
        <module name="RightCurly">    
            <property name="option" value="alone"/>     
        </module>

        <!-- 检查在重写了equals方法后是否重写了hashCode方法 --> 
        <module name="EqualsHashCode"/>

        <!--  Checks for illegal instantiations where a factory method is preferred.  
      Rationale: Depending on the project, for some classes it might be preferable to create instances through factory methods rather than calling the constructor.  
      A simple example is the java.lang.Boolean class. In order to save memory and CPU cycles, it is preferable to use the predefined constants TRUE and FALSE. Constructor invocations should be replaced by calls to Boolean.valueOf().  
      Some extremely performance sensitive projects may require the use of factory methods for other classes as well, to enforce the usage of number caches or object pools. -->  
        <module name="IllegalInstantiation">  
            <property name="classes" value="java.lang.Boolean"/>  
        </module>

        <!-- Checks for Naming Conventions.   命名规范   -->
        <!-- local, final variables, including catch parameters -->
        <module name="LocalFinalVariableName"/>

        <!-- local, non-final variables, including catch parameters--> 
        <module name="LocalVariableName"/>

        <!-- static, non-final fields -->
        <module name="StaticVariableName">
            <property name="format" value="(^[A-Z0-9_]{0,19}$)"/>    
        </module>  

        <!-- packages -->
        <module name="PackageName" >
            <property name="format" value="^[a-z]+(\.[a-z][a-z0-9]*)*$"/>
        </module> 

        <!-- classes and interfaces -->
        <module name="TypeName">  
            <property name="format" value="(^[A-Z][a-zA-Z0-9]{0,19}$)"/>    
        </module>

        <!-- methods -->  
        <module name="MethodName">          
            <property name="format" value="(^[a-z][a-zA-Z0-9]{0,19}$)"/>         
        </module> 

        <!-- non-static fields -->
        <module name="MemberName">  
            <property name="format" value="(^[a-z][a-z0-9][a-zA-Z0-9]{0,19}$)"/>         
        </module>

        <!-- parameters -->
        <module name="ParameterName">
            <property name="format" value="(^[a-z][a-zA-Z0-9_]{0,19}$)"/>         
        </module>

        <!-- constants (static,  final fields) -->
        <module name="ConstantName"> 
            <property name="format" value="(^[A-Z0-9_]{0,19}$)"/>      
        </module>

        <!-- 代码缩进   -->
        <module name="Indentation">        
        </module>

        <!-- Checks for redundant exceptions declared in throws clause such as duplicates, unchecked exceptions or subclasses of another declared exception. 
      检查是否抛出了多余的异常  
    <module name="RedundantThrows">
        <property name="logLoadErrors" value="true"/>
        <property name="suppressLoadErrors" value="true"/> 
    </module>
    --> 

        <!--  Checks for overly complicated boolean expressions. Currently finds code like  if (b == true), b || true, !false, etc.   
       检查boolean值是否冗余的地方  
       Rationale: Complex boolean logic makes code hard to understand and maintain. -->  
        <module name="SimplifyBooleanExpression"/>

        <!--  Checks for overly complicated boolean return statements. For example the following code  
       检查是否存在过度复杂的boolean返回值  
       if (valid())  
          return false;  
       else  
          return true;  
       could be written as  
          return !valid();  
       The Idea for this Check has been shamelessly stolen from the equivalent PMD rule. -->  
        <module name="SimplifyBooleanReturn"/>  

        <!-- Checks that a class which has only private constructors is declared as final.只有私有构造器的类必须声明为final-->  
        <module name="FinalClass"/>

        <!--  Make sure that utility classes (classes that contain only static methods or fields in their API) do not have a public constructor.  
       确保Utils类（只提供static方法和属性的类）没有public构造器。  
       Rationale: Instantiating utility classes does not make sense. Hence the constructors should either be private or (if you want to allow subclassing) protected. A common mistake is forgetting to hide the default constructor.  
       If you make the constructor protected you may want to consider the following constructor implementation technique to disallow instantiating subclasses:  
       public class StringUtils // not final to allow subclassing  
       {  
           protected StringUtils() {  
               throw new UnsupportedOperationException(); // prevents calls from subclass  
           }  
           public static int count(char c, String s) {  
               // ...  
           }  
       } 
    <module name="HideUtilityClassConstructor"/> 
    --> 

        <!--  Checks visibility of class members. Only static final members may be public; other class members must be private unless property protectedAllowed or packageAllowed is set.  
      检查class成员属性可见性。只有static final 修饰的成员是可以public的。其他的成员属性必需是private的，除非属性protectedAllowed或者packageAllowed设置了true.  
       Public members are not flagged if the name matches the public member regular expression (contains "^serialVersionUID$" by default). Note: Checkstyle 2 used to include "^f[A-Z][a-zA-Z0-9]*$" in the default pattern to allow CMP for EJB 1.1 with the default settings. With EJB 2.0 it is not longer necessary to have public access for persistent fields, hence the default has been changed.  
       Rationale: Enforce encapsulation. 强制封装 -->  
        <module name="VisibilityModifier"/> 

        <!-- 每一行只能定义一个变量 -->
        <module name="MultipleVariableDeclarations">       
        </module>

        <!-- Checks the style of array type definitions. Some like Java-style: public static void main(String[] args) and some like C-style: public static void main(String args[])   
       检查再定义数组时，采用java风格还是c风格，例如：int[] num是java风格，int num[]是c风格。默认是java风格-->  
        <module name="ArrayTypeStyle"> 
        </module>

        <!-- Checks that there are no "magic numbers", where a magic number is a numeric literal that is not defined as a constant. By default, -1, 0, 1, and 2 are not considered to be magic numbers. 
    <module name="MagicNumber">   
    </module>
    -->  

        <!-- A check for TODO: comments. Actually it is a generic regular expression matcher on Java comments. To check for other patterns in Java comments, set property format.   
       检查是否存在TODO（待处理） TODO是javaIDE自动生成的。一般代码写完后要去掉。  
     -->  
        <module name="TodoComment"/>  

        <!--  Checks that long constants are defined with an upper ell. That is ' L' and not 'l'. This is in accordance to the Java Language Specification,  Section 3.10.1.  
      检查是否在long类型是否定义了大写的L.字母小写l和数字1（一）很相似。  
      looks a lot like 1. -->  
        <module name="UpperEll"/>

        <!--  Checks that switch statement has "default" clause. 检查switch语句是否有‘default’从句  
       Rationale: It's usually a good idea to introduce a default case in every switch statement. 
       Even if the developer is sure that all currently possible cases are covered, this should be expressed in the default branch,
        e.g. by using an assertion. This way the code is protected aginst later changes, e.g. introduction of new types in an enumeration type. --> 
        <module name="MissingSwitchDefault"/> 

        <!--检查switch中case后是否加入了跳出语句，例如：return、break、throw、continue -->
        <module name="FallThrough"/>  

        <!-- Checks the number of parameters of a method or constructor. max default 7个. -->    
        <module name="ParameterNumber">      
            <property name="max" value="5"/>              
        </module>

        <!-- 每行字符数 -->    
        <module name="LineLength">  
            <property name="max" value="200"/>       
        </module>  

        <!-- Checks for long methods and constructors. max default 150行. max=300 设置长度300 --> 
        <module name="MethodLength">  
            <property name="max" value="300"/>                 
        </module>        

        <!-- ModifierOrder 检查修饰符的顺序，默认是 public,protected,private,abstract,static,final,transient,volatile,synchronized,native -->  
        <module name="ModifierOrder">          
        </module>      

        <!-- 检查是否有多余的修饰符，例如：接口中的方法不必使用public、abstract修饰  -->
        <module name="RedundantModifier">       
        </module>

        <!--- 字符串比较必须使用 equals() -->   
        <module name="StringLiteralEquality">          
        </module> 

        <!-- if-else嵌套语句个数 最多4层 -->
        <module name="NestedIfDepth">        
            <property name="max" value="3"/>         
        </module>  

        <!-- try-catch 嵌套语句个数 最多2层 -->
        <module name="NestedTryDepth">  
            <property name="max" value="2"/>         
        </module>  

        <!-- 返回个数 -->   
        <module name="ReturnCount">        
            <property name="max" value="5"/>  
            <property name="format" value="^$"/>          
        </module>                  

    </module>

</module>
```

#### 说明

```
代码规范的插件, 它有一个核心目标: check, 通常我们将它绑定到maven某个生命周期的某个阶段, 比如绑定到 default生命周期的package阶段, 在执行package时即可发挥作用, 具体的代码规范可以使用checkstyle.xml 文件进行配置
```

### 2.7  maven-shade-plugin

maven-shade-plugin插件有两个重要的作用：1.将依赖的jar打包到当前jar中（默认情况，不用任何插件的时候是不会将依赖的jar打进来的，注意和插件 maven-jar-plugin 的区别）；2.对依赖的jar包进行重命名（用于类的隔离）

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-shade-plugin</artifactId>
    <version>3.1.1</version>
    <configuration>
        <!-- put your configurations here -->
    </configuration>
    <executions>
        <execution>
            <phase>package</phase>
            <goals>
                <goal>shade</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

#### 说明

1.如果只添加了上述的配置，默认打成target如下

```
target
	-- classes (只包含项目src/main下的*.java文件编译后的class文件, 以及 src/main/resources 下的配置文件)
	-- generated-sources
	-- maven-archiver
	-- maven-status
	-- common-learning-maven-plugins-1.0.0.jar
		-- com
		-- mapper
		-- location-config.properties (当前项目src/main/resources下的配置文件)
		-- org (依赖的第三方jar中的class文件)
			-- apache
			-- springframework
		-- META-INF (当前jar的核心配置文件, 包括maven的pom.xml配置文件和 MENIFEST.MF 文件)
			-- maven
			-- MENIFEST.MF
	-- original-common-learning-maven-plugins-1.0.0.jar (当前项目的jar, 跟没有用任何插件, maven默认方式打成的jar相同)
		-- com (当前项目java对应的class)
		-- mapper (当前项目src/main/resources下的配置文件)
		-- location-config.properties (当前项目src/main/resources下的配置文件)
		-- META-INF (当前jar的核心配置文件, 包括maven的pom.xml配置文件和 MENIFEST.MF 文件)
			-- maven
			-- MENIFEST.MF
```

可以看到，打包后会将 依赖的第三方jar 中的class文件以及resources下的配置文件直接打包到 common-learning-maven-plugins-1.0.0.jar，这里是跟 maven-jar-plugin 不同的地方！maven-jar-plugin 只是会将第三方依赖jar输出到特定位置，并在 MENIFEST.MF 核心配置文件中配置 其为classpath！但是，maven-shade-plugin 则直接将第三方jar中的class文件和配置文件直接打包到jar中。

如果只做了以上配置，那么 仍然无法用 java -jar common-learning-maven-plugins-1.0.0.jar 直接运行，因为 MENIFEST.MF 配置文件中没有指定main-class

```properties
Manifest-Version: 1.0
Archiver-Version: Plexus Archiver
Built-By: DELL
Created-By: Apache Maven 3.8.3
Build-Jdk: 1.8.0_171
```

2.指定main-class启动类

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-shade-plugin</artifactId>
    <version>3.1.1</version>
    <configuration>
        <!-- 指定启动类 -->
        <transformers>
            <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                <mainClass>com.tca.common.learning.maven.plugins.PluginStart</mainClass>
            </transformer>
        </transformers>
    </configuration>
    <executions>
        <execution>
            <phase>package</phase>
            <goals>
                <goal>shade</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

查看 MENIFEST.MF 配置文件

```properties
Manifest-Version: 1.0
Archiver-Version: Plexus Archiver
Built-By: DELL
Created-By: Apache Maven 3.8.3
Build-Jdk: 1.8.0_171
Main-Class: com.tca.common.learning.maven.plugins.PluginStart
```

其他功能在此不作细述

#### 问题1

```
既然使用 maven-shade-plugin 就可以打成可执行jar，此时我们直接使用此插件打包 springboot 工程，可以替代spring开发的用于打包springboot工程的插件 spring-boot-maven-plugin
```

1.搭建工程 common-learning-maven-plugins-springboot

pom.xml

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
        <groupId>tca</groupId>
        <artifactId>common-core</artifactId>
        <version>1.0.0</version>
    </dependency>
</dependencies>
```

首先不添加任何maven插件，即使用maven默认方式打包，打包后的结果和普通maven工程打包出来的没有不同，即

```
target
    -- classes // 存放了当前项目的classpath下普通的java文件编译后的class文件, 以及classpath下面的配置文件
    -- generated-sources // 暂略
    -- maven-archiver // 生成了 pom.properties, 其中主要是当前项目的 maven 配置信息
    -- maven-status // 当前项目使用的maven插件
    -- common-learning-maven-plugins-springboot-1.0.0.jar // 打成的jar, 默认情况下, jar中不包含依赖的第三方jar
```

直接运行会报错：1.没有指定main-class；2.没有将第三方依赖打印进jar，或没有使用maven-jar-plugin的方式将第三方jar输出并在MENIFEST.MF文件中指定其为class-path

2.使用maven-shade-plugin插件打包

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-shade-plugin</artifactId>
    <version>3.1.1</version>
    <configuration>
        <!-- 指定启动类 -->
        <transformers>
            <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                <mainClass>com.tca.common.learning.maven.plugins.springboot.PluginSpringBootApplication</mainClass>
            </transformer>
        </transformers>
    </configuration>
    <executions>
        <execution>
            <phase>package</phase>
            <goals>
                <goal>shade</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

打包，运行，报错

```
java.lang.IllegalArgumentException: No auto configuration classes found in META-INF/spring.factories. If you are using a custom packaging, make sure that file is correct.
        at org.springframework.util.Assert.notEmpty(Assert.java:464) ~[common-learning-maven-plugins-springboot-1.0.0.jar:na]
        at org.springframework.boot.autoconfigure.AutoConfigurationImportSelector.getCandidateConfigurations(AutoConfigurationImportSelector.java:173) ~[common-learning-maven-plugins-springboot-1.0.0.jar:na]
......
```

可以看到，直接使用maven-shade-plugin的插件打包，运行也会出错！！！因为，读不到依赖的jar中的META-INF/spring.factories文件。原因：因为这种打包方式，会把所有依赖的jar的class文件、配置文件等杂糅在一起，打包进来，这样如果依赖的第三方jar中相同名字的配置文件就会发生冲突，就会被覆盖！！！

3.使用maven-jar-plugin配合maven-dependency-plugin

```xml
<!-- 打包jar文件时，配置manifest文件，加入lib包的jar依赖 -->
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-jar-plugin</artifactId>
    <version>3.1.0</version>
    <configuration>
        <classesDirectory>target/classes/</classesDirectory>
        <archive>
            <manifest>
                <!-- 启动类 -->
                <mainClass>com.tca.common.learning.maven.plugins.springboot.PluginSpringBootApplication</mainClass>
                <!-- 打包时 MANIFEST.MF文件不记录的时间戳版本 -->
                <useUniqueVersions>false</useUniqueVersions>
                <!-- 是否添加依赖的jar路径配置 -->
                <!-- 为依赖包添加路径, 这些路径会写在MANIFEST文件的Class-Path下 -->
                <addClasspath>true</addClasspath>
                <!-- 依赖的jar包存放路径, 和生成的jar放在同一级目录下的lib -->
                <!-- 这个jar所依赖的jar包添加classPath的时候的前缀，如果这个jar本身
                            和依赖包在同一级目录，则不需要添加 -->
                <classpathPrefix>lib</classpathPrefix>
            </manifest>
            <manifestEntries>
                <Class-Path>.</Class-Path>
            </manifestEntries>
        </archive>
    </configuration>
</plugin>

<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-dependency-plugin</artifactId>
    <executions>
        <execution>
            <id>copy-dependencies</id>
            <phase>package</phase>
            <goals>
                <goal>copy-dependencies</goal>
            </goals>
            <configuration>
                <type>jar</type>
                <includeTypes>jar</includeTypes>
                <outputDirectory>${project.build.directory}/lib</outputDirectory>
            </configuration>
        </execution>
    </executions>
</plugin>
```

打包，运行成功！

但是没有打在一个jar中，这样部署就相对麻烦了一些！

3.所以，spring自身开发了一个maven插件，可以将springboot打包成可执行jar

```xml
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <version>${spring-boot.version}</version>
    <configuration>
        <addResources>true</addResources>
        <finalName>${project.artifactId}</finalName>
    </configuration>
    <executions>
        <execution>
            <goals>
                <goal>repackage</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

#### 问题2

```
我们依赖的第三方jar中, 也会有配置文件xxx.properties, xxx.yml, 甚至有 application.yml 配置文件, 那么问题来了？采用上述后两者插件打包springboot项目, 运行时会有什么不同? 可以读到配置文件吗? 可以使用@Value自动注入依赖的第三方的jar中的application.yml文件吗
```

1.首先，我们修改 common-learning-maven-package 工程，在其resources路径下添加 application.yml 配置文件

```yaml
location: default-maven-package.yml
```

此时，common-learning-maven-package 工程的resources目录下有两个文件

```
src/main/resources
	-- application.yml
	-- location-config.properties
```

2.修改 common-learning-maven-plugins-springboot，添加对 common-learning-maven-package 的依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
        <groupId>tca</groupId>
        <artifactId>common-core</artifactId>
        <version>1.0.0</version>
    </dependency>

    <dependency>
        <groupId>tca</groupId>
        <artifactId>common-learning-maven-package</artifactId>
        <version>1.0.0</version>
    </dependency>
</dependencies>
```

添加controller

```java
@RestController
@RequestMapping("/hello")
public class HelloController {

    @Value("${location:}")
    private String location;

    @GetMapping("/application")
    public Result<String> application() {
        return Result.success("hello, plugin-spring-boot, my location is " + location);
    }

    @GetMapping("/location")
    public Result<String> location() throws IOException {
        try {
            PropertiesReader.main(null);
        } catch (IOException e) {
        }
        System.out.println(this.getClass().getClassLoader());
        return Result.success(PropertiesReader.readProperties("location-config.properties").getProperty("location"));
    }
}
```

3.说明

3.1 当我们采用传统的方式 maven-jar-plugin 配合 maven-dependency-plugin 两个插件打包后，运行

访问 /hello/applicaiton 时，返回结果如下，说明并没有将第三方依赖的jar（common-learning-maven-package）中的application.yml配置文件解析到Environment环境变量中！！！

```
hello, plugin-spring-boot, my location is 
```

访问 /hello/location时，返回结果如下，说明使用jdk自带的App类加载器进行加载，并可以读取到依赖的第三方jar中的properties文件

```
default-maven-package
jdk.internal.loader.ClassLoaders$AppClassLoader@6d5380c2
```

3.2 当我们采用spring提供的插件 spring-boot-maven-plugin 打包后，运行

访问 /hello/applicaiton 时，返回结果和上面的方式一样，说明并没有将第三方依赖的jar（common-learning-maven-package）中的application.yml配置文件解析到Environment环境变量中！！！

访问 /hello/location时，返回结果如下，说明使用spring自定义的类加载器LaunchedURLClassLoader进行加载，并可以读取到依赖的第三方jar中的properties文件

```
default-maven-package
org.springframework.boot.loader.LaunchedURLClassLoader@68be2bc2
```

总结

```
不管采用什么方式打包, 都会将第三方jar作为classpath来处理, 即默认都可以读到第三方jar中的配置文件, 所以如果依赖的第三方jar中有log4j2.xml等日志配置文件, 依然是可以起作用的, 关键是要看日志框架中是如何读取日志配置文件的！
但是, 在springboot中, springboot读取主配置文件 application.yml 和 application.properties 时, 它只会读取当前项目中原有的主配置文件, 将其加载到Environment变量中, 不会读取依赖的第三方的jar中的 application.yml 和 application.properties！这里, 我们可以通过springboot源码解析看出来！！！
springboot的SPI机制, 读取META-INF/spring.factories文件, 是会从第三方jar中读取的, 但是读取主配置文件, 只会从当前项目中读！！！
```

### 2.8 flatten-maven-plugin

flatten-maven-plugin可以用来作maven聚合工程的版本号管理

```xml
<plugins>
    <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>flatten-maven-plugin</artifactId>
        <version>1.1.0</version>
        <configuration>
            <updatePomFile>true</updatePomFile>
            <flattenMode>resolveCiFriendliesOnly</flattenMode>
        </configuration>
        <executions>
            <execution>
                <id>flatten</id>
                <phase>process-resources</phase>
                <goals>
                    <goal>flatten</goal>
                </goals>
            </execution>
            <execution>
                <id>flatten.clean</id>
                <phase>clean</phase>
                <goals>
                    <goal>clean</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
</plugins>
```

#### 说明

```
1.在parent中定义revision, 使用 ${revision} 和 ${project.version} 管理maven聚合项目版本, 项目版本号定义使用 <version>${revision}</version>, 项目之间版本相互依赖在<dependency>中定义<version>${project.verison}</version>

2.需要高版本的maven支持, 使用3.3.9版本的maven是不行的, 目前使用3.8.3的版本是可以的
```

#### 实际应用

```
在common脚手架工程中, 我们需要使用统一的版本号, 即有如果修改其中某一个子模块的版本号, 就可能会影响到其他模块, 所以需要统一版本号: 即其中任意一个子模块的版本号被修改了, 我们需要直接修改rversion, 即所有模块的版本号需要保持一致！具体如下:
```

##### common

pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>tca</groupId>
    <artifactId>common</artifactId>
    <version>${revision}</version>
    <description>聚合common</description>
    <packaging>pom</packaging>

    <!-- 定义rversion统一版本号 -->
    <properties>
        <revision>1.0.0</revision>
    </properties>

    <modules>
        <module>common-parent</module>
        <module>common-cache</module>
        <module>common-utils</module>
        <!-- ...其他模块... -->
    </modules>

</project>
```

说明

```
1.common最外层工程只是用来作聚合, 并且定义统一的版本号
```

##### common-parent

pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>common</artifactId>
        <groupId>tca</groupId>
        <version>${revision}</version>
        <relativePath>../pom.xml</relativePath>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>common-parent</artifactId>
    <description>版本管理</description>
    <packaging>pom</packaging>


    <properties>
        <!-- 字符编码 -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <!-- jdk -->
        <java.version>1.8</java.version>
        <!-- springframework 与 springboot版本兼容-->
        <spring.version>5.3.13</spring.version>
        <!-- springboot -->
        <spring-boot.version>2.6.1</spring-boot.version>
        <spring-boot-redis.version>2.3.2.RELEASE</spring-boot-redis.version>
        <!-- springcloud 与springboot版本兼容 -->
        <spring-cloud.version>2021.0.0</spring-cloud.version>
        <!-- springcloud alibaba 与springboot版本兼容 -->
        <spring-cloud-alibaba.version>2.2.6.RELEASE</spring-cloud-alibaba.version>
        <!-- ...其他各种版本号... -->
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- spring-beans -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-beans</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <!-- spring boot -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!-- springboot-redis整合 -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-data-redis</artifactId>
                <version>${spring-boot-redis.version}</version>
            </dependency>
            <!-- spring cloud -->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!--spring cloud alibaba-->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring-cloud-alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
			
            <!-- ...其他jar版本依赖定义... -->
        </dependencies>
    </dependencyManagement>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
		
        <!-- maven相关插件版本管理 -->
        <pluginManagement>
            <plugins>
                <!-- 代码规范插件 -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>3.0.0</version>
                    <executions>
                        <execution>
                            <!-- 绑定pmd:pmd到 package 生命周期，在 package 时会自动进行代码规范检查 -->
                            <id>package</id>
                            <phase>package</phase>
                            <configuration>
                                <!-- 配置文件的路径，在style文件夹下 -->
                                <configLocation>../codestyle/checkstyle.xml</configLocation>
                                <encoding>UTF-8</encoding>
                                <consoleOutput>true</consoleOutput>
                                <failsOnError>true</failsOnError>
                                <includeTestSourceDirectory>false</includeTestSourceDirectory>
                            </configuration>
                            <goals>
                                <goal>check</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>3.0.1</version>
                    <configuration>
                        <attach>true</attach>
                    </configuration>
                    <executions>
                        <execution>
                            <phase>compile</phase>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <!-- 将springboot打成可执行jar -->
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>${spring-boot.version}</version>
                    <configuration>
                        <addResources>true</addResources>
                        <finalName>${project.artifactId}</finalName>
                    </configuration>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>

        <!-- maven插件 -->
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>flatten-maven-plugin</artifactId>
                <version>1.1.0</version>
                <configuration>
                    <updatePomFile>true</updatePomFile>
                    <flattenMode>resolveCiFriendliesOnly</flattenMode>
                </configuration>
                <executions>
                    <execution>
                        <id>flatten</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>flatten</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>flatten.clean</id>
                        <phase>clean</phase>
                        <goals>
                            <goal>clean</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- 编译插件 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.5.1</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <debug>true</debug>
                    <debuglevel>lines,vars,source</debuglevel>
                    <!-- 编译后参数名不变 -->
                    <compilerArgs>
                        <arg>-parameters</arg>
                    </compilerArgs>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>alimaven</id>
            <name>aliyun maven</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

</project>
```

说明

```
1.在common-parent中, 主要定义第三方依赖的jar的版本, 插件版本, 插件, 仓库配置(依赖仓库、插件仓库、发布仓库等)
```

##### common-cache

pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>common-parent</artifactId>
        <groupId>tca</groupId>
        <version>${revision}</version>
        <relativePath>../common-parent</relativePath>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>common-cache</artifactId>
    <packaging>pom</packaging>

    <modules>
        <module>common-cache-redis</module>
        <module>common-cache-test</module>
        <module>common-cache-api</module>
    </modules>

</project>
```

说明

```
1.common-cache本身是一个聚合项目, 它继承了common-parent, 它下面有三个子模块：
common-cache-api, common-cache-redis, common-cache-test, 下面以common-cache-redis为例, 介绍其pom配置文件
```

###### common-cache-redis

pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>common-cache</artifactId>
        <groupId>tca</groupId>
        <version>${revision}</version>
        <relativePath>../../common-cache</relativePath>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>common-cache-redis</artifactId>
    <description>redis缓存</description>

    <dependencies>

        <!-- springboot-redis整合 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>

        <!-- redisson -->
        <dependency>
            <groupId>org.redisson</groupId>
            <artifactId>redisson-spring-boot-starter</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.redisson</groupId>
                    <artifactId>redisson</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.redisson</groupId>
            <artifactId>redisson</artifactId>
        </dependency>


        <!-- json -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
        </dependency>

        <!-- commons-pool2  解决jar冲突 -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-pool2</artifactId>
        </dependency>

        <!-- common-utils -->
        <dependency>
            <groupId>tca</groupId>
            <artifactId>common-utils</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- common-cache-api -->
        <dependency>
            <groupId>tca</groupId>
            <artifactId>common-cache-api</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
```

说明

```
1.common-cache-redis项目的父项目为common-cache, 并不是common-parent, 这里需要注意！！！
```

[参考](https://maven.apache.org/maven-ci-friendly.html)



[maven基础学习参考](./maven.md)