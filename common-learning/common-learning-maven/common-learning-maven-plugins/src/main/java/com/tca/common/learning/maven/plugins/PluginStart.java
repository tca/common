package com.tca.common.learning.maven.plugins;

import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

/**
 * @author zhoua
 */
public class PluginStart {

    public static void main(String[] args) throws IOException {
        System.out.println("plugin start");

        // 打印类加载器
        ClassLoader classLoader = PluginStart.class.getClassLoader();
        System.out.println("当前类加载器为: " + classLoader);

        // 打印classpath
        Enumeration<URL> resources = classLoader.getResources("");
        while (resources.hasMoreElements()) {
            URL url = resources.nextElement();
            System.out.println("当前classpath: " + url);
        }

        // 打印依赖的第三方jar

        ClassLoader thirdJarClassLoader = StringUtils.class.getClassLoader();
        System.out.println("spring-core.jar的类加载器为: " + thirdJarClassLoader);

    }
}
