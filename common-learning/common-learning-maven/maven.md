# maven

## 1.HTTP代理配置

为什么需要http代理

```
有时候公司基于安全因素考虑, 要求你使用通过安全认证的代理访问外网。这种情况下, 需要为maven配置http代理, 才能让它正常访问外部仓库！
```

前提

```
1.首先确认自己无法直接访问公共的maven中央仓库, 直接运行命令 ping repo1.maven.org, 检查网络情况, 如果ping不通, 则可能需要代理服务
2.确认代理服务, 如果代理服务为 218.14.227.197:3128, 则使用 telnet 218.14.227.197 3128 检查代理服务是否正常
```

设置http代理

修改核心配置文件 conf/settings.xml

```xml
<proxies>
    <proxy>
      <!-- 代理id, 全局唯一 -->  
      <id>proxy-id</id>
      <!-- 是否激活, 如果配置多个, 只能一个处于激活状态 -->
      <active>true</active>
      <!-- 代理协议 -->  
      <protocol>http</protocol>
      <!-- 代理服务鉴权: 用户名和密码 -->
      <username>proxyuser</username>
      <password>proxypass</password>
      <!-- 代理服务器ip, port -->
      <host>proxy.host.net</host>
      <port>80</port>
      <!-- 配置哪些主机不需要通过代理 -->
      <nonProxyHosts>local.net|some.host.com</nonProxyHosts>
    </proxy>
</proxies>
```



## 2.MAVEN_OPTS配置

```
因为maven是java编写的, 所以实际上, 运行mvn命令(如 mvn clean 等), 执行的是java命令, 既然运行的是java, 那么就需要java的运行参数, 可以通过配置 MAVEN_OPTS 环境变量, 来控制maven运行时环境。如配置 MAVEN_OPTS 为 -Xms128m -Xmx1024m, 即可以避免一般情况下的 OOM
```



## 3.使用Archetype生成maven项目骨架

```
在平时, 我们都是使用idea的maven插件, 直接在idea中创建maven项目, 实际上也是通过maven的 maven-archetype-plugin 插件来完成的, 我们也可以直接通过命令行来创建maven工程:
mvn archetype:generate
并按照提示依次指定 groupId, artifactId, version, package, archetype等
```



## 4.依赖配置

```xml
<dependencies>
	<dependency>
        <groupId>...</groupId>
        <artifactId>...</artifactId>
        <version>...</version>
        <type>...</type>
        <scope>...</scope>
        <optional>...</optional>
        <exelusions>
        	<exclusion>
            ...    
            </exclusion>
            ...
        </exelusions>
    </dependency>
...
</dependencies>
```

### 4.1 基本坐标

```
groupId, artifactId, version: 依赖的基本坐标
```

### 4.2 依赖类型

```
type: 依赖的类型, 默认为jar, 也可以是 war 和 pom
```

### 4.3 依赖范围

```
scope: 依赖的范围
```

#### 4.3.1 原理

```
maven在编译项目主代码的时候需要使用一套classpath, 其次, 在编译和执行测试的时候使用另外一套classpath, 实际运行的时候, 也会使用一套classpath
```

#### 4.3.2 常用选项

##### compile

```
编译依赖范围。如果没有指定, 就会默认使用该依赖范围。使用此依赖范围的maven依赖, 对于编译、测试和运行的时候都需要使用该依赖
```

##### test

```
测试依赖范围。使用此依赖范围的maven依赖时, 只对于测试classpath有效, 在编译主代码或者运行项目的使用时将无法使用此类依赖, 最常见的就是 Junit 
```

##### provided

```
已提供依赖范围。使用此依赖范围的maven依赖时, 对于编译和测试classpath有效, 但在运行时无效。最常见的是servlet-api, 编译和测试项目的时候都需要, 但在运行项目的时候, 由于servlet容器已经提供, 就不需要maven重复地再引入一次
```

##### runtime

```
运行时依赖范围。使用此依赖范围的maven依赖, 对于测试和运行classpath有效, 但在编译主代码时无效。最常见的是JDBC驱动实现, 项目主代码的编译只需要jdk提供的JDBC接口, 只有在执行测试或者运行项目的时候才需要实现上述接口的具体JDBC驱动
```

##### system

```
系统范围依赖。该依赖与三种classpath的关系, 和provided依赖范围完全一致。但是, 使用system范围的依赖时必须通过systemPath元素显式地指定依赖文件的路径。由于此依赖不是通过maven仓库解析的, 而且往往与本机系统绑定, 可能造成构建的不可移植, 因此应该谨慎使用
```

使用场景

```
我们依赖了一个中央仓库中没有的jar(如通过本地打包, 生成maven坐标), 我们将其放在 /home/tca/lib 路径下, 此时在代码中不能直接使用这个jar中的java类, 而是需要通过SPI机制, 或者通过反射机制使用！！！
```

##### import

```
<scope>import</scope> 一般和 <type>pom</type> 一起使用。该范围的依赖只在dependencyManagement元素下才有效果, 使用该范围依赖通常指向一个pom, 作用是将引入的目标pom的dependencyManagement配置导入并合并到当前pom的dependencyManagement元素中
```

#### 4.3.3 传递依赖和依赖范围的关系

|          | compile  | test | provided | runtime  |
| -------- | -------- | ---- | -------- | -------- |
| compile  | compile  | -    | -        | runtime  |
| test     | test     | -    | -        | test     |
| provided | provided | -    | provided | provided |
| runtime  | runtime  | -    | -        | runtime  |

 ```
1.如果 A 依赖 B, B 依赖 C, 则称: A对于B是第一直接依赖, B对于C是第二直接依赖, A对于C是传递依赖。第一直接依赖和第二直接依赖的范围决定了传递依赖的范围
2.如上图, 第一列表示 第一直接依赖的范围, 第一行表示第二直接依赖的范围, 中间交叉的单元格表示传递依赖的范围！
 ```

### 4.4 可选依赖

```
optional: 表示可选依赖。作用: 如果A依赖B, 依赖范围为compile, B依赖C, 也为compile, 按照传递依赖的规则, A默认是会依赖C的, 如果不想要依赖B的项目都依赖C, 则可以在B项目中依赖C的配置中加上<optional>true</optional>(默认为false, 表示支持传递依赖), 则当其他项目依赖B时, 不会自动依赖C  
```



## 5.远程仓库配置

### 5.1 全局配置 （镜像配置）

#### 配置仓库镜像

一般, 使用中央仓库, 因为是国外的服务器, 所以访问较慢, 我们一般会配置aliyun镜像，在conf/settings.xml文件中进行配置，可以配置多个镜像，但是默认只会有一个生效！

```xml
<mirrors>
     <!-- mirrorOf的值为central, 表示该配置为中央仓库的镜像, 任何对于中央仓库的请求都会转至该镜像, 用户也可以使用同样的方法配置其他仓库的镜像。id表示镜像的唯一标识符, name表示镜像的名称, url表示镜像的地址 --> 
	 <mirror>
		<id>alimaven</id>
		<name>aliyun maven</name>
		<url>http://maven.aliyun.com/nexus/content/groups/public/</url>
		<mirrorOf>central</mirrorOf>       
	</mirror>
  </mirrors>
```

#### 配置远程仓库

conf/settings.xml主配置文件中不能配置<repositories>和<repository>标签，但是提供了<profile>标签

```xml
<profiles>
  <profile>
    <id>default</id>
    <repositories>
        <repository>
            <id>aliyun</id>
            <name>Nexus aliyun</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>   
        <repository>      
            <id>nexus</id>      
            <name>nexus私服URL</name>      
            <url>http://ip:port/repository/maven-public/</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
    </repositories>
  </profile>
</profiles>
<activeProfiles>
  <activeProfile>default</activeProfile>
</activeProfiles>
```

### 5.2 项目配置

#### 5.2.1 pom.xml配置

针对于每一个项目，可以配置单独的中央仓库，如配置私服作为中央仓库，如我们在项目中配置如下

```xml
<repositories>
    <!-- 配置私服作为项目的中央仓库, id表示唯一标识符, name表示名称, url表示私服的地址 -->
    <repository>
        <id>hfepay</id>
        <name>Nexus Release Repository</name>
        <url>http://172.16.23.16:8081/repository/maven-public/</url>
        <!-- 表示会拉取快照版的构件 -->
        <snapshots>
            <enabled>true</enabled>
            <!-- 配置maven从远程仓库检查更新的频率, daily为每天检查一次, 为默认值 -->
            <updatePolicy>daily</updatePolicy>
            <!-- 忽略构件的文件校验 -->
            <checksumPolicy>ignore</checksumPolicy>
        </snapshots>
        <!-- 表示会拉取发布版的构件 -->
        <releases>
            <enabled>true</enabled>
        </releases>
    </repository>
</repositories>
```

#### 5.2.2 鉴权配置

大多数远程仓库无需认证就可以访问，但有时候处于安全考虑，需要提供认证信息才能访问一些远程仓库。但是，配置认证信息和配置仓库信息不同，仓库信息可以直接在项目的pom.xml配置文件中配置，但是认证信息必须配置在settings.xml文件中。这是因为 pom 往往会被提交到代码仓库中供所有成员访问，而settings.xml 一般只放在本机上。因此，在settings.xml 中配置认证信息更为安全。

```xml
<servers>
    <server>
        <!-- 这里的id需要与上述repository的id一致, 表示其为上述repository的鉴权信息, 包括用户名和密码 -->
        <id>hfepay</id>  
        <username>devops</username>  
        <password>devops@123</password>  
    </server>  
</servers>
```

### 5.3 参考

[参考](https://haizei.blog.csdn.net/article/details/87088388?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.essearch_pc_relevant&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.essearch_pc_relevant)



## 6.部署到远程仓库（私服）

### 6.1 pom.xml配置

部署到远程仓库的配置，一般在项目的 pom.xml 文件中配置，如下，distributionManagement包含repository和snapshotRepository两个子元素。repository表示发布版本构件的仓库，snapshotRepository表示快照版本的仓库。

```xml
<distributionManagement>
    <repository>
        <id>releases</id>
        <name>Nexus Release Repository</name>
        <url>http://172.16.23.16:8081/repository/maven-releases/</url>
    </repository>
    <snapshotRepository>
        <id>snapshots</id>
        <name>Nexus Snapshot Repository</name>
        <url>http://172.16.23.16:8081/repository/maven-snapshots/</url>
    </snapshotRepository>
</distributionManagement>
```

### 6.2 鉴权配置

往远程仓库部署构件的时候，通常都需要鉴权认证。配置认证的方式与@5.2.2相同，即在 conf/settings.xml 配置文件中配置server节点，id与上述distributionManagement中的id保持一致

```xml
<servers>
    <server>
        <!-- 这里的id需要与上述repository的id一致, 表示其为上述repository的鉴权信息, 包括用户名和密码 -->
        <id>releases</id>  
        <username>devops</username>  
        <password>devops@123</password>  
    </server>  
    <server>
        <!-- 这里的id需要与上述snapshotRepository的id一致, 表示其为上述snapshotRepository的鉴权信息, 包括用户名和密码 -->
        <id>snapshots</id>  
        <username>devops</username>  
        <password>devops@123</password>  
    </server>
</servers>
```

 

## 7.快照版本

### 7.1 作用

```
快照版本一般作用于企业内协同开发。如: 张三和李四协同开发, 张三开发A模块, 李四开发B模块, B依赖于A。在开发过程中B可能需要实时获取最新的A, 此时张三需要将A模块的版本号定义为 xxx-SNAPSHOT, 然后部署到私服, 这样B可以不需要手动清理本地仓库。对于SNAPSHOT版本的依赖, 默认情况下, maven会每天检查一次(可配置), 本地仓库中的构件是不是最新的, 并进行更新！！
```

### 7.2 版本定义

```
如果将版本定义为 1.0-SNAPSHOT, 在发布过程中, maven会自动为构件打上时间戳, 比如 2.1-20191214.221414-13 表示2019年12月14日22点14分14秒构建的, 为第13个快照版本, 这样很容易比对本地仓库和远程库中是否一致
```

### 7.3 强制更新

```
mvn clean install -U
```



## 8.生命周期和插件

### 8.1 三套生命周期

#### 概述

```
1.maven拥有三套相互独立的生命周期, 分别为 clean, default, site。clean生命周期的目的是清理项目, default生命周期的目的是构建项目, site生命周期的目的是建立项目站点
2.每个生命周期都包含一些阶段(phase), 阶段是有顺序的, 并且后面的阶段依赖前面的阶段。用户和maven最直接的交互方式就是调用这些声明周期阶段。比如clean生命周期, 有三个阶段: pre-clean, clean, post-clean, 当用户调用 pre-clean 时, 只会执行 pre-clean 阶段; 当用户调用 clean 时, pre-clean和clean阶段都会被执行
3.三套生命周期是相互独立的, 每一套生命周期中的阶段有依赖关系
```

#### clean

##### 作用

```
清理项目
```

##### 阶段

```
pre-clean: 执行一些清理前需要完成的工作
clean: 清理上一次构建生成的文件
post-clean: 执行一些清理后需要完成的工作
```

#### default

##### 作用

```
default生命周期定义了真正构建时所需要执行的所有步骤, 它是所有生命周期中最核心的部分
```

##### 阶段

```
validate
initialize
generate-sources
process-sources: 处理项目主资源文件。一般来说, 是对src/main/resources目录的内容进行变量替换等工作后, 复制到项目输出的主classpath目录中
generate-resources
process-resources
compile: 编译项目的主源码。一般来说, 是编译src/main/java目录下的java文件至项目输出的主classpath目录中
process-classes
generate-test-sources
process-test-sources
generate-test-resources
process-test-resources
test-compile
process-test-classes
test: 使用单元测试框架运行测试, 测试代码不会被打包或部署
prepare-package
package: 接受编译好的代码, 打包成可发布的格式, 如jar
pre-integration-test
integration-test
post-integration-test
verify
install: 将包安装到maven本地仓库
deploy: 将包复制到远程仓库
```

#### site

##### 作用

```
site生命周期的目的是建立和发布项目站点, maven能够基于pom所包含的信息, 自动生成一个友好的站点
```

##### 阶段

```
pre-site: 执行一些在生成站点之前需要完成的工作
site: 生成项目站点文档
post-site: 执行一些在生成项目站点之后需要完成的工作
site-deploy: 将生成的项目站点发布到服务器上
```

### 8.2 maven命令行与生命周期的关系

从命令行执行maven指令, 基本都是在使用maven插件调用maven生命周期阶段！

```
mvn clean: 表示调用clean生命周期的clean阶段, 因为生命周期内的阶段是相互依赖的, 一次会执行clean生命周期的pre-clean阶段和clean阶段
mvn clean install: 表示调用clean生命周期的clean阶段和default生命周期的install阶段, 实际执行的是clean生命周期的pre-clean阶段和clean阶段, 以及install生命周期的从validate到install的阶段 (但是如果我们只执行了 mvn install, 那么就不会执行clean生命周期的相关阶段)
```

### 8.3  插件

#### 8.3.1 插件目标

```
每个插件可能有多个功能, 每个功能对应一个目标。如, 对于 maven-dependency-plugin, 它有多个功能及目标。如执行 mvn dependency:analyze 可以完成项目依赖分析; 执行 mvn dependency:tree 可以列出项目的依赖树
语法为: mvn $插件前缀:$插件目标
```

#### 8.3.2 插件绑定

```
maven生命周期阶段与插件(或插件目标)相互绑定, 用以完成实际的构建！如项目编译这个任务, 对应了default生命周期的compile阶段, maven-compiler-plugin 这一插件的 complie目标绑定！
```

##### 内置绑定

- clean生命周期

  ```
  clean生命周期有pre-clean, clean 和 post-clean 三个生命周期。其中clean阶段与插件目标 maven-clean-plugin:clean绑定
  ```

- default生命周期

  常见的内置绑定如下：

  ```
  1.process-resources: 插件目标为: maven-resources-plugin:resources, 执行任务为: 复制主资源文件至主输出目录
  2.compile: 插件目标为: maven-compliler-plugin:compile, 执行任务为: 编译主代码至主输出目录
  3.package: 插件目标为: maven-jar-plugin:jar, 执行任务为: 创建项目jar包
  4.install: 插件目标为: maven-install-plugin:install, 执行任务为: 将项目输出构件安装到本地仓库
  5.deploy: 插件目标为: maven-deploy-plugin:deploy, 执行任务为: 将项目输出构件部署到远程仓库
  ```

- site生命周期

  ```
  site生命周期有pre-site, site, post-site和site-deploy四个阶段, 其中, site阶段和maven-site-plugin:site绑定, site-deploy阶段和maven-site-plugin:deploy目标绑定
  ```


##### 自定义绑定

```
用户可以在项目的pom.xml文件中配置插件, 自己选择将某个插件目标绑定到生命周期的某个阶段上！
```

- 如下配置，添加maven-source-plugin插件，将其jar目标绑定到default生命周期的compile阶段！！！

```xml
<build>
	<plugins>
    	<plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <version>3.1.0</version>
            <configuration>
                <attach>true</attach>
            </configuration>
            <executions>
                <execution>
                    <phase>compile</phase>
                    <goals>
                        <goal>jar</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

- 有时候，我们不需要配置<phase>和<goal>也可以，因为很多插件的目标在编写时已经定义了默认的绑定阶段
- 当多个插件目标绑定到同一个生命周期阶段时，这些插件声明的先后顺序决定了目标的执行顺序

#### 8.3.3 插件参数配置

##### 命令行插件参数配置

```
有些插件参数一般在命令行中配置, 如: maven-surefire-plugin 插件提供了一个参数 maven.test.skip, 如我们执行 mvn install -Dmaven.test.skip=true 时, 过程中会执行插件目标 maven-surefire-plugin:test, 当设置maven.test.skip=true 时, 会跳过测试！
```

##### pom文件中配置参数

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>3.1.0</version>
    <configuration>
        <source>1.8</source> <!-- 源代码使用的JDK版本 -->
        <target>1.8</target> <!-- 需要生成的目标class文件的编译版本 -->
        <encoding>UTF-8</encoding> <!-- 字符集编码 -->
    </configuration>
</plugin>
```

如上，我们配置了source=1.8，target=1.8，encoding=UTF-8

#### 8.3.4 命令行调用插件

插件的功能及目标不一定需要与生命周期的阶段绑定，如使用 mvn dependency：tree 查看当前项目的依赖树结构，与生命周期毫无关系，可以使用如下命令调用插件

```
mvn [options] [goals] [phase]
```

#### 8.3.5 插件解析机制

##### 插件仓库

- 全局配置

```
在 conf/settings.xml 主配置文件中的 <profile> 标签中定义 <pluginRepositoreis> 标签
```

- 项目配置

```
在项目中的 <build> 标签中定义 <pluginRepositoreis> 标签
```

```xml
<pluginRepositories>
    <pluginRepository>
        <id>default</id>
        <name>default-plugin-repo</name>
        <url>http://repo1.maven.org/maven2/</url>
        <releases>
            <enabled>true</enabled>
            <updatePolicy>daily</updatePolicy>
        </releases>
        <snapshots>
            <enabled>false</enabled>
            <checksumPolicy>warn</checksumPolicy>
        </snapshots>
        <layout>default</layout>
    </pluginRepository>
</pluginRepositories>
```



## 9.聚合与继承

### 9.1 聚合

#### 说明

```
1.<packaging>使用pom
2.添加<modules>和<module>标签
```

### 9.2 继承

#### 说明

```
1.parent工程的<packaging>使用pom
2.添加<parent>标签, 子标签有<relativePath>, 表示父模块pom的相对路径, 默认值为 ../pom.xml。当项目构建时, maven会根据relativePath检查父pom, 如果找不到, 再从本地仓库中找
```

#### 可继承的pom元素

```
groupId: 项目组id (重要)
version: 版本号 (重要)
description: 描述
organization: 项目的组织信息
inceptionYear: 项目创始年份
url: 项目url地址
developers: 项目的开发者信息 (重要)
contributors: 项目的贡献者信息 (重要)

distributionManagement: 项目的部署信息 (重要)
issueManagement: 项目的缺陷跟踪系统信息
ciManagement: 项目的持续集成系统信息
scm: 项目的版本控制信息
mailingLists: 项目的邮件列表信息
properties: 自定义的maven属性 (重要)
dependencies: 项目的依赖信息 (重要)
dependencyManagement: 项目的依赖管理配置 (重要)
repositories: 项目的仓库配置 (重要)
build: 包括项目的源码目录配置, 输出目录配置, 插件配置, 插件管理配置等 (重要)
reporting: 包括项目的报告输出目录配置, 报告插件配置
```



## 10.超级pom

### 说明

```
1.任何一个maven项目都隐式地继承了一个超级pom, 类似于java中任何一个类都隐式继承了Object类
2.超级pom的文件在 $MAVEN_HOME/lib/maven-model-builder-x.x.x.jar中的org/apache/maven/model/pom-4.0.0.xml路径下
3.超级pom中定义了仓库以及插件仓库, 且都关闭了SNAPSHOT的支持; 同时定义了项目的主输出目录, 主代码输出目录, 最终构件的名称格式, 测试代码, 主资源目录等;
其次还定义了核心插件版本
```



## 11.nexus私服

### 11.1  nexus安装

#### 下载

[下载地址](https://help.sonatype.com/repomanager3/product-information/download)

#### 安装部署

[安装部署参考](https://blog.csdn.net/localhost01/article/details/84350773)

### 11.2 nexus仓库与仓库组

#### 仓库类型

- group（仓库组）

```
仓库组, 用来合并多个hosted/proxy仓库, 当你的项目希望在多个仓库使用资源时不需要多次引用了, 只需要引用一个group即可
```

- hosted（宿主）

```
宿主仓库, 通常我们会部署自己的构件到这一类型的仓库
```

- proxy（代理）

```
代理仓库, 它们被用来代理远程的公共仓库, 如maven中央仓库
```

#### 仓库策略

- 发布版本（Release）
- 快照（Snapshots）

### 11.3 nexus仓库使用

#### 配置maven从nexus下载构件

##### 项目配置

针对项目，是在具体项目的pom.xml配置文件中配置

```xml
<repositories>
    <repository>
        <id>nexus</id>
        <name>nexus</name>
        <url>http://172.16.23.16:8081/repository/maven-public/</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
        <releases>
            <enabled>true</enabled>
        </releases>
    </repository>
</repositories>
<pluginRepositories>
	<pluginRepository>
    	<id>nexus</id>
        <name>nexus</name>
        <url>http://172.16.23.16:8081/repository/maven-public/</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
        <releases>
            <enabled>true</enabled>
        </releases>
    </pluginRepository>
</pluginRepositories>
```

##### 全局配置

针对全局配置，在maven主配置文件$M2_HOME/conf/settings.xml中进行配置

```xml
<profiles>
    <profile>
        <id>nexus</id>
        <repositories>
            <repository>
                <id>nexus</id>
                <name>nexus</name>
                <url>http://172.16.23.16:8081/repository/maven-public/</url>
                <snapshots>
                    <enabled>true</enabled>
                </snapshots>
                <releases>
                    <enabled>true</enabled>
                </releases>
            </repository>
        </repositories>
        <pluginRepositories>
            <pluginRepository>
                <id>nexus</id>
                <name>nexus</name>
                <url>http://172.16.23.16:8081/repository/maven-public/</url>
                <snapshots>
                    <enabled>true</enabled>
                </snapshots>
                <releases>
                    <enabled>true</enabled>
                </releases>
            </pluginRepository>
        </pluginRepositories>
    </profile>
</profiles>
<activeProfiles>
    <activeProfile>nexus</activeProfile>
</activeProfiles>
```

#### 配置镜像让maven只访问nexus私服

##### 全局配置

如果只进行了上述配置，则maven除了从nexus私服上下载之外，还会时不时地访问中央仓库。通常在实际项目开发中，如果我们有nexus私服，我们只希望从私服上下载，以全面发挥私服的作用。此时，我们需要使用镜像配置。在主配置文件$M2_HOME/conf/settings.xml中进行配置

```xml
 <mirrors>
    <mirror>  
      <id>nexus</id>  
      <mirrorOf>*</mirrorOf>  
      <url>http://172.16.23.16:8081/repository/maven-public/</url>  
    </mirror> 
</mirrors>
<profile>  
    <id>nexus</id>  
    <repositories>  
        <repository>  
            <id>central</id>  
            <url>http://central</url>  
            <releases><enabled>true</enabled></releases>  
            <snapshots><enabled>true</enabled></snapshots>  
        </repository>  
    </repositories>  
    <pluginRepositories>  
        <pluginRepository>  
            <id>central</id>  
            <url>http://central</url>  
            <releases><enabled>true</enabled></releases>  
            <snapshots><enabled>true</enabled></snapshots>  
        </pluginRepository>  
    </pluginRepositories>  
</profile> 
<activeProfiles>
    <activeProfile>nexus</activeProfile>
</activeProfiles>
```

注意，这里插件仓库和仓库的id都是用central，即它们都覆盖了超级pom中央仓库的配置，它们的url已经无关紧要了，因为所有的请求都会通过镜像访问私服地址！！！

### 11.4 部署构件到nexus仓库

#### 项目配置

需要在项目的配置文件中配置，配置如下

```xml
<distributionManagement>
    <repository>
        <id>releases</id>
        <name>Nexus Release Repository</name>
        <url>http://172.16.23.16:8081/repository/maven-releases/</url>
    </repository>
    <snapshotRepository>
        <id>snapshots</id>
        <name>Nexus Snapshot Repository</name>
        <url>http://172.16.23.16:8081/repository/maven-snapshots/</url>
    </snapshotRepository>
</distributionManagement>
```

但是，nexus的仓库对于匿名用户是只读的，为了能够部署构件，需要在主配置文件$M2_HOME/conf/settings.xml文件中配置认证信息，需要特别注意的是：
如果镜像里配置的也是私服，则对应的id也需要配置<server>鉴权节点！！！

```xml
<servers>
    <server>
        <id>nexus</id>
        <username>admin</username>
        <password>admin123</password>
    </server>
    <server> 
        <id>snapshots</id>
        <username>admin</username>
        <password>admin123</password>
    </server>
</servers>
```



## 12.maven属性

### 内置属性

```
${basedir}: 表示项目根目录, 即包含pom.xml文件的目录
${version}: 表示项目的版本号
```

### pom属性

```
${project.build.sourceDirectory}: 项目的主源码目录, 默认为 src/main/java/
${project.build.testSourceDirectory}: 项目的测试源码目录, 默认为src/test/java/
${project.build.directory}: 项目构建的输出目录, 默认为target/
${project.outputDirectory}: 项目主代码编译输出目录, 默认为target/classes/
${project.testOutputDirectory}: 项目测试代码编译输出目录, 默认为target/test-classes/
${project.groupId}: 项目的groupId
${project.artifactId}: 项目的artifactId
${project.version}: 项目的version, 与${version}等价
${project.build.finalName}: 项目打包输出文件的名称, 默认为${project.artifactId}-${project.version}
```

### 自定义属性

```
在项目中的<properties>中定义的属性
```

### settings属性

```
用户使用settings.开头的属性引用 settings.xml 文件中的xml元素的值, 如常用的 ${settings.localRepository} 指向用户本地仓库的地址
```

### java属性

```
${user.home} 指向用户目录
```

### 环境变量属性

```
使用 env.开头的属性引用环境变量, 如 ${env.JAVA_HOME} 指代了JAVA_HOME环境变量的值
```



## 13.自定义maven插件

### 13.1 编写步骤

1.创建一个maven-plugin项目

```
插件本身也是maven项目, 特殊的地方在于它的packaging必须是maven-plugin
```

2.为插件编写目标

```
每个插件都必须包含一个或多个目标, maven称之为 Mojo(与Pojo对应)。编写插件的时候必须提供一个或多个继承自AbstractMojo的类
```

3.为目标提供配置点

```
大部分maven插件及其目标都是可配置的, 因此编写Mojo的时候需要注意提供可配置的参数
```

4.编写代码实现目标行为

```
根据实际的需要实现Mojo
```

5.错误处理及日志

```
当Mojo发生异常时, 根据情况控制maven的运行状态。在代码中编写必要的日志以便为用户提供足够的信息
```

6.测试插件

```
编写自动化的测试代码测试行为, 然后再实际运行插件以验证其行为
```

### 13.2 案例

编写一个用于代码行统计的maven插件

1.创建maven项目，pom.xml文件如下

```xml
<parent>
    <artifactId>common-parent</artifactId>
    <groupId>tca</groupId>
    <version>1.0.0</version>
    <relativePath/>
</parent>
<modelVersion>4.0.0</modelVersion>

<artifactId>common-learning-maven-plugins-custom</artifactId>
<!-- 这里packaging必须是 maven-plugin -->
<packaging>maven-plugin</packaging>

<properties>
    <maven.version>3.0</maven.version>
</properties>

<dependencies>
    <!-- maven插件的核心api -->
    <dependency>
        <groupId>org.apache.maven</groupId>
        <artifactId>maven-plugin-api</artifactId>
        <version>${maven.version}</version>
    </dependency>
</dependencies>
```

2.编写自定义MOJO继承AbstractMojo

```java
/**
 * @goal count
 */
public class CountMojo extends AbstractMojo {

    /**
     * 这里需要采用java1.4的编码规范, 注解需要加在注释里, 使用了@parameter注解后, 就可以在配置插件的时候进行配置
     * @Parameter
     */
    private String[] includes;

    private static final String[] DEFAULT_INCLUDES = {"java", "xml", "properties"};

    /**
     * @parameter expression="${project.basedir}"
     * @required
     * @readonly
     */
    private File basedir;

    /**
     * @parameter expression="${project.build.sourceDirectory}"
     * @required
     * @readonly
     */
    private File sourceDirectory;

    /**
     * @parameter expression="${project.build.testSourceDirectory}"
     * @required
     * @readonly
     */
    private File testSourceDirectory;

    /**
     * @parameter expression="${project.build.resources}"
     * @required
     * @readonly
     */
    private List<Resource> resources;

    /**
     * @parameter expression="${project.build.testResources}"
     * @required
     * @readonly
     */
    private List<Resource> testResources;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (includes == null || includes.length == 0) {
            includes = DEFAULT_INCLUDES;
        }
        try {
            // 源码
            countDir(sourceDirectory);
            // 测试代码
            countDir(testSourceDirectory);

            // 配置文件
            for (Resource resource : resources) {
                countDir(new File(resource.getDirectory()));
            }

            for (Resource testResource : testResources) {
                countDir(new File(testResource.getDirectory()));
            }
        } catch (Exception e) {
            throw new MojoExecutionException("Unable to count lines of code", e);
        }


    }

    private void countDir(File file) throws Exception {
        if (!file.exists()) {
            return;
        }

        List<File> collected = new ArrayList<>();

        collectFiles(collected, file);

        int lines = 0;

        for (File sourceFile : collected) {
            lines += countLine(sourceFile);
        }

        String path = file.getAbsolutePath().substring(basedir.getAbsolutePath().length());

        getLog().info(path + ": " + lines + " lines of code in " + collected.size() + " files");
    }

    private int countLine(File file) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        int lines = 0;

        try {
            while (reader.ready()) {
                reader.readLine();
                lines++;
            }
        } catch (Exception e) {

        } finally {
            reader.close();
        }

        return lines;
    }

    private void collectFiles(List<File> files, File file) {
        if (file.isFile()) {
            for (String include : includes) {
                if (file.getName().endsWith("." + include)) {
                    files.add(file);
                    break;
                }
            }
        } else {
            for (File sub : file.listFiles()) {
                collectFiles(files, sub);
            }
        }
    }
}
```

注意

```
1.因为历史原因, 所以这里使用java1.4风格的标注(将标注写在注释上)
2.在类的注释上写上标注的@goal
3.在字段上使用@parameter等标注
4.编写完后, 执行 mvn clean install, 将当前插件安装到maven本地仓库
```

3.运行

```bash
mvn tca:common-learning-maven-custom-plugin:1.0.0:count
```

### 13.3 Mojo标注

mojo标注放在自定义Mojo类的注释上

#### @goal <name>

```
这是唯一必须声明的标注, 为目标(功能)
```

#### @phase <name>

```
默认将该目标绑定至Default生命周期的某个阶段, 这样在配置使用该插件目标的时候不需要声明phase
```

### 13.4 Mojo参数

mojo参数放在Mojo类内部的字段上

#### @parameter

```
将Mojo的某个字段标注为可配置的参数, 即为mojo参数
```

#### @parameter expression="${xxx}"

```
使用属性表达式对mojo参数进行赋值
```

#### @readonly

```
表示当前Mojo参数是只读的
```

#### @required

```
表示当前mojo是必需的, 如果使用了该注解, 但是没有默认值, 且用户没有配置, 那么会报错
```

### 13.5 错误处理

```
Mojo的execute方法中会抛出两类异常: MojoExcutionException 以及 MojoFailureException
MojoFailureException 一般表示发生了预期的异常, 控制台会输出 BUILD FAILURE;
MojoExecutionException 一般表示发生了未知错误, 控制台会输出 BUILD ERROR
```

### 13.6 日志处理

```
使用getLog()方法获取日志实例对象
```















