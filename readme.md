# quick start
方式一 在项目中引入common-parent作为父项目
step1 引入父项目
```xml
<parent>
    <groupId>tca</groupId>
    <artifactId>common-parent</artifactId>
    <version>1.0.0-RELEASE</version>
</parent>
```
step2 引入具体sdk, 默认会继承common-parent中的版本号
```xml
<dependency>
    <groupId>tca</groupId>
    <artifactId>common-web</artifactId>
</dependency>
```

方式二 在聚合项目中如果有特定的父项目, 则可以通过common-dependencies引入common中的所有依赖(只是版本号)
step1 父项目中引入
```xml
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>tca</groupId>
            <artifactId>common-dependencies</artifactId>
            <version>1.0.0-RELEASE</version>
            <scope>import</scope>
            <type>pom</type>
        </dependency>
    </dependencies>
</dependencyManagement>
```
step2 子项目中使用具体sdk, 不需要指定版本号, 默认会使用common-dependencies中引入的版本号
```xml
<dependency>
    <groupId>tca</groupId>
    <artifactId>common-web</artifactId>
</dependency>
```