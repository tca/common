package com.tca.common.custom.inject.config;

import com.tca.common.custom.inject.plugins.FactoryCodeBeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author zhoua
 * @date 2024/9/7 12:39
 */
@Configuration
@Import(value = {FactoryCodeBeanDefinitionRegistryPostProcessor.class})
public class CustomInjectAutoConfiguration {

}
