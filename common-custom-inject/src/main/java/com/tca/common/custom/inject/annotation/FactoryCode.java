package com.tca.common.custom.inject.annotation;

import java.lang.annotation.*;

/**
 * @author zhoua
 * @date 2024/9/7 12:15
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface FactoryCode {

    /**
     * 属性值factory_code
     * @return
     */
    String value() default "";

}
