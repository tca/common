package com.tca.common.core.bean;

/**
 * @author zhouan
 * @date 2022/2/14
 */
public interface IResultInfo {
    
    /**
     * 返回响应码
     * @return
     */
    Integer getCode();
    
    /**
     * 返回响应信息
     * @return
     */
    String getMsg();
}
