package com.tca.common.core.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author zhoua
 * @date 2022/2/14
 */
public enum DataScopeTypeEnum {
    
    /**
     * 全部数据权限
     */
    ALL(0),
    
    /**
     * 租户数据权限
     */
    TENANT(1),
    
    /**
     * 部门数据权限
     */
    DEPARTMENT(2);
    
    /**
     * code
     */
    private Integer code;
    
    private final static Map<Integer, DataScopeTypeEnum> CODE_MAP = new HashMap<>(4);
    
    static {
        for (DataScopeTypeEnum dataScopeTypeEnum : DataScopeTypeEnum.values()) {
            CODE_MAP.put(dataScopeTypeEnum.getCode(), dataScopeTypeEnum);
        }
    }
    
    DataScopeTypeEnum(Integer code) {
        this.code = code;
    }
    
    public static DataScopeTypeEnum getByCode(Integer code) {
        if (Objects.isNull(code)) {
            return null;
        }
        return CODE_MAP.get(code);
    }
    
    public Integer getCode() {
        return code;
    }
}
