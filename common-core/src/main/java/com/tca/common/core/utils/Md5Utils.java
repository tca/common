package com.tca.common.core.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/***
 * @author kevin
 * @date 2018/8/20
 */
public class Md5Utils {

	private final static Logger logger = Logger.getLogger(Md5Utils.class.getName());

	private Md5Utils() { }

    /**
     * 生成大写的MD5值
     * @param origin
     * @return
     */
    @Deprecated
    public static String encode(String origin) {
        char[] hexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        try {
            byte[] originBytes = origin.getBytes(StandardCharsets.UTF_8);
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(originBytes);
            byte[] digestedBytes = messageDigest.digest();
            char[] str = new char[digestedBytes.length * 2];
            int k = 0;
            byte[] var7 = digestedBytes;
            int var8 = digestedBytes.length;

            for(int var9 = 0; var9 < var8; ++var9) {
                byte b = var7[var9];
                str[k++] = hexDigits[b >>> 4 & 15];
                str[k++] = hexDigits[b & 15];
            }

            return new String(str);
        } catch (Exception var11) {
			logger.log(Level.SEVERE, var11.getMessage());
            return "";
        }
    }

    /**
     * 进行md5加密
     * @param source
     * @return
     */
    public static String encrypt(String source) {
        if (source == null) {
            source = "";
        }
        String result = "";

        result = encrypt(source, "MD5");
        return result;
    }

	/***
	 * Encrypt
	 *
	 * @param source
	 * @param algorithm
	 * @return
	 */
    private static String encrypt(String source, String algorithm) {
        byte[] resByteArray = new byte[0];
        try {
            resByteArray = encrypt(source.getBytes(StandardCharsets.UTF_8), algorithm);
        } catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
        }
        return toHexString(resByteArray);
    }

	/***
	 * Encrypt
	 *
	 * @param source
	 * @param algorithm
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
    private static byte[] encrypt(byte[] source, String algorithm)
            throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        md.reset();
        md.update(source);
        return md.digest();
    }

	/***
	 * toHexString
	 *
	 * @param res
	 * @return
	 */
    private static String toHexString(byte[] res) {
        StringBuffer sb = new StringBuffer(res.length << 1);
        for (int i = 0; i < res.length; i++) {
            String digit = Integer.toHexString(0xFF & res[i]);
            if (digit.length() == 1) {
                digit = '0' + digit;
            }
            sb.append(digit);
        }
        return sb.toString().toLowerCase();
    }

}
