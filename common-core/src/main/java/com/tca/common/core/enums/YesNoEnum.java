package com.tca.common.core.enums;

import java.io.Serializable;

/**
 * 是否状态枚举
 *
 * @author lebin
 * @date 2021/12/25
 */
public enum YesNoEnum implements Serializable {
	/***
	 * 是
	 */
    YES(1, "是"),
	/***
	 * 否
	 */
    NO(0, "否");

    private Integer key;
    private String name;

    YesNoEnum(Integer key, String name) {
        this.key = key;
        this.name = name;
    }

    public Integer getKey() {
        return key;
    }


    public String getName() {
        return name;
    }
}