package com.tca.common.core.bean;


import com.tca.common.core.enums.DataScopeTypeEnum;
import com.tca.common.core.exception.DataScopeException;
import com.tca.common.core.utils.ValidateUtils;

import java.util.List;
import java.util.Objects;

/**
 * @author zhoua
 * @date 2022/2/14
 */
public class DataScope {
    
    private DataScope() {}

    /**
     * 构造器
     * @param tenant
     * @param department
     * @param departmentList
     * @param dataScopeTypeEnum
     */
    private DataScope(String tenant, String department, List<String> departmentList, DataScopeTypeEnum dataScopeTypeEnum) {
        if (Objects.isNull(tenant) && dataScopeTypeEnum != DataScopeTypeEnum.ALL) {
            throw new DataScopeException("tenant can not be null if the dataScopeType is not ALL");
        }
        if (ValidateUtils.isEmpty(departmentList) && dataScopeTypeEnum == DataScopeTypeEnum.DEPARTMENT) {
            throw new DataScopeException("department can not be null if the dataScopeType is DEPARTMENT");
        }
        this.tenant = tenant;
        this.department = department;
        this.departmentList = departmentList;
        this.dataScopeTypeCode = dataScopeTypeEnum.getCode();
    }

    /**
     * 创建 all 级别数据权限
     * @param tenant
     * @param department
     * @return
     */
    public static DataScope newAllDataScope(String tenant, String department) {
        return new DataScope(tenant, department, null, DataScopeTypeEnum.ALL);
    }

    /**
     * 创建 tenant 级别数据权限
     * @param tenant
     * @param department
     * @return
     */
    public static DataScope newTenantDataScope(String tenant, String department) {
        return new DataScope(tenant, department, null, DataScopeTypeEnum.TENANT);
    }

    /**
     * 创建 department 级别数据权限
     * @param tenant
     * @param department
     * @param departmentList
     * @return
     */
    public static DataScope newDepartmentDataScope(String tenant, String department, List<String> departmentList) {
        return new DataScope(tenant, department, departmentList, DataScopeTypeEnum.DEPARTMENT);
    }
    
    /**
     * 当前用户所属租户
     */
    private String tenant;
    
    /**
     * 当前用户所属部门
     */
    private String department;
    
    /**
     * 当前用户拥有的部门数据权限
     */
    private List<String> departmentList;
    
    /**
     * 当前用户所属数据权限类别
     */
    private Integer dataScopeTypeCode;

    /**
     * 用户id
     */
    private Long userId;

	/**
	 * 用户名称
	 */
    private String username;

    public String getTenant() {
        return tenant;
    }
    
    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
    
    public String getDepartment() {
        return department;
    }
    
    public void setDepartment(String department) {
        this.department = department;
    }
    
    
    public Integer getDataScopeTypeCode() {
        return dataScopeTypeCode;
    }
    
    public void setDataScopeTypeCode(Integer dataScopeTypeCode) {
        this.dataScopeTypeCode = dataScopeTypeCode;
    }
    
    
    public List<String> getDepartmentList() {
        return departmentList;
    }
    
    public void setDepartmentList(List<String> departmentList) {
        this.departmentList = departmentList;
    }

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
