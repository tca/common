package com.tca.common.core.utils;


import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author kevin
 * @date 2017/12/14
 */
public final class AesUtils {

	/***
	 * CODER
	 */
    private final static String CODER = "AES";
	/***
	 * 算法
	 */
    private final static String ALGORITHM = "SHA1PRNG";
	/***
	 * Charset
	 */
    private final static String CHARSET = "utf-8";
	/***
	 * Digits
	 */
    private final static int DIGITS = 128;

    private final static Logger logger = Logger.getLogger(AesUtils.class.getName());

    /**
     * 加密字符串
     *
     * @param content 加密内容
     * @param password 秘钥
     * @return 返回值如果错误返回Null
     */
    public static String encrypt(String content, String password) {
        try {
            byte[] bytes = encrypt(content.getBytes(CHARSET), password);
            return Base64Utils.encode(bytes);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }

    /**
     * 解密
     *
     * @param content 解密内容
     * @param password 秘钥
     * @return 返回值如果错误返回Null
     */
    public static String decrypt(String content, String password) {
        try {
            byte[] bytes = decrypt(Base64Utils.decode(content), password);
            return new String(bytes, CHARSET);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }

    /**
     * AES加密字符串
     *
     * @param content 需要被加密的字符串
     * @param password 加密需要的密码
     * @return 密文
     */
    public static byte[] encrypt(byte[] content, String password) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance(CODER);
            SecureRandom random = SecureRandom.getInstance(ALGORITHM);
            random.setSeed(password.getBytes(CHARSET));
            kgen.init(DIGITS, random);

            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, CODER);
            Cipher cipher = Cipher.getInstance(CODER);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] result = cipher.doFinal(content);
            return result;
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }

    /**
     * 解密AES加密过的字符串
     *
     * @param content AES加密过过的内容
     * @param password 加密时的密码
     * @return 明文
     */
    public static byte[] decrypt(byte[] content, String password) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(CODER);
            SecureRandom random = SecureRandom.getInstance(ALGORITHM);
            random.setSeed(password.getBytes(CHARSET));
            keyGenerator.init(DIGITS, random);

            SecretKey secretKey = keyGenerator.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, CODER);
            Cipher cipher = Cipher.getInstance(CODER);
            cipher.init(Cipher.DECRYPT_MODE, key);
            return cipher.doFinal(content);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }
}
