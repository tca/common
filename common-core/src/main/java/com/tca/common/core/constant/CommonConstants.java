package com.tca.common.core.constant;

/**
 * @author zhoua
 * @date 2022/2/14
 */
public class CommonConstants {
    /**
     * 基础包
     */
	public final static String BASE_PACKAGES = "com.hfitech";
    /**
     * header 中租户，格式 字段名:ID集合（逗号分隔）
     */
	public final static String TENANT = "TENANT";
    /**
     * 企业ID header name
     */
	public final static String CLIENT_ENTERPRISE_ID = "client-enterprise-id";
    /**
     * user header name
     */
	public final static String CLIENT_USER = "client-user";
    /**
     * userid header name
     */
	public final static String CLIENT_USER_ID = "client-user-id";
    /**
     * username header name
     */
	public final static String CLIENT_USER_NAME = "client-user-name";
    /**
     * 产品ID header name
     */
	public final static String CLIENT_PRODUCT_ID = "client-product-id";
    /**
     * from header name
     */
	public final static String CLIENT_FROM = "client-from";
    /**
     * token header name
     */
	public final static String CLIENT_TOKEN = "client-token";

    /**
     * departmentId header name
     */
    public final static String CLIENT_DEPARTMENT_ID = "client-department-id";

    /**
     * operate departmentId header name
     */
    public final static String OPERATE_DEPARTMENT_ID = "operate-department-id";

    /**
     * 数据权限 header name
     */
    public final static String DATA_SCOPE = "data-scope";

}
