//package com.tca.common.core.utils;
//
//
//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.MapperFeature;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
//import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
//import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
//import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
//import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
//import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
//import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
//import com.sun.xml.internal.ws.developer.SerializationFeature;
//import lombok.extern.slf4j.Slf4j;
//import org.slf4j.LoggerFactory;
//
//import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
//import java.time.format.DateTimeFormatter;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Logger;
//
///**
// * jackson 工具类
// *
// * @author: xudawei
// * @date: 2022/5/26
// */
//@Slf4j
//public class JsonUtils {
//
////	private static final Logger log = LoggerFactory.getLogger(JsonUtils.class);
//
//	private static final String STANDARD_PATTERN = "yyyy-MM-dd HH:mm:ss";
//	private static final String DATE_PATTERN = "yyyy-MM-dd";
//	private static final String TIME_PATTERN = "HH:mm:ss";
//
//	/**
//	 * 配置ObjectMapper对象
//	 */
//	private static ObjectMapper objectMapper = new ObjectMapper();
//
//	/**
//	 * 忽略注解配置的ObjectMapper对象
//	 * 会忽略 @JsonProperty @JsonIgnore 等注解
//	 */
//	private static ObjectMapper noAnnoObjectMapper = new ObjectMapper();
//
//	static {
//		// 初始化JavaTimeModule
//		JavaTimeModule javaTimeModule = new JavaTimeModule();
//
//		// 处理LocalDateTime
//		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(STANDARD_PATTERN);
//		javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(dateTimeFormatter));
//		javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(dateTimeFormatter));
//
//		// 处理LocalDate
//		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
//		javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(dateFormatter));
//		javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer(dateFormatter));
//
//		// 处理LocalTime
//		DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(TIME_PATTERN);
//		javaTimeModule.addSerializer(LocalTime.class, new LocalTimeSerializer(timeFormatter));
//		javaTimeModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer(timeFormatter));
//
//		objectMapper.setDateFormat(new SimpleDateFormat(STANDARD_PATTERN));
//		// null的属性不进行序列化
//		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
//		// 禁用 序列化日期时以timestamps输出
//		objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//		objectMapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
//		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//		objectMapper.registerModule(javaTimeModule);
//
//		noAnnoObjectMapper.setDateFormat(new SimpleDateFormat(STANDARD_PATTERN));
//		// null的属性不进行序列化
//		noAnnoObjectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
//		// 禁用 序列化日期时以timestamps输出
//		noAnnoObjectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//		noAnnoObjectMapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
//		noAnnoObjectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		noAnnoObjectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//		noAnnoObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
//		noAnnoObjectMapper.registerModule(javaTimeModule);
//
//
//	}
//
//	public static ObjectMapper getObjectMapper() {
//		return objectMapper;
//	}
//	/**
//	 * 对象转为Json字符串
//	 *
//	 * @param object
//	 * @return
//	 */
//	public static String toJSONString(Object object) {
//		return toJSONString(object, false);
//	}
//
//	/**
//	 * 对象转为Json字符串
//	 *
//	 * @param object
//	 * @param disableAnnotations true: 禁用@JsonProperty @JsonIgnore等注解 <br />
//	 *                           false: 启用注解
//	 * @return
//	 */
//	public static String toJSONString(Object object, boolean disableAnnotations) {
//		try {
//			if (disableAnnotations) {
//				return noAnnoObjectMapper.writeValueAsString(object);
//			}
//			return objectMapper.writeValueAsString(object);
//		} catch (Exception e) {
//			log.error("toJSONString exception", e);
//		}
//		return null;
//	}
//
//	/**
//	 * Json字符串转为对象
//	 *
//	 * @param content
//	 * @param valueType
//	 * @param <T>
//	 * @return
//	 */
//	public static <T> T parseObject(String content, Class<T> valueType) {
//		try {
//			return objectMapper.readValue(content, valueType);
//		} catch (Exception e) {
//			log.error("parseObject exception", e);
//		}
//		return null;
//	}
//
//	/**
//	 * Json字符串转为List对象
//	 *
//	 * @param content
//	 * @param valueType
//	 * @param <T>
//	 * @return
//	 */
//	public static <T> List<T> parseArray(String content, Class<T> valueType) {
//		try {
//			return objectMapper.readValue(content, objectMapper.getTypeFactory().constructCollectionType(List.class, valueType));
//		} catch (Exception e) {
//			log.error("parseArray exception", e);
//		}
//		return null;
//	}
//
//	/**
//	 * Json字符串转为Map对象
//	 *
//	 * @param content
//	 * @param kClass
//	 * @param vClass
//	 * @param <K>
//	 * @param <V>
//	 * @return
//	 */
//	public static <K, V> Map<K, V> parseMap(String content, Class<K> kClass, Class<V> vClass) {
//		try {
//			return objectMapper.readValue(content, objectMapper.getTypeFactory().constructMapType(Map.class, kClass, vClass));
//		} catch (Exception e) {
//			log.error("parseMap exception：", e);
//		}
//		return null;
//	}
//}
