package com.tca.common.core.datapermission;


import com.tca.common.core.bean.DataScope;

/**
 * @author zhoua
 * @date 2022/2/14
 */
public interface DataPermissionContextHolder {
    
    /**
     * 设置数据权限
     * @param dataScope
     */
    void setDataScope(DataScope dataScope);
    
    /**
     * 获取数据权限
     * @return
     */
    DataScope getDataScope();
    
    /**
     * 清除数据权限
     */
    void clearDataScope();
    
}
