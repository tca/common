package com.tca.common.core.enums;

import java.io.Serializable;

/**
 * 开启状态枚举
 *
 * @author lebin
 * @date 2021/12/25
 */
public enum StatusEnum implements Serializable {
	/***
	 * 启用
	 */
    ACTIVE(1, "启用"),
	/***
	 * 禁用
	 */
    DISUSE(0, "禁用");

    private Integer key;
    private String name;

    StatusEnum(Integer key, String name) {
        this.key = key;
        this.name = name;
    }

    /**
     * 根据值取枚举
     *
     * @param key 枚举值
     * @return 枚举对象
     */
    public static String getNameByKey(Integer key) {
        if (key == null) {
            return "";
        }
        for (StatusEnum type : StatusEnum.values()) {
            if (key.equals(type.getKey())) {
                return type.getName();
            }
        }
        return "";
    }


    public Integer getKey() {
        return key;
    }


    public String getName() {
        return name;
    }
}