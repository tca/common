package com.tca.common.core.exception;

/**
 * @author zhoua
 * @date 2022/2/14
 */
public class DataScopeException extends RuntimeException {
    
    public DataScopeException() {}
    
    public DataScopeException(String message) {
        super(message);
    }
    
    public DataScopeException(Integer errorCode, String message, Throwable cause) {
        super(message, cause);
    }
}
