package com.tca.common.web.handler;

import com.tca.common.web.bean.LogRecordBean;

/**
 * @author zhoua
 * @date 2022/1/12 21:30
 */
public interface LogRecordListener {

    /**
     * 处理日志
     * @param logRecordBean
     */
    void onLogRecordEvent(LogRecordBean logRecordBean);

    /**
     * 真正处理日志的方法
     * @param logRecordBean
     */
    void doHandle(LogRecordBean logRecordBean);



}
