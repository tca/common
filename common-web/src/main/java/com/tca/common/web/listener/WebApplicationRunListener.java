package com.tca.common.web.listener;

import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;

/**
 * @author zhoua
 * @date 2025/1/11 16:24
 */
public class WebApplicationRunListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent>, Ordered {

    private static final Integer ORDER = HIGHEST_PRECEDENCE + 2;

    private static final String SERVER_PORT = "server.port";

    private static final String DEFAULT_SERVER_PORT = "8080";

    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
    }

    @Override
    public int getOrder() {
        return ORDER;
    }

}
