package com.tca.common.web.datapermission;


import com.alibaba.ttl.TransmittableThreadLocal;
import com.tca.common.core.bean.DataScope;
import com.tca.common.core.datapermission.DataPermissionContextHolder;

/**
 * @author zhoua
 * @date 2022/2/14
 */
public class ThreadLocalDataPermissionContextHolder implements DataPermissionContextHolder {
    
    private final ThreadLocal<DataScope> HOLDER = new TransmittableThreadLocal<>();
    
    @Override
    public void setDataScope(DataScope dataScope) {
        HOLDER.set(dataScope);
    }
    
    @Override
    public DataScope getDataScope() {
        return HOLDER.get();
    }
    
    @Override
    public void clearDataScope() {
        HOLDER.remove();
    }
}
