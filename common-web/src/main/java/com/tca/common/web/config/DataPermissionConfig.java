package com.tca.common.web.config;

import com.tca.common.core.datapermission.DataPermissionContextHolder;
import com.tca.common.web.datapermission.ThreadLocalDataPermissionContextHolder;
import com.tca.common.web.filter.DataPermissionContextHolderFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhoua
 * @date 2022/2/19
 */
@Configuration
@ConditionalOnProperty(name = "tca.data.permission.enabled", havingValue = "true", matchIfMissing = true)
public class DataPermissionConfig {

	@Bean
	@ConditionalOnMissingBean(DataPermissionContextHolder.class)
	public DataPermissionContextHolder dataPermissionContextHolder() {
		return new ThreadLocalDataPermissionContextHolder();
	}
	
	/**
	 * 基于 DataPermissionContextHolder 的数据权限拦截器
	 * @return
	 */
	@Bean
	public DataPermissionContextHolderFilter dataPermissionContextHolderFilter(DataPermissionContextHolder dataPermissionContextHolder) {
		return new DataPermissionContextHolderFilter(dataPermissionContextHolder);
	}
}
