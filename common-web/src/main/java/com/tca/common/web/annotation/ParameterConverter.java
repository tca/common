package com.tca.common.web.annotation;


import com.tca.common.web.converter.InputParamConverter;

import java.lang.annotation.*;

/**
 * @author zhoua
 * @date 2022/1/13
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ParameterConverter {

    /**
     * 入参转化器
     *
     * @return
     */
    Class<? extends InputParamConverter>[] value() default {};
    
}
