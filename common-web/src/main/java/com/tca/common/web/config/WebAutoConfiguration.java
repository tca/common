package com.tca.common.web.config;

import com.tca.common.web.aspect.LogRecordAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;

/**
 * @author zhoua
 * @date 2022/1/12 21:03
 */
@Configuration
@Import(value = {WebMvcConfig.class,
        DataPermissionConfig.class,
        ExceptionHandlerAdviceConfig.class,
        LogRecordListenerExecutorConfig.class,
        LogRecordAspect.class,
        CorsConfig.class})
public class WebAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(ParameterNameDiscoverer.class)
    public ParameterNameDiscoverer parameterNameDiscoverer() {
        return new DefaultParameterNameDiscoverer();
    }

}
