package com.tca.common.web.properties;

import com.google.common.collect.Lists;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

import java.util.Collections;
import java.util.List;

/**
 * @author zhoua
 * @date 2025/1/24 23:16
 */
@Data
@ConfigurationProperties(prefix = CorsProperties.PREFIX)
public class CorsProperties {

    public static final String PREFIX = "tca.web.cors";

    /**
     * 是否开启跨域配置, 默认关闭
     */
    private boolean enabled = false;

    /**
     * Cors过滤的路径, 默认 /**
     */
    private String path = "/**";

    /**
     * 允许访问的源, 默认 "*"
     */
    private List<String> allowedOrigins = Collections.singletonList(CorsConfiguration.ALL);

    /**
     * 允许访问的请求头, 默认 "*"
     */
    private List<String> allowedHeaders = Collections.singletonList(CorsConfiguration.ALL);

    /**
     * 允许发送cookie, 默认允许
     */
    private boolean allowedCredentials = true;

    /**
     * 允许的请求方式, 默认 "*"
     */
    private List<String> allowedMethods = Collections.singletonList(CorsConfiguration.ALL);

    /**
     * 允许的响应头, 这里不能使用 "*", 会抛异常
     */
    private List<String> exposedHeaders = Lists.newArrayList("Content-Type",
            "X-Requested-With", "accept", "Origin", "Access-Control-Request-method");

    /**
     * 该响应的有效时间为默认30min, 在有效时间内, 浏览器无需为同一请求再次发起预检请求
     */
    private Long maxAge = 1800L;

}
