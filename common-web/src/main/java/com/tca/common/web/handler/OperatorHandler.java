package com.tca.common.web.handler;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhoua
 * @date 2022/1/12 21:30
 */
public interface OperatorHandler {

    /**
     * 获取用户名
     * @param request
     * @return
     */
    String getAccountName(HttpServletRequest request);

    /**
     * 账号id
     * @param request
     * @return
     */
    Long getAccountId(HttpServletRequest request);

    /**
     * 租户名称
     * @param request
     * @return
     */
    String getTenantName(HttpServletRequest request);

    /**
     * 租户id
     * @param request
     * @return
     */
    Long getTenantId(HttpServletRequest request);

    /**
     * 部门id
     * @param request
     * @return
     */
    Long getDepartmentId(HttpServletRequest request);
}
