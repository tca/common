package com.tca.common.web.annotation;

import com.tca.common.web.converter.OutputParamConverter;
import com.tca.common.web.enums.LogStorageTypeEnum;

import java.lang.annotation.*;

/**
 * @author zhoua
 * @date 2022/1/12
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface LogRecord {
    
    /**
     * 模块名称
     *
     * @return
     */
    String module() default "";
    
    /**
     * 操作
     *
     * @return
     */
    String operation() default "";
    
    /**
     * 入参
     *
     * @return
     */
    boolean inputParam() default true;
    
    /**
     * 出参
     *
     * @return
     */
    boolean outputParam() default true;
    
    /**
     * 出参转化器
     *
     * @return
     */
    Class<OutputParamConverter>[] outputParamConverters() default {};

	/**
	 * 日志存储类型
	 *
	 * @return
	 */
	LogStorageTypeEnum storageType() default LogStorageTypeEnum.DATABASE;
}
