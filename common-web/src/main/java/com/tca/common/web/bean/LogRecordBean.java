package com.tca.common.web.bean;

import com.tca.common.web.enums.LogStorageTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author zhoua
 * @date 2022/1/12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogRecordBean {
    
    /**
     * 模块名称
     */
    private String module;
    
    /**
     * 日志类型
     */
    private int type;
    
    /**
     * 操作
     */
    private String operation;
    
    /**
     * 操作时间
     */
    private String operationTime;
    
    /**
     * 操作时间戳
     */
    private Long operationTimestamp;
    
    /**
     * 请求url
     */
    private String uri;
    
    /**
     * 方法调用
     */
    private String method;
    
    /**
     * 请求方法 (get/post/put/delete)
     */
    private String methodType;
    
    /**
     * 入参
     */
    private String inputParam;
    
    /**
     * 出参
     */
    private String outputParam;
    
    /**
     * 消耗时间
     */
    private Long timeConsume;
    
    /**
     * 响应code
     */
    private Integer code;
    
    /**
     * 响应message
     */
    private String message;
    
    /**
     * 操作ip
     */
    private String ip;
    
    /**
     * 用户名
     */
    private String accountName;

	/**
	 * 账号id
	 */
	private Long accountId;
    
    /**
     * 租户id
     */
    private Long tenantId;
    
    /**
     * 部门id
     */
    private Long departmentId;
    
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

	/**
	 * 日志存储类型
	 */
	private LogStorageTypeEnum storageType;
}
