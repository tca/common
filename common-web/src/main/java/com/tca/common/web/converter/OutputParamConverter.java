package com.tca.common.web.converter;

import com.tca.common.core.bean.Result;

import java.util.function.UnaryOperator;

/**
 * @author zhoua
 * @date 2022/1/13
 */
public interface OutputParamConverter extends UnaryOperator<Result> {

}
