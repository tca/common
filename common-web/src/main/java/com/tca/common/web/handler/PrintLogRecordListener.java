package com.tca.common.web.handler;

import com.tca.common.web.bean.LogRecordBean;
import com.tca.common.web.config.LogRecordListenerExecutorConfig;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zhoua
 * @date 2022/1/12 23:52
 */
@Slf4j
public class PrintLogRecordListener extends AbstractLogRecordListener{


    public PrintLogRecordListener(LogRecordListenerExecutorConfig logRecordListenerExecutorConfig) {
        super(logRecordListenerExecutorConfig);
    }

    @Override
    public void doHandle(LogRecordBean logRecordBean) {
        log.info("操作日志: {}", logRecordBean);
    }
}
