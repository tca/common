package com.tca.common.web.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhoua
 * @date 2022/1/12
 */
@ConfigurationProperties(prefix = "tca.log.executor")
@Data
public class LogRecordListenerExecutorConfig {
    
    /**
     * 核心线程数
     */
    private int core = 1;
    
    /**
     * 最大线程数
     */
    private int max = 1;
    
    /**
     * 闲置时间
     */
    private Long keepAliveTime = 0L;
    
}
