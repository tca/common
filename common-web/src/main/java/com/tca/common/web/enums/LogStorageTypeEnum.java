package com.tca.common.web.enums;

/**
 * 日志存储类型
 *
 * @author: xudawei
 * @date: 2022/5/23
 */
public enum LogStorageTypeEnum {

    /**
     * 全部
     */
    ALL(),

    /**
     * 存储于数据库
     */
    DATABASE(),

    /**
     * 存储于文件 ./logs/data-points.log
     */
    FILE(),

    /**
     * 自定义
     */
	CUSTOM();

}
