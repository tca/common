package com.tca.common.web.converter;

import java.util.function.UnaryOperator;

/**
 * @author zhoua
 * @date 2022/1/12
 * 日志参数转换器
 */
public interface InputParamConverter<T> extends UnaryOperator<T> {

}
