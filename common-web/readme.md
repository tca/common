#  1.springboot中配置自定义mvc组件（如拦截器、视图等）的方式
### 答案
实现
```
自定义类，extends WebMvcConfigurationSupport，添加@Configuration注解，此时使用的默认组件是 WebMvcConfigurationSupport 配置的组件，没有使用springboot中的自动配置类
WebMvcAutoConfiguration 中使用的mvc组件
```
原理
```
1.在springmvc-web.xml项目中，我们需要在springmvc-config.xml文件中配置组件，如添加：
<!-- 静态资源处理器 -->
<mvc:default-servlet-handler/>
<!-- 配置注解驱动 -->
<mvc:annotation-driven/>

2.在 springmvc-annotation 项目中，我们可以使用 @EnableWebMvc 注解，该注解的原理是向容器中导入 DelegatingWebMvcConfiguration，DelegatingWebMvcConfiguration 上使用了 @Configuration 注解，且继承了 WebMvcConfigurationSupport，WebMvcConfigurationSupport 向容器中注册相关springmvc组件
所以，如果我们需要自定义组件，可以继承 WebMvcConfigurationSupport，复写相关方法，并使用@Configuration

3.在 springboot 项目中，通过SPI机制，在spring.factories配置文件中配置了 WebMvcAutoConfiguration，WebMvcAutoConfiguration 向容器中注册相关组件，且 
WebMvcAutoConfiguration上使用了@ConditionalOnMissingBean(WebMvcConfigurationSupport.class) 注解，即没有配置WebMvcConfigurationSupport 的相关bean时，WebMvcAutoConfiguration 才会生效。
所以，当我们使用springboot时，默认会使用 WebMvcAutoConfiguration，或者像springmvc-annotation一样，自定义 WebMvcConfigurationSupport 子类
```
[参考1](https://charming.blog.csdn.net/article/details/84636521?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.opensearchhbase&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.opensearchhbase)

[EnableWebMvc、WebMvcConfigurationSupport和WebMvcConfigurationAdapter的使用](https://blog.csdn.net/pinebud55/article/details/53420481)

# 2.跨域问题
### 问题1
```
什么是跨域问题？
```

#### 答案
###### 浏览器的同源策略
```
同源策略（Sameoriginpolicy）是一种约定，它是浏览器最核心也最基本的安全功能，如果缺少了同源策略，则浏览器的正常功能可能都会受到影响。可以说Web是构建在同源策略基础之上的，浏览器只是针对同源策略的一种实现。
同源策略会阻止一个域的javascript脚本和另外一个域的内容进行交互。所谓同源（即指在同一个域）就是具有相同的协议（protocol），主机（host）和端口号（port）
```
###### 跨域问题的原因
```
要同时满足三个条件才会产生跨域问题：
1、浏览器限制，而不是服务端限制，可以查看Network，请求能够正确响应，response返回的值也是正确的
2、请求地址的协议、域名或端口和当前访问的协议、域名或端口不一样
3、发送的是XHR（XMLHttpRequest）请求（
在浏览器中，<script>、<img>、<iframe>、<link>等标签都可以加载跨域资源，而不受同源限制）

注意： 
不允许跨域访问并非是浏览器限制了发起跨站请求，而是跨站请求可以正常发起，但是返回结果被浏览器拦截了。但有些浏览器不允许从HTTPS跨域访问HTTP，比如Chrome和Firefox，这些浏览器在请求还未发出的时候就会拦截请求,这是特例。
```

### 问题2
```
跨域问题如何解决？
```
#### 答案
```
方案一：客户端浏览器解除跨域限制（不推荐）
方案二：发送jsonp请求替代XHR请求（不推荐）
方案三：修改服务器端（包括Http服务器和应用服务器）【推荐】
```
[参考1](https://zhuanlan.zhihu.com/p/66484450)
[参考2](https://zhuanlan.zhihu.com/p/145837536)

### 问题3
```
如何判断接口是否支持跨域
```
#### 答案
[参考](https://blog.csdn.net/zhishidi/article/details/119342198)

### 问题3
```
csrf攻击的原理
```
#### 答案
[参考](https://www.cnblogs.com/hyddd/archive/2009/04/09/1432744.html)


### 问题4
```
后端csrf攻击解决方案
```
#### 答案
spring-security处理csrf攻击
[参考](https://www.jianshu.com/p/969784677864)



### 问题5
```
OPTION请求的作用是什么
```