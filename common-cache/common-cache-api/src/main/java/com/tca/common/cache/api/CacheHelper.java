package com.tca.common.cache.api;

import java.util.concurrent.TimeUnit;

/**
 * @author zhoua
 * @date 2022/1/10 16:53
 */
public interface CacheHelper {

    /**
     * set
     * @param key key
     * @param value
     */
    void set(String key, Object value);

    /**
     * set + time
     * @param key
     * @param value
     * @param time
     * @param timeUnit
     */
    void set(String key, Object value, long time, TimeUnit timeUnit);

    /**
     * get
     * @param key
     * @return
     */
    String get(String key);

    /**
     * get + T
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T get(String key, Class<T> clazz);

    /**
     * list lpush
     * @param key
     * @param value
     * @return
     */
    Long leftPush(String key, Object value);

    /**
     * list rpush
     * @param key
     * @param value
     * @return
     */
    Long rightPush(String key, Object value);


    /**
     * list rpop
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T rightPop(String key, Class<T> clazz);

    /**
     * list rpop string
     * @param key
     * @return
     */
    String rightPop(String key);

    /**
     * list lpop
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T leftPop(String key, Class<T> clazz);

    /**
     * lpop string
     * @param key
     * @return
     */
    String leftPop(String key);

    /***
     * list size
     * @param key
     * @return
     */
    Long getListLength(String key);

    /**
     * delete
     * @param key
     * @return
     */
    Boolean delete(String key);

}
