package com.tca.common.cache.test.controller;

import com.alibaba.fastjson.JSONObject;
import com.tca.common.cache.api.CacheHelper;
import com.tca.common.cache.test.req.CacheRedisReq;
import com.tca.common.core.utils.ValidateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhouan
 * @Date 2020/11/23
 */
@RestController
@RequestMapping("/cache/redis")
public class CacheRedisController {

    @Autowired
    private CacheHelper cacheHelper;

    @PostMapping("/set")
    public String set(@RequestBody CacheRedisReq cacheRedisReq) {
        cacheHelper.set(cacheRedisReq.getKey(), cacheRedisReq.getValue());
        return "ok";
    }

    @GetMapping("/{key}")
    public String get(@PathVariable(value = "key") String key) {
        return cacheHelper.get(key);
    }

    @PostMapping("/lpush")
    public String lpush() {
        for (int i = 0; i < 10; i++) {
            cacheHelper.leftPush("index:integer", i);
        }
        return "ok";
    }

    @GetMapping("lpop")
    public String lpop() {
        return cacheHelper.leftPop("index:integer");
    }

    @GetMapping("/cacheable/{id}")
    @Cacheable(cacheNames = "player", key = "#id")
    public String cacheable(@PathVariable Integer id) {
        if (id == 10) {
            System.out.println("read from code...");
            return "Messi";
        }
        if (id == 7) {
            System.out.println("read from code...");
            return "Ronaldo";
        }

        return "null";
    }

    @GetMapping("/fromCache/{id}")
    public String fromCache(@PathVariable Integer id) {
        String result = cacheHelper.get("player::" + id);
        if (ValidateUtils.isNotEmpty(result)) {
            return result;
        }
        return "null";

    }

    @GetMapping("/cacheableObj/{id}")
    @Cacheable(cacheNames = "player::info", key = "#id")
    public JSONObject cacheableObj(@PathVariable Integer id) {
        JSONObject result = new JSONObject();
        if (id == 10) {
            result.put("name", "Messi");
            result.put("age", "35");
            System.out.println("read from code...");
            return result;
        }
        if (id == 7) {
            result.put("name", "Ronaldo");
            result.put("age", "37");
            System.out.println("read from code...");
            return result;
        }
        return result;
    }
}
