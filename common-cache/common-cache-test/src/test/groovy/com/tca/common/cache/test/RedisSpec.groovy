package com.tca.common.cache.test


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.StringRedisTemplate
import spock.lang.Specification
/**
 * @author zhoua
 * @date 2025/1/7 22:01
 */
@SpringBootTest(classes = CacheTestApplication.class)
class RedisSpec extends Specification{

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    def testAdd() {
        given:
        def a = 1
        def b = 1

        when:
        def c = a + b

        then:
        c == 2

    }



}
