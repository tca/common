package org.redisson.config;

/**
 * @author zhoua
 * @date 2022/12/16 17:21
 */
public enum ReadMode {

    /**
     * Read from slave nodes
     */
    SLAVE,

    /**
     * Read from master node
     */
    MASTER,

    /**
     * Read from master and slave nodes
     */
    MASTER_SLAVE,
}
