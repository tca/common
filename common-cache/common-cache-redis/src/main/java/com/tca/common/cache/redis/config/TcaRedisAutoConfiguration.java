package com.tca.common.cache.redis.config;

import com.tca.common.cache.api.CacheHelper;
import com.tca.common.cache.redis.bean.RedisCacheHelper;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;

/**
 * @author zhoua
 * @date 2022/1/10 16:57
 */
@Configuration
@EnableCaching
@AutoConfigureBefore(RedisAutoConfiguration.class)
public class TcaRedisAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(CacheHelper.class)
    public RedisCacheHelper redisCacheHelper(StringRedisTemplate stringRedisTemplate){
        return new RedisCacheHelper(stringRedisTemplate);
    }

    @Bean
    @ConditionalOnMissingBean(CacheManager.class)
    public CacheManager redisCacheManager(RedisConnectionFactory redisConnectionFactory) {
        RedisCacheWriter redisCacheWriter = RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory);
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig();
        // 设置key的序列化, key --> string
        redisCacheConfiguration.serializeKeysWith(RedisSerializationContext.SerializationPair
                .fromSerializer(new StringRedisSerializer()));
        // 设置value的序列化, value --> jdk序列化
        redisCacheConfiguration.serializeValuesWith(RedisSerializationContext.SerializationPair
                .fromSerializer(new JdkSerializationRedisSerializer()));
        // 默认缓存时间
        // 踩坑, 这里需要重新赋值
        // 【参考】(http://www.manongjc.com/detail/9-tmhufzosekablpw.html)
        redisCacheConfiguration = redisCacheConfiguration.entryTtl(Duration.ofHours(2L));
        return new RedisCacheManager(redisCacheWriter, redisCacheConfiguration);
    }



}
