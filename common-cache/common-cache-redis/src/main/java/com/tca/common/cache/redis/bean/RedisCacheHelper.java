package com.tca.common.cache.redis.bean;

import com.alibaba.fastjson.JSONObject;
import com.tca.common.cache.api.CacheHelper;
import com.tca.common.core.utils.ValidateUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @author zhoua
 * @date 2022/1/10 16:57
 */
@AllArgsConstructor
@Data
public class RedisCacheHelper implements CacheHelper {

    private StringRedisTemplate stringRedisTemplate;

    /**
     * set string value
     * @param key
     * @param value
     */
    private void setString(String key, String value) {
        stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * set string value
     * @param key
     * @param value
     * @param time
     * @param timeUnit
     */
    private void setString(String key, String value, long time, TimeUnit timeUnit) {
        stringRedisTemplate.opsForValue().set(key, value, time, timeUnit);
    }

    /**
     * set
     * @param key
     * @param value
     */
    @Override
    public void set(String key, Object value) {
        if (ValidateUtils.isEmpty(key) || ValidateUtils.isEmpty(value)) {
            return;
        }
        if (value instanceof String) {
            setString(key, (String)value);
            return;
        }
        setString(key, JSONObject.toJSONString(value));
    }

    /**
     * set
     * @param key
     * @param value
     * @param time
     * @param timeUnit
     */
    @Override
    public void set(String key, Object value, long time, TimeUnit timeUnit) {
        if (ValidateUtils.isEmpty(key) || ValidateUtils.isEmpty(value)) {
            return;
        }
        if (value instanceof String) {
            setString(key, (String)value, time, timeUnit);
            return;
        }
        setString(key, JSONObject.toJSONString(value), time, timeUnit);
    }

    /**
     * get
     * @param key
     * @param <T>
     * @return
     */
    @Override
    public <T> T get(String key, Class<T> clazz) {
        String value = stringRedisTemplate.opsForValue().get(key);
        if (value != null && clazz != null) {
            return JSONObject.parseObject(value, clazz);
        }
        return null;
    }

    /**
     * 取值
     * @param key
     * @return
     */
    @Override
    public String get(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }


    /**
     * list lpush
     * @param key
     * @param value
     * @return
     */
    @Override
    public Long leftPush(String key, Object value) {
        if (ValidateUtils.isEmpty(key) || ValidateUtils.isEmpty(value)) {
            return null;
        }
        String cacheValue;
        if (value instanceof String) {
            cacheValue = value.toString();
        } else {
            cacheValue = JSONObject.toJSONString(value);
        }
        return stringRedisTemplate.opsForList().leftPush(key, cacheValue);
    }

    /**
     * list rpush
     * @param key
     * @param value
     * @return
     */
    @Override
    public Long rightPush(String key, Object value) {
        if (ValidateUtils.isEmpty(key) || ValidateUtils.isEmpty(value)) {
            return null;
        }
        String cacheValue;
        if (value instanceof String) {
            cacheValue = value.toString();
        } else {
            cacheValue = JSONObject.toJSONString(value);
        }
        return stringRedisTemplate.opsForList().rightPush(key, cacheValue);
    }

    /**
     * list rpop
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    @Override
    public <T> T rightPop(String key, Class<T> clazz) {
        if (ValidateUtils.isEmpty(key)) {
            return null;
        }
        String value = stringRedisTemplate.opsForList().rightPop(key);
        if (ValidateUtils.isEmpty(value)) {
            return null;
        }
        if (clazz == String.class) {
            return (T)value;
        }
        return JSONObject.parseObject(value, clazz);
    }

    /**
     * list rpop string
     * @param key
     * @return
     */
    @Override
    public String rightPop(String key) {
        if (ValidateUtils.isEmpty(key)) {
            return null;
        }
        return stringRedisTemplate.opsForList().rightPop(key);
    }

    /**
     * list lpop
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    @Override
    public <T> T leftPop(String key, Class<T> clazz) {
        if (ValidateUtils.isEmpty(key)) {
            return null;
        }
        String value = stringRedisTemplate.opsForList().leftPop(key);
        if (ValidateUtils.isEmpty(value)) {
            return null;
        }
        if (clazz == String.class) {
            return (T)value;
        }
        return JSONObject.parseObject(value, clazz);
    }

    /**
     * lpop string
     * @param key
     * @return
     */
    @Override
    public String leftPop(String key) {
        if (ValidateUtils.isEmpty(key)) {
            return null;
        }
        return stringRedisTemplate.opsForList().leftPop(key);
    }

    /***
     * list size
     * @param key
     * @return
     */
    @Override
    public Long getListLength(String key) {
        if (ValidateUtils.isEmpty(key)) {
            return 0L;
        }

        return stringRedisTemplate.opsForList().size(key);
    }

    @Override
    public Boolean delete(String key) {
        return stringRedisTemplate.delete(key);
    }

}
