# 简介
common-dependencies用于集中管理和约束common应用中常用依赖的版本。

# 与common-parent对比
common-parent与common-dependencies之间的关系类似于spring-boot-starter-parent和spring-boot-dependencies的关系
* 关系:类似于spring-boot-starter-parent与spring-boot-dependencies, 我们先简述二者的关系
* 简述:spring-boot-starter-parent适合作为自定义项目中的父POM, 它继承了spring-boot-dependencies, 并在此基础上做了更多的配置和优化。 
  它包含了Spring Boot的一些默认设置和最佳实践, 适用于大多数的Spring Boot项目
* 默认配置:spring-boot-starter-parent提供了默认的maven配置, 如插件版本、编码、构建设置等。它为项目提供了开箱即用的默认配置, 减少手动配置的
  麻烦
* 插件管理:spring-boot-starter-parent包含了常见的构建插件配置, 如spring-boot-maven-plugin, 用于构建Spring Boot项目为可执行的
  jar或war文件, 以及其他常用的插件配置


