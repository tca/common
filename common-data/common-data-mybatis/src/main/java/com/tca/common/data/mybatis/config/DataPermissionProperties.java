package com.tca.common.data.mybatis.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhoua
 * @date 2022/2/11 14:39
 */
@Data
@ConfigurationProperties(prefix = "tca.data.permission")
public class DataPermissionProperties {
    
    /**
     * 数据权限
     */
    private boolean enabled = true;
    
    /**
     * 租户列名称
     */
    private String tenantColumn = "enterprise_id";
    
    /**
     * 部门列名称
     */
    private String departmentColumn = "department_id";
    
    /**
     * 忽略的表
     */
    private List<String> ignoreTables = new ArrayList<>();
    
    /**
     * 不做租户验证的方法
     */
    private List<String> ignoreMethods = new ArrayList<>();
    
    /**
     * 不做验证的方法 名称表达式
     */
    private List<String> ignoreMethodPatterns = new ArrayList<>();

}
