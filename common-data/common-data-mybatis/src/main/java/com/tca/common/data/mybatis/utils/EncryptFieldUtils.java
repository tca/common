package com.tca.common.data.mybatis.utils;


import com.tca.common.core.utils.AesUtils;

/**
 * 加解密接口
 *
 * @author chenhaiyang
 */
public class EncryptFieldUtils {

    private static final String PASSWORD = "hfepay@123";

    /**
     * 对字符串进行加密存储
     *
     * @param src 源
     * @return 返回加密后的密文
     * @throws RuntimeException 算法异常
     */
    public static String encrypt(String src) {
        return AesUtils.encrypt(src, PASSWORD);
    }

    /**
     * 对加密后的字符串进行解密
     *
     * @param encrypt 加密后的字符串
     * @return 返回解密后的原文, 解密不成功则原样返回
     * @throws RuntimeException 算法异常
     */
    public static String decrypt(String encrypt) {
        String decrypt = AesUtils.decrypt(encrypt, PASSWORD);
        return decrypt == null ? encrypt : decrypt;
    }
}
