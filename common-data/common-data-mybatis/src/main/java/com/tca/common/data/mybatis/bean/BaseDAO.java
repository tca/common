package com.tca.common.data.mybatis.bean;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tca.common.core.utils.IdUtils;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * @author zhoua
 * @Date 2021/7/8
 */
public class BaseDAO<M extends BaseMapper<T>, T extends BaseEntity> extends ServiceImpl<M,T> {
    @Override
    public boolean save(T entity) {
        if(entity.getId() == null){
            entity.setId(IdUtils.getId());
        }
        entity.setUpdateTime(LocalDateTime.now());
        entity.setCreateTime(LocalDateTime.now());
        return super.save(entity);
    }

    @Override
    public boolean saveBatch(Collection<T> entityList, int batchSize) {
        entityList.forEach(entity->{
            if(entity.getId() == null){
                entity.setId(IdUtils.getId());
            }
            entity.setUpdateTime(LocalDateTime.now());
            entity.setCreateTime(LocalDateTime.now());
        });
        return super.saveBatch(entityList, batchSize);
    }

    @Override
    public boolean updateById(T entity) {
        entity.setUpdateTime(LocalDateTime.now());
        return super.updateById(entity);
    }
}
