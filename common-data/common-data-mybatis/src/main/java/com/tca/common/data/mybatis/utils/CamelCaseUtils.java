package com.tca.common.data.mybatis.utils;


import com.tca.common.core.utils.ValidateUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zhoua
 * @date 2022/2/16 11:24
 */
public class CamelCaseUtils {
    
    private CamelCaseUtils() {}
    
    private static final Map<String, String> COLUMN_FIELD_NAME = new ConcurrentHashMap<>();
    
    /**
     * 将列名转成驼峰式字段名
     * @param column
     * @return
     */
    public static String toCamelCaseField(String column) {
        String fieldName = COLUMN_FIELD_NAME.get(column);
        if (ValidateUtils.isEmpty(fieldName)) {
            StringBuilder sb = new StringBuilder();
            String[] s = column.split("_");
            sb.append(s[0]);
            for (int i = 1; i < s.length; i++) {
                char c = s[i].charAt(0);
                if (c >= 97) {
                    c -= 32;
                }
                StringBuilder tmp = new StringBuilder(s[i]);
                tmp.replace(0, 1, String.valueOf(c));
                sb.append(tmp);
            }
            fieldName = sb.toString();
            COLUMN_FIELD_NAME.put(column, fieldName);
        }
        return fieldName;
    }
    
}
