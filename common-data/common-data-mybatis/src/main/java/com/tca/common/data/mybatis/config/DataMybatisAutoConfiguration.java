package com.tca.common.data.mybatis.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.DataPermissionInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.tca.common.core.datapermission.DataPermissionContextHolder;
import com.tca.common.data.mybatis.datapermission.ThreadLocalDataPermissionContextHolder;
import com.tca.common.data.mybatis.handler.DataPermissionHandler;
import com.tca.common.data.mybatis.handler.MetaObjectHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhoua
 * @Date 2021/7/8
 */
@Configuration
@EnableConfigurationProperties(DataPermissionProperties.class)
public class DataMybatisAutoConfiguration {
    
    @Autowired
    private DataPermissionProperties dataPermissionProperties;
    
    /**
     * 乐观锁插件
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(OptimisticLockerInnerInterceptor.class)
    public OptimisticLockerInnerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInnerInterceptor();
    }
    
    /**
     * 分页插件, 默认使用Mysql, 如果需要
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(PaginationInnerInterceptor.class)
    public PaginationInnerInterceptor paginationInnerInterceptor() {
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor(DbType.MYSQL);
        // 设置请求的页面大于最大页后操作, true调回到首页, false 继续请求  默认false
        paginationInnerInterceptor.setOverflow(false);
        // 设置最大单页限制数量, 默认 500 条, -1 不受限制
        paginationInnerInterceptor.setMaxLimit(500L);
        return paginationInnerInterceptor;
    }

    /**
     * 自定义自动填充器
     * @return
     */
    @Bean
    public MetaObjectHandler tcaMetaObjectHandler(DataPermissionContextHolder dataPermissionContextHolder) {
        return new MetaObjectHandler(dataPermissionContextHolder, dataPermissionProperties);
    }
    
    @Bean
    @ConditionalOnMissingBean(DataPermissionContextHolder.class)
    public DataPermissionContextHolder dataPermissionContextHolder() {
        return new ThreadLocalDataPermissionContextHolder();
    }
    
    @Bean
    public DataPermissionHandler tcaDataPermissionHandler(DataPermissionContextHolder dataPermissionContextHolder) {
        return new DataPermissionHandler(dataPermissionContextHolder, dataPermissionProperties);
    }
    
    @Bean
    @ConditionalOnMissingBean(DataPermissionInterceptor.class)
    public DataPermissionInterceptor dataPermissionInterceptor(DataPermissionHandler dataPermissionHandler) {
        DataPermissionInterceptor dataPermissionInterceptor = new DataPermissionInterceptor();
        dataPermissionInterceptor.setDataPermissionHandler(dataPermissionHandler);
        return dataPermissionInterceptor;
    }
    
    /**
     * 插件组装
     * @param optimisticLockerInnerInterceptor
     * @param paginationInnerInterceptor
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(
            OptimisticLockerInnerInterceptor optimisticLockerInnerInterceptor,
            PaginationInnerInterceptor paginationInnerInterceptor,
            DataPermissionInterceptor dataPermissionInterceptor) {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(optimisticLockerInnerInterceptor);
        // 先添加数据权限的拦截器, 再添加分页拦截器
        if (dataPermissionProperties.isEnabled()) {
            interceptor.addInnerInterceptor(dataPermissionInterceptor);
        }
        interceptor.addInnerInterceptor(paginationInnerInterceptor);
        return interceptor;
    }
    
}
