package com.tca.common.data.mybatis.annotation;

import java.lang.annotation.*;

/**
 * 标记在类上，用于说明是否支持加解密
 * @author zhoua
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface EncryptEnabled {
    /**
     * 是否开启加解密模式
     * @return EncryptEnabled
     */
    boolean value() default true;
}
