package com.tca.common.data.mybatis.listener;

import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;

/**
 * @author zhoua
 * @date 2025/1/11 16:24
 */
public class MybatisApplicationRunListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent>, Ordered {

    private static final Integer ORDER = HIGHEST_PRECEDENCE + 1;

    private static final String MYBATIS_BANNER_KEY = "mybatis-plus.global-config.banner";

    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
        // 关闭 mybatis-plus 的banner
        System.setProperty(MYBATIS_BANNER_KEY, "false");
    }

    @Override
    public int getOrder() {
        return ORDER;
    }

}
