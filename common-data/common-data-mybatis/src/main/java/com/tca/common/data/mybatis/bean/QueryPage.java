package com.tca.common.data.mybatis.bean;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author zhoua
 * @Date 2021/7/8
 */
public class QueryPage extends Page {

    public QueryPage() {
        super();
    }

    public QueryPage(Integer current, Integer size) {
        super();
        setCurrent(current == null ? 1 : current);
        setSize(size == null? 10:size);
    }
}
