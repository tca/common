package com.tca.common.cloud2standalone.component;

import lombok.Data;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author zhoua
 * @date 2022/1/14 10:51
 */
@Data
public class FeignFactoryBean<T> implements FactoryBean<T> {

    private Class<T> feignClass;

    private BeanFactory beanFactory;

    private String prefix;


    @Override
    public T getObject() throws Exception {
        InvocationHandler handler = new FeignProxyHandler<>(feignClass, beanFactory, prefix);
        return (T) Proxy.newProxyInstance(feignClass.getClassLoader(),
                new Class[]{feignClass}, handler);
    }

    @Override
    public Class<T> getObjectType() {
        return feignClass;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}