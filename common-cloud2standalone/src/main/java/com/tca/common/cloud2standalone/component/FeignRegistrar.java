package com.tca.common.cloud2standalone.component;

import com.tca.common.cloud2standalone.annotation.EnableStandalone;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author zhoua
 * @date 2022/1/21 15:10
 */
public class FeignRegistrar implements ImportBeanDefinitionRegistrar {


    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        AnnotationAttributes annoAttrs = AnnotationAttributes
                .fromMap(importingClassMetadata.getAnnotationAttributes(EnableStandalone.class.getName()));
        String[] basePackage = annoAttrs.getStringArray("value");
        ClassPathFeignScanner classPathFeignScanner = new ClassPathFeignScanner(registry);
        classPathFeignScanner.scan(basePackage);
    }
}