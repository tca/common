package com.tca.common.cloud2standalone.annotation;

import com.tca.common.cloud2standalone.component.FeignRegistrar;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author zhoua
 * @date 2022/1/14 10:29
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(FeignRegistrar.class)
public @interface EnableStandalone {

    String[] value() default {};
}

